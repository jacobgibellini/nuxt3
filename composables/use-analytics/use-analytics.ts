// @ts-nocheck
import events from './events.json'

interface AnalyticsEvent {
    eventCategory: string
    eventAction: string
    eventLabel: string
}

export function useAnalytics () {
    function event (name: string, callback: Function) {
        const analyticsEvent: AnalyticsEvent | undefined = events[name];

        if (window.utag?.link && analyticsEvent) {
            window.utag.link({
                event_category: analyticsEvent.eventCategory,
                event_action: analyticsEvent.eventAction,
                event_label: analyticsEvent.eventLabel,
            }, callback, [11, 40]);
        }
        else {
            callback();
        }
    }

    return {
        event
    }
}