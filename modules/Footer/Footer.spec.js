import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import Footer from './Footer';

jest.mock('moment', () => {
    return () => ({
        year: () => '2030',
    });
});

describe('Module: Footer', () => {
    let wrapper;
    let store;

    const mountComponent = ({ state } = {}) => {
        // Arrange
        const localVue = createLocalVue();
        localVue.use(Vuex);

        store = new Vuex.Store({
            modules: {
                app: {
                    state,
                },
            },
        });

        wrapper = shallowMount(Footer, {
            localVue,
            store,
            mocks: {
                $rmitoAnalytics: {
                    event: (str, callback) => callback(),
                },
            },
            stubs: [
                'rmit-online-logo',
                'svg-facebook',
                'svg-twitter',
                'svg-linkedin',
                'svg-instagram',
                'svg-youtube',
                'svg-header-phone-icon',
                'svg-chat-icon',
            ],
        });
    };

    beforeEach(() => {
        mountComponent({
            state: {
                currentYear: '2022',
            },
        });
    });

    it('should render correctly', () => {
        // Assert
        expect(wrapper.html()).toMatchSnapshot();
    });

    it('should prompt user when telephone is clicked', () => {
        // Arrange
        delete window.location;
        window.location = { href: '' };
        mountComponent({
            state: {
                currentYear: null,
            },
        });

        // Act
        const phoneIconEl = wrapper.find('[data-test=social-phone]');
        phoneIconEl.trigger('click');

        // Assert
        expect(window.location.href).toBe('tel:1300145032');
    });

    it("should fallback to the client's date if the year cannot be retrieved from server state", () => {
        // Arrange
        mountComponent({
            state: {
                currentYear: null,
            },
        });

        // Assert
        expect(wrapper.vm.copyrightText).toBe('© 2030 RMIT University');
    });
});
