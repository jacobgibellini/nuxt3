import moment from 'moment';
import { defineStore } from 'pinia'

export default defineStore('app', {
    state: () => ({
        // This will get the date from the server, not the client's browser.
        currentYear: moment().year(),
    })
})
