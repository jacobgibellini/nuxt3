import AppStore from './app.store';

jest.mock('moment', () => {
    return () => ({
        year: () => 2030,
    });
});

describe('Store: app', () => {
    describe('state', () => {
        it('should set the default state when initialised', () => {
            // Arrange
            const expectedState = {
                currentYear: 2030,
            };

            // Act
            const state = AppStore.state();

            // Assert
            expect(state).toEqual(expectedState);
        });
    });
});
