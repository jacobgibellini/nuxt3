import isArrayAndHasChild from './is-array-and-has-child';

/**
 * Returns the first item if the value passed
 * is an array and is not empty.
 * Otherwise, it will return the value.
 *
 * @param { Array } maybeArr
 */
const returnFirstItemIfArray = (maybeArr) => {
    return isArrayAndHasChild(maybeArr) ? maybeArr[0] : maybeArr;
};

export default returnFirstItemIfArray;
