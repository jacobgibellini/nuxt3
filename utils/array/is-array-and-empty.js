const isArrayAndEmpty = (maybeArray) => {
    return Array.isArray(maybeArray) && maybeArray.length === 0;
};

export default isArrayAndEmpty;
