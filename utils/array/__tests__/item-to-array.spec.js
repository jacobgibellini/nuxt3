import itemToArray from '../item-to-array';

describe('fn: itemToArray', () => {
    it.each`
        item
        ${0}
        ${1}
        ${'a string'}
        ${''}
        ${false}
        ${true}
        ${{}}
        ${{}}
    `('should convert $item to [$item]', ({ item }) => {
        // Arrange
        const expected = [item];

        // Act
        const result = itemToArray(item);

        // Assert
        expect(result).toEqual(expected);
    });

    it.each`
        item
        ${[]}
        ${['test']}
        ${[undefined]}
    `('should keep the item as an array', ({ item }) => {
        // Arrange
        const expected = item;

        // Act
        const result = itemToArray(item);

        // Assert
        expect(result).toEqual(expected);
    });

    it.each`
        item
        ${null}
        ${undefined}
    `('should return an empty array when the item is falsey', ({ item }) => {
        // Arrange
        const expected = [];

        // Act
        const result = itemToArray(item);

        // Assert
        expect(result).toEqual(expected);
    });
});
