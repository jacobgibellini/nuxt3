import mapAndRemoveUndefined from '../map-and-remove-undefined';

describe('#mapAndRemoveUndefined', () => {
    it('should return an empty array when the initial value is undefined', () => {
        // Arrange
        const initialValue = undefined;
        const expectedArray = [];
        const mapperFunction = (item) => item;

        // Act
        const result = mapAndRemoveUndefined(initialValue, mapperFunction);

        // Assert
        expect(result).toEqual(expectedArray);
    });

    it('should return an empty array when the initial value is an empty object', () => {
        // Arrange
        const initialValue = {};
        const expectedArray = [];
        const mapperFunction = (item) => item;

        // Act
        const result = mapAndRemoveUndefined(initialValue, mapperFunction);

        // Assert
        expect(result).toEqual(expectedArray);
    });

    it('should map boolean values', () => {
        // Arrange
        const initialValue = [true, true, false];
        const expectedArray = [true, true, false];
        const mapperFunction = (item) => item;

        // Act
        const result = mapAndRemoveUndefined(initialValue, mapperFunction);

        // Assert
        expect(result).toEqual(expectedArray);
    });

    it('should map an array of objects and remove all undefined values', () => {
        // Arrange
        const initialValue = [
            { console: 'Playstation' },
            { console: 'Xbox' },
            { pc: 'windows' },
            {},
            'mac',
        ];
        const expectedArray = ['Playstation', 'Xbox'];
        const mapperFunction = (item) => item.console;

        // Act
        const result = mapAndRemoveUndefined(initialValue, mapperFunction);

        // Assert
        expect(result).toEqual(expectedArray);
    });

    // Tealium example
    it('should map an array of nested objects and remove all undefined values', () => {
        // Arrange
        const initialValue = [
            {
                entity: {
                    entityId: '181',
                    entityUrl: {},
                    entityLabel: 'Business & Finance',
                    machineName: 'business',
                    __typename: 'TaxonomyTermIndustry',
                },
                __typename: 'FieldNodeArticleFieldIndustry',
            },
            {
                entity: null,
                __typename: 'FieldNodeArticleFieldIndustry',
            },
        ];
        const expectedArray = ['Business & Finance'];
        const mapperFunction = (industry) => industry?.entity?.entityLabel;

        // Act
        const result = mapAndRemoveUndefined(initialValue, mapperFunction);

        // Assert
        expect(result).toEqual(expectedArray);
    });
});
