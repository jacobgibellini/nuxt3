import returnFirstItemIfArray from '../return-first-item-if-array';

describe('fn: returnFirstItemIfArray', () => {
    it('should return the first item when array is passed', () => {
        // Arrange
        const arr = [
            { id: 1, name: 'Obi-wan' },
            { id: 2, name: 'Anakain' },
            { id: 3, name: 'Yoda' },
        ];
        const expected = { id: 1, name: 'Obi-wan' };

        // Act
        const result = returnFirstItemIfArray(arr);

        // Assert
        expect(result).toEqual(expected);
    });

    it('should return the object that was passed', () => {
        // Arrange
        const obj = { id: 3, name: 'Yoda' };
        const expected = { id: 3, name: 'Yoda' };

        // Act
        const result = returnFirstItemIfArray(obj);

        // Assert
        expect(result).toEqual(expected);
    });
});
