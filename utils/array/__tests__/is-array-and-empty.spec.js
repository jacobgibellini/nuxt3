import { isArrayAndEmpty } from '@/utils/array';

describe('#isArrayAndEmpty', () => {
    it('should return true when value is empty array', () => {
        // Arrange
        const initialValue = [];
        const expectedResult = true;

        // Act
        const result = isArrayAndEmpty(initialValue);

        // Assert
        expect(result).toBe(expectedResult);
    });

    it('should return false when value is undefined', () => {
        // Arrange
        const initialValue = undefined;
        const expectedResult = false;

        // Act
        const result = isArrayAndEmpty(initialValue);

        // Assert
        expect(result).toBe(expectedResult);
    });

    it('should return false when value is an object literal', () => {
        // Arrange
        const initialValue = { id: 123 };
        const expectedResult = false;

        // Act
        const result = isArrayAndEmpty(initialValue);

        // Assert
        expect(result).toBe(expectedResult);
    });

    it('should return false when value is array that contains an item', () => {
        // Arrange
        const initialValue = ['test'];
        const expectedResult = false;

        // Act
        const result = isArrayAndEmpty(initialValue);

        // Assert
        expect(result).toBe(expectedResult);
    });
});
