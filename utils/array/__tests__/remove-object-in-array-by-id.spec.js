import removeObjectInArrayById from '../remove-object-in-array-by-id';

describe('fn: removeObjectInArrayById', () => {
    it('should remove object from an array', () => {
        // Arrange
        const arr = [
            { id: 1, name: 'Obi-wan' },
            { id: 2, name: 'Anakain' },
            { id: 3, name: 'Yoda' },
        ];
        const id = 2;
        const expected = [
            { id: 1, name: 'Obi-wan' },
            { id: 3, name: 'Yoda' },
        ];

        // Act
        const result = removeObjectInArrayById(arr, id);

        // Assert
        expect(result).toEqual(expected);
    });
});
