import { isArrayAndHasChild } from '@/utils/array';

describe('#isArrayAndHasChild', () => {
    it('should return false when value is undefined', () => {
        const result = isArrayAndHasChild(undefined);

        expect(result).toBe(false);
    });

    it('should return false when value is empty array', () => {
        const result = isArrayAndHasChild([]);

        expect(result).toBe(false);
    });

    it('should return false when value is an object literal', () => {
        const result = isArrayAndHasChild({
            id: 123,
        });

        expect(result).toBe(false);
    });

    it('should return true when value is array with one empty object as item', () => {
        const result = isArrayAndHasChild([{}]);

        expect(result).toBe(true);
    });
});
