import { sortArrayOfObjects } from '@/utils/array';

describe('#sortArrayOfObjects', () => {
    it('should return an empty array when nothing is provided', () => {
        // Arrange
        const expectedArray = [];

        // Act
        const sortedArray = sortArrayOfObjects();

        // Assert
        expect(sortedArray).toEqual(expectedArray);
    });

    it('should return an empty array when the initial array is empty', () => {
        // Arrange
        const initialArray = [];
        const expectedArray = [];

        // Act
        const sortedArray = sortArrayOfObjects(initialArray, 'title');

        // Assert
        expect(sortedArray).toEqual(expectedArray);
    });

    it('should return an empty array when the initial value is not an array', () => {
        // Arrange
        const initialValue = 'value';
        const expectedArray = [];

        // Act
        const sortedArray = sortArrayOfObjects(initialValue, 'title');

        // Assert
        expect(sortedArray).toEqual(expectedArray);
    });

    it('should not sort an array when no object prop argument is passed', () => {
        // Arrange
        const initialArray = [
            { title: 'Mercedez' },
            { title: 'Lamborghini' },
            { title: 'Ferrari' },
            { title: 'Aston Martin' },
            { title: 'Ford' },
            { title: 'Bugatti' },
        ];
        const expectedArray = [
            { title: 'Mercedez' },
            { title: 'Lamborghini' },
            { title: 'Ferrari' },
            { title: 'Aston Martin' },
            { title: 'Ford' },
            { title: 'Bugatti' },
        ];

        // Act
        const sortedArray = sortArrayOfObjects(initialArray);

        // Assert
        expect(sortedArray).toEqual(expectedArray);
    });

    it('should sort an array of objects by string', () => {
        // Arrange
        const initialArray = [
            { title: 'Mercedez' },
            { title: 'Lamborghini' },
            { title: 'Ferrari' },
            { title: 'Aston Martin' },
            { title: 'Ford' },
            { title: 'Bugatti' },
        ];
        const expectedArray = [
            { title: 'Aston Martin' },
            { title: 'Bugatti' },
            { title: 'Ferrari' },
            { title: 'Ford' },
            { title: 'Lamborghini' },
            { title: 'Mercedez' },
        ];

        // Act
        const sortedArray = sortArrayOfObjects(initialArray, 'title');

        // Assert
        expect(sortedArray).toEqual(expectedArray);
    });

    it('should sort an array of objects by number', () => {
        // Arrange
        const initialArray = [{ rating: 10 }, { rating: 2 }, { rating: 5 }, { rating: 1 }];
        const expectedArray = [{ rating: 1 }, { rating: 2 }, { rating: 5 }, { rating: 10 }];

        // Act
        const sortedArray = sortArrayOfObjects(initialArray, 'rating');

        // Assert
        expect(sortedArray).toEqual(expectedArray);
    });
});
