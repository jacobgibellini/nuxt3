/**
 * @description
 * Turns a single data property into an array.
 * This will return the same item if it's already an array.
 * @param { any } item
 * @returns { Array }
 */
export default function itemToArray(item) {
    if (item == null) {
        return [];
    }
    return Array.isArray(item) ? item : [item];
}
