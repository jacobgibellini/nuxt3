const sortArrayOfObjects = (arr = [], objectProp = '') => {
    const sort = (a, b) => {
        if (a[objectProp] < b[objectProp]) {
            return -1;
        }
        if (a[objectProp] > b[objectProp]) {
            return 1;
        }
        return 0;
    };

    return Array.isArray(arr) ? arr.sort(sort) : [];
};

export default sortArrayOfObjects;
