import remove from 'lodash/remove';

const removeObjectInArrayById = (arr, id) => {
    return remove(arr, (item) => item?.id !== id);
};

export default removeObjectInArrayById;
