const isArrayAndHasChild = (maybeArray) => Array.isArray(maybeArray) && maybeArray.length > 0;

export default isArrayAndHasChild;
