import isArrayAndHasChild from './is-array-and-has-child';

const mapAndRemoveUndefined = (arr, mapper) =>
    isArrayAndHasChild(arr)
        ? arr.reduce((result, item) => {
              const mappedItem = mapper(item);
              if (mappedItem !== undefined) {
                  result.push(mappedItem);
              }
              return result;
          }, [])
        : [];

export default mapAndRemoveUndefined;
