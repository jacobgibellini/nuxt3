import { mapMainMenuLinks } from '@/utils/mappers/menu';

describe('#mapMainMenuLinks', () => {
    it('should "/courses" have RA event name "clickHeaderCatalogue"', () => {
        const links = [
            {
                label: 'Courses & degrees',
                url: {
                    path: '/courses'
                }
            },
            {
                label: 'blog',
                url: {
                    path: '/blog'
                }
            }
        ];

        const actual       = mapMainMenuLinks(links);
        const catalogueNav = actual.find(({ path }) => path === '/courses');

        expect(catalogueNav.event).toBe('clickHeaderCatalogue');
    });

    it('should "/course" not have RA event name "clickHeaderCatalogue"', () => {
        const links = [
            {
                label: 'Some Course',
                url: {
                    path: '/course'
                }
            },
            {
                label: 'blog',
                url: {
                    path: '/blog'
                }
            }
        ];

        const actual    = mapMainMenuLinks(links);
        const courseNav = actual.find(({ path }) => path === '/course');

        expect(courseNav.event).not.toBe('clickHeaderCatalogue');
    });

    it('should map raw data to legacy data structure', () => {
        const expected = [
            {
                text: 'Courses & degrees',
                path: '/courses',
                event: 'clickHeaderCatalogue',
                links: []
            },
            {
                text: 'Blog',
                path: '/blog',
                links: []
            },
            {
                text: 'For business',
                path: '#!',
                links: []
            },
        ];
        const links = [
            {
                label: 'Courses & degrees',
                url: {
                    path: '/courses',
                },
                links: []
            },
            {
                label: 'Blog',
                url: {
                    path: '/blog'
                }
            },
            {
                label: 'For business',
                url: '/blog',
                event: 'LegacyFtw'
            }
        ];

        const actual = mapMainMenuLinks(links);

        expect(actual).toEqual(expected);
    });

    it('should sublinks included', () => {
        const expected = [
            {
                text: 'Courses & degrees',
                path: '/courses',
                event: 'clickHeaderCatalogue',
                links: [
                    {
                        label: 'Category Name 1',
                        url: {
                            path: '/category-name-1',
                        },
                        links: [
                            {
                                label: 'Sub Category Name 1',
                                url: {
                                    path: '/sub-category-name-1',
                                }
                            }
                        ]
                    }
                ]
            }
        ];
        const links = [
            {
                label: 'Courses & degrees',
                url: {
                    path: '/courses',
                },
                links: [
                    {
                        label: 'Category Name 1',
                        url: {
                            path: '/category-name-1',
                        },
                        links: [
                            {
                                label: 'Sub Category Name 1',
                                url: {
                                    path: '/sub-category-name-1',
                                }
                            }
                        ]
                    }
                ]
            }
        ];

        const actual = mapMainMenuLinks(links);

        expect(actual).toEqual(expected);
    });
});