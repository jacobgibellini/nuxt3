import { isArrayAndHasChild } from '@/utils/array';

export const mapSubCategoryToCategory = (subCategories, categoryName) =>
    subCategories.reduce((subCategoryNames, subCategory) => {
        const subCategoryName = subCategory?.machineName;

        return {
            ...subCategoryNames,
            ...(subCategoryName ? { [subCategoryName]: categoryName } : {})
        }
    }, {})

export const mapToCategoryReference = term => {
    const categoryName  = term?.machineName;
    const subCategories = term?.children?.entities;

    if (!categoryName) return;

    return {
        ...(categoryName ? { [categoryName]: categoryName } : {}),
        ...(isArrayAndHasChild(subCategories) ? mapSubCategoryToCategory(subCategories, categoryName) : {})
    };
};

export const mapTermListResults = terms =>
    terms.reduce((normalizedTerms, term) => ({
        ...normalizedTerms,
        ...mapToCategoryReference(term)
    }), {});

export const mapToAreaOfStudy = terms =>
    isArrayAndHasChild(terms)
        ? terms.map(({
            entityLabel,
            entityUrl,
            description
        }) => ({
            label: entityLabel || '',
            description: description?.processed || '',
            url: entityUrl?.path || ''
        }))
        : []