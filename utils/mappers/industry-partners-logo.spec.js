import { mapIndustryPartnersLogo } from './industry-partners-logo';

describe('#mapIndustryPartnersLogo', () => {
    it('should map items have a valid "url"', () => {
        const expected = [
            {
                url: '//assets.marketstg.rmitonlinestg.com/image-1_160x60.png',
                alt: 'accenture'
            },
            {
                url: '//assets.marketstg.rmitonlinestg.com/image-2_160x60.png',
                alt: ''
            }
        ];
        const logos = [
            {
                fieldIndustryLogo: {
                    is_svg: false,
                    svg: null,
                    url: '//assets.marketstg.rmitonlinestg.com/image-1_160x60.png',
                    alt: 'accenture',
                    width: 160,
                    height: 60
                }
            },
            {
                fieldIndustryLogo: {
                    is_svg: false,
                    svg: null,
                    url: '//assets.marketstg.rmitonlinestg.com/image-2_160x60.png',
                    width: 160,
                    height: 60
                }
            },
            {
                fieldIndustryLogo: {
                    is_svg: false,
                    svg: null,
                    url: '',
                    alt: 'stone & chalk',
                    width: 160,
                    height: 60
                }
            },
            {
                fieldIndustryLogo: {
                    is_svg: false,
                    svg: null,
                    url: null,
                    alt: '',
                    width: 160,
                    height: 60
                }
            },
            {
                fieldIndustryLogo: {
                    is_svg: false,
                    svg: null,
                    alt: '',
                    width: 160,
                    height: 60
                }
            },
            {
                fieldIndustryLogo: null
            },
            {
                fieldIndustryLogo: {}
            },
            {},
            false,
            '',
            0,
            null
        ];

        const actual = mapIndustryPartnersLogo(logos);

        expect(actual).toEqual(expected);
    });
})