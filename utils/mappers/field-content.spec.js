import {
    addDataToStudentHubTabItems,
    addFaqAccodionFooterLink,
    mapEntityBundleToRenderConfig,
    mapFieldContentToComponent,
    mapFieldContentWithData,
    mapFieldMultiPodVideos,
    mapItemsOfEntitySubqueueLandingPageFaqsLists,
} from './field-content';

// Shared payload data
const itemsOfEntitySubqueueLandingPageFaqsLists = [
    {
        entity: {
            entityId: '171',
            info: 'Are there any entry requirements?',
            body: {
                processed: '<p>To enroll in our courses, you must be 18 years or older.</p>',
            },
        },
    },
    {
        entity: {
            entityId: '276',
            info: 'How many feedback sessions do I get?',
            body: {
                processed:
                    '<p>Please speak to your enrolment or student success advisor for specific details about your course. ...</p>',
            },
        },
    },
];

// Shared expected data
const expectedRawPanelsCollection = [
    {
        id: '171',
        label: 'Are there any entry requirements?',
        contentHtml: '<p>To enroll in our courses, you must be 18 years or older.</p>',
    },
    {
        id: '276',
        label: 'How many feedback sessions do I get?',
        contentHtml:
            '<p>Please speak to your enrolment or student success advisor for specific details about your course. ...</p>',
    },
];

// Test Begin below
describe('#mapEntityBundleToRenderConfig', () => {
    it('should return "RmitoBlockquote" render config mapped when entityBundle was "block_quote"', () => {
        const expected = {
            name: 'RmitoBlockquote',
            props: {
                id: 'block_quote-1111',
                content: 'Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.',
                footer: 'Anonymous',
            },
        };
        const entity = {
            entityId: '1111',
            entityBundle: 'block_quote',
            fieldQuote: 'Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.',
            fieldAuthorName: 'Anonymous',
        };

        const actual = mapEntityBundleToRenderConfig(entity);

        expect(actual).toEqual(expected);
    });

    it('should return "RmitoBlockquote" render config does not contain footer prop when "fieldAuthorName" is not found from source', () => {
        const expected = {
            name: 'RmitoBlockquote',
            props: {
                id: 'block_quote-1235',
                content: 'a blockquote without author name',
                footer: undefined,
            },
        };
        const entity = {
            entityId: '1235',
            entityBundle: 'block_quote',
            fieldQuote: 'a blockquote without author name',
        };

        const actual = mapEntityBundleToRenderConfig(entity);

        expect(actual).toEqual(expected);
    });

    it('should return "RmitoPullquote" render config mapped when entityBundle was "pull_quote"', () => {
        const expected = {
            name: 'RmitoPullquote',
            props: {
                id: 'pull_quote-2235',
                content: 'Jonny home made sour dough',
            },
        };
        const entity = {
            entityId: '2235',
            entityBundle: 'pull_quote',
            fieldQuote: 'Jonny home made sour dough',
        };

        const actual = mapEntityBundleToRenderConfig(entity);

        expect(actual).toEqual(expected);
    });

    it('should return "RmitoImageWithCaption" render config mapped when entityBundle was "image"', () => {
        const expected = {
            name: 'RmitoImageWithCaption',
            props: {
                id: 'image-666',
                src: '/jonny-smiley-face.jpg',
                alt: 'a gentlemen smiling with the grumpy wallaby',
                caption: 'The grumpy wallaby asked Jonny "What so funny"',
            },
        };
        const entity = {
            entityId: '666',
            entityBundle: 'image',
            fieldImage: {
                alt: 'a gentlemen smiling with the grumpy wallaby',
                title: 'The grumpy wallaby asked Jonny "What so funny"',
                derivative: {
                    url: '/jonny-smiley-face.jpg',
                },
            },
        };

        const actual = mapEntityBundleToRenderConfig(entity);

        expect(actual).toEqual(expected);
    });

    it('should return "RmitoRichTextContent" render config mapped when entityBundle was "content"', () => {
        const expected = {
            name: 'RmitoRichTextContent',
            props: {
                id: 'content-666',
                'content-html':
                    '<h1>When Jonny had a day off</h1><p>Jonny was very happy until Wallaby rang and said we had production issue.</p>',
            },
        };
        const entity = {
            entityId: '666',
            entityBundle: 'content',
            fieldBody: {
                processed:
                    '<h1>When Jonny had a day off</h1><p>Jonny was very happy until Wallaby rang and said we had production issue.</p>',
            },
        };

        const actual = mapEntityBundleToRenderConfig(entity);

        expect(actual).toEqual(expected);
    });

    it('should return "RmitoSupportiveVideo" render config mapped when entityBundle was "hero_video_component"', () => {
        const expected = {
            name: 'RmitoSupportiveVideo',
            props: {
                id: 'hero_video_component-9411',
                duration: '19:11',
                sectionDescriptionHtml:
                    'All the basics to get you started with your study with RMIT Online',
                sectionTitle: 'Future Skills online study basics',
                videoDescriptionHtml:
                    "<h3>What to expect from your Future Skills course</h3>\n\n<p><strong>You'll learn about:</strong></p>\n\n<ul><li>Course length and study modules</li>\n\t<li>Activities, milestones and the final project</li>\n\t<li>Mentors and the learner success team </li>\n\t<li>Communication tools and channels</li>\n</ul>",
                videoUrl: 'https://www.youtube.com/watch?v=Hf0b_-hVBS4',
                videoPosition: 'right',
            },
        };
        const entity = {
            entityId: '9411',
            entityBundle: 'hero_video_component',
            fieldHeading: 'Future Skills online study basics',
            fieldSubheading: {
                processed: 'All the basics to get you started with your study with RMIT Online',
            },
            fieldVideoUrl: 'https://www.youtube.com/watch?v=Hf0b_-hVBS4',
            fieldVideoDescription: {
                processed:
                    "<h3>What to expect from your Future Skills course</h3>\n\n<p><strong>You'll learn about:</strong></p>\n\n<ul><li>Course length and study modules</li>\n\t<li>Activities, milestones and the final project</li>\n\t<li>Mentors and the learner success team </li>\n\t<li>Communication tools and channels</li>\n</ul>",
            },
            fieldVideoDuration: '19:11',
            fieldVideoPosition: 'right',
        };

        const actual = mapEntityBundleToRenderConfig(entity);

        expect(actual).toEqual(expected);
    });

    it('should return "RmitoSupportiveImage" render config mapped when entityBundle was "hero_image_component"', () => {
        const expected = {
            name: 'RmitoSupportiveImage',
            props: {
                id: 'hero_image_component-1337',
                title: 'The value of your digital credential',
                description: '<p>You will learn about:<p>',
                position: 'right',
                src: 'http://localhost:3000/fake-image',
                button: {
                    secondary: false,
                    label: 'Click me!',
                    url: '/courses',
                },
            },
        };
        const entity = {
            entityId: '1337',
            entityBundle: 'hero_image_component',
            fieldHeading: 'The value of your digital credential',
            fieldDescription: {
                processed: '<p>You will learn about:<p>',
            },
            fieldImage: {
                derivative: {
                    url: 'http://localhost:3000/fake-image',
                },
            },
            fieldImagePosition: 'Right',
            fieldButtonColour: 'Primary',
            fieldLink: {
                title: 'Click me!',
                url: {
                    path: '/courses',
                },
            },
        };

        const actual = mapEntityBundleToRenderConfig(entity);

        expect(actual).toEqual(expected);
    });

    it('should return "RmitoMultiVideoPod" render config mapped when entityBundle was "multi_pod_videos_component"', () => {
        const expected = {
            name: 'RmitoMultiVideoPod',
            props: {
                id: 'multi_pod_videos_component-9426',
                sectionDescriptionHtml: 'Section <strong>Description</strong>',
                sectionTitle: 'Course toolkit',
                videos: [],
            },
        };
        const entity = {
            entityId: '9426',
            entityBundle: 'multi_pod_videos_component',
            fieldHeading: 'Course toolkit',
            fieldSubheading: {
                processed: 'Section <strong>Description</strong>',
            },
            fieldMultiPodVideos: null,
        };

        const actual = mapEntityBundleToRenderConfig(entity);

        expect(actual).toEqual(expected);
    });

    it('should return "RmitAccordionPanels" render config mapped when entityBundle was "faqs_accordion"', () => {
        const expected = {
            name: 'RmitAccordionPanels',
            props: {
                sectionDescriptionHtml:
                    "<p>The prospect of online learning can feel daunting before you start. We've collected answers to some common queries to put your mind at ease.</p>",
                sectionTitle: 'Got any questions?',
                rawPanelsCollection: expectedRawPanelsCollection,
                multiOpen: true,
                gaEventFooterLink: 'clickFaqBrowseAll',
                gaEventOnClick: 'clickFaqPanel',
            },
        };
        const entity = {
            entityBundle: 'faqs_accordion',
            fieldHeading: 'Got any questions?',
            fieldSubheading: {
                processed:
                    "<p>The prospect of online learning can feel daunting before you start. We've collected answers to some common queries to put your mind at ease.</p>",
            },
            fieldFaqsEntitySubqueue: {
                entity: {
                    entityBundle: 'student_hub_faqs_lists',
                    entityId: 'short_course',
                    title: 'Short course',
                    itemsOfEntitySubqueueLandingPageFaqsLists,
                },
            },
        };

        const actual = mapEntityBundleToRenderConfig(entity);

        expect(actual).toEqual(expected);
    });

    it('should return "RmitoCategoryCardsPod" render config mapped when entityBundle was "short_courses"', () => {
        const expected = {
            name: 'RmitoCategoryCardsPod',
            props: {
                cards: [
                    {
                        label: 'Digital Delivery with Agile',
                        description:
                            'Next start 2 Nov 2020<p>Harness agile and lean methodologies and collaborate more effectively within your team.</p>',
                        color: '#0078FF',
                        url: '/course/sc-digital-delivery-agile-dtr104',
                    },
                ],
                footerLinkText: 'Browse all courses available',
                footerLinkUrl: '/courses',
                footerLinkType: 'url',
                footerLinkEvent: 'clickBrowseAllStudyOptions',
                cardClickEvent: 'clickBrowseCourse',
                extendedLine: true,
            },
        };
        const entity = {
            entityId: '9948',
            entityBundle: 'short_courses',
            fieldShortCoursesList: [
                {
                    entity: {
                        title: 'Digital Delivery with Agile',
                        fieldCourseDescription: {
                            processed:
                                '<p>Harness agile and lean methodologies and collaborate more effectively within your team.</p>',
                        },
                        fieldNextStartDate: {
                            date: '2020-11-02 12:00:00 UTC',
                        },
                        entityUrl: {
                            path: '/course/sc-digital-delivery-agile-dtr104',
                        },
                    },
                },
            ],
            fieldAllCoursesLink: {
                title: 'Browse all courses available',
                url: {
                    path: '/courses',
                },
            },
            fieldSidebarColour: {
                color: '#0078FF',
            },
        };

        const actual = mapEntityBundleToRenderConfig(entity);

        expect(actual).toEqual(expected);
    });

    it('should return "RmitoCategoryCardsPod" render config mapped when entityBundle was "micro_credential_list"', () => {
        const expected = {
            name: 'RmitoCategoryCardsPod',
            props: {
                cards: [
                    {
                        label: 'Agile Ways of Working',
                        description: 'Agile methodology in business',
                        color: '#AA00AA',
                    },
                ],
                footerLinkText: 'Download brochure to see all options',
                footerLinkUrl:
                    '//assets.rmitonline.docksal/DTR107_Agile_Project_Management_10f.pdf',
                footerLinkType: 'file',
                footerLinkEvent: 'clickDownloadBrochureCredentials',
            },
        };
        const entity = {
            entityId: '9952',
            entityBundle: 'micro_credential_list',
            fieldMicroCredentials: [
                {
                    entity: {
                        fieldLabel: 'Agile Ways of Working',
                        fieldDescription: {
                            processed: 'Agile methodology in business',
                        },
                    },
                },
            ],
            fieldDownloadBrochureLink: {
                entity: {
                    url: '//assets.rmitonline.docksal/DTR107_Agile_Project_Management_10f.pdf',
                },
            },
            fieldDownloadBrochureLabel: 'Download brochure to see all options',
            fieldSidebarColour: {
                color: '#AA00AA',
            },
        };

        const actual = mapEntityBundleToRenderConfig(entity);

        expect(actual).toEqual(expected);
    });

    it('should return undefined when entityBundle does not exist', () => {
        const entity = {
            fieldSurprised: 'dance dance',
        };

        const actual = mapEntityBundleToRenderConfig(entity);

        expect(actual).toBeUndefined();
    });

    it('should return undefined when entityBundle has an invalid value', () => {
        const entity = {
            entityBundle: 'mu-ha-ha-wa-ha-ha',
            fieldBody: 'beep beep',
        };

        const actual = mapEntityBundleToRenderConfig(entity);

        expect(actual).toBeUndefined();
    });
});

describe('#mapFieldContentToComponent', () => {
    it('should map drupal "fieldContent" valid items to Vue components', () => {
        const expected = [
            {
                name: 'RmitoRichTextContent',
                props: {
                    id: null,
                    'content-html':
                        '<h2>What Is Python?</h2><p>Python is an interpreted, high-level, general-purpose programming language...</p>',
                },
            },
            {
                name: 'RmitoImageWithCaption',
                props: {
                    id: null,
                    src: 'http://sample.com/image.jpg',
                    alt: 'sample image',
                    caption: 'Photo: copyright caption goes here',
                },
            },
            {
                name: 'RmitoPullquote',
                props: {
                    id: null,
                    content:
                        'Python developer is one of the 10 Most In-Demand Tech Jobs of 2019. This is due to the rise of AI and Machine Learning technologies in the global market.',
                },
            },
            {
                name: 'RmitoBlockquote',
                props: {
                    id: null,
                    content: 'Lorem ipsum dolor sit amet, consectetur',
                    footer: '- Author’s name',
                },
            },
        ];
        const fieldContent = [
            {
                entity: {
                    entityBundle: 'content',
                    fieldBody: {
                        processed:
                            '<h2>What Is Python?</h2><p>Python is an interpreted, high-level, general-purpose programming language...</p>',
                    },
                },
            },
            {
                entity: {
                    entityBundle: 'image',
                    fieldImage: {
                        alt: 'sample image',
                        title: 'Photo: copyright caption goes here',
                        derivative: {
                            url: 'http://sample.com/image.jpg',
                        },
                    },
                },
            },
            {
                entity: {
                    entityBundle: 'pull_quote',
                    fieldQuote:
                        'Python developer is one of the 10 Most In-Demand Tech Jobs of 2019. This is due to the rise of AI and Machine Learning technologies in the global market.',
                },
            },
            {
                entity: {
                    entityBundle: 'block_quote',
                    fieldQuote: 'Lorem ipsum dolor sit amet, consectetur',
                    fieldAuthorName: '- Author’s name',
                },
            },
            {
                entity: {
                    entityBundle: 'new_component_not_available_in_vue_yet',
                    fieldShock: 'Why this is not rendering in front-end?',
                },
            },
            {},
            [],
            null,
            undefined,
            false,
            0,
            '',
        ];
        const actual = mapFieldContentToComponent(fieldContent);

        expect(actual).toEqual(expected);
    });
});

describe('#mapFieldMultiPodVideos', () => {
    it('should return collection of videos formatted for Component: "RmitoMultiVideoPod" prop - "video"', () => {
        const expected = [
            {
                id: null,
                videoUrl: 'https://www.youtube.com/watch?v=kEZZCQTSSAg',
                duration: '10:45',
                description: '<p>A Beginners Guide to Coffee Tasting<p>',
            },
            {
                id: 'multi_pod_video_item-9421',
                videoUrl: 'https://www.youtube.com/watch?v=8cIqLvJz8VM',
                duration: null,
                description: 'Supermarket Instant Coffee - Which One Tastes Best?',
            },
        ];
        const fieldMultiPodVideos = [
            {
                entity: {
                    entityBundle: 'multi_pod_video_item',
                    fieldVideoUrl: 'https://www.youtube.com/watch?v=kEZZCQTSSAg',
                    fieldVideoDescription: {
                        processed: '<p>A Beginners Guide to Coffee Tasting<p>',
                    },
                    fieldVideoDuration: '10:45',
                },
            },
            {
                entity: {
                    entityId: '9421',
                    entityBundle: 'multi_pod_video_item',
                    fieldVideoUrl: 'https://www.youtube.com/watch?v=8cIqLvJz8VM',
                    fieldVideoDescription: {
                        processed: 'Supermarket Instant Coffee - Which One Tastes Best?',
                    },
                    fieldVideoDuration: null,
                },
            },
        ];

        const actual = mapFieldMultiPodVideos(fieldMultiPodVideos);

        expect(actual).toEqual(expected);
    });
});

describe('#mapItemsOfEntitySubqueueLandingPageFaqsLists', () => {
    it('should map raw data to Component: "RmitAccordionPanels" prop - "rawPanelsCollection"', () => {
        const actual = mapItemsOfEntitySubqueueLandingPageFaqsLists(
            itemsOfEntitySubqueueLandingPageFaqsLists
        );

        expect(actual).toEqual(expectedRawPanelsCollection);
    });
});

describe('#mapFieldContentWithData', () => {
    const mockFieldContent = [
        {
            entity: {
                entityId: '9826',
                entityBundle: 'faqs_accordion',
                fieldHeading: 'Test FAQ Accordion Heading',
                fieldSubheading: {
                    processed: '<p>Test FAQ Accordion Subheading</p>',
                },
                fieldFaqsEntitySubqueue: {
                    entity: {
                        entityBundle: 'landing_page_faqs_lists',
                        entityId: 'test_faq',
                        title: 'test faq',
                        itemsOfEntitySubqueueLandingPageFaqsLists: [
                            {
                                entity: {
                                    entityBundle: 'faq_item',
                                    entityId: '186',
                                },
                            },
                        ],
                    },
                },
            },
        },
        {
            entity: {
                entityBundle: 'something_else',
                fieldRandom: 'does not matter',
            },
        },
    ];

    it('should return original data when "rawDataHackFn" was not specified', () => {
        const actual = mapFieldContentWithData(mockFieldContent);

        expect(actual).toEqual(mockFieldContent);
    });

    it('should return original data when "rawDataHackFn" was not a function', () => {
        const actual = mapFieldContentWithData(mockFieldContent, 'hello');

        expect(actual).toEqual(mockFieldContent);
    });

    it('should return an array of undefined when "rawDataHackFn" does not return modified item or raw item', () => {
        const expected = [undefined, undefined];
        const actual = mapFieldContentWithData(mockFieldContent, () => {});

        expect(actual).toEqual(expected);
    });

    it('should return an array of values returned from "rawDataHackFn"', () => {
        const expected = ['faqs_accordion', 'something_else'];
        const actual = mapFieldContentWithData(
            mockFieldContent,
            ({ entity }) => entity.entityBundle
        );

        expect(actual).toEqual(expected);
    });
});

describe('#addFaqAccodionFooterLink', () => {
    it('should return existing fields with "footerLinkUrl" and "footerLinkText" when entityBundle was "faqs_accordion"', () => {
        const expected = {
            entity: {
                entityId: '9826',
                entityBundle: 'faqs_accordion',
                footerLinkUrl: '/faq',
                footerLinkText: 'See all FAQs',
                fieldFaqsEntitySubqueue: {
                    entity: {
                        entityBundle: 'landing_page_faqs_lists',
                        itemsOfEntitySubqueueLandingPageFaqsLists: [],
                    },
                },
            },
        };
        const mockFieldContentItem = {
            entity: {
                entityId: '9826',
                entityBundle: 'faqs_accordion',
                fieldFaqsEntitySubqueue: {
                    entity: {
                        entityBundle: 'landing_page_faqs_lists',
                        itemsOfEntitySubqueueLandingPageFaqsLists: [],
                    },
                },
            },
        };

        const actual = addFaqAccodionFooterLink(mockFieldContentItem);

        expect(actual).toEqual(expected);
    });

    it('should return existing fields without "footerLinkUrl" and "footerLinkText" when entityBundle was not "faqs_accordion"', () => {
        const expected = {
            entity: {
                entityId: '007',
                lampchops: ['standard', 'premium'],
                entityBundle: 'fa_la_fa_le_foo',
            },
        };
        const mockFieldContentItem = {
            entity: {
                entityId: '007',
                lampchops: ['standard', 'premium'],
                entityBundle: 'fa_la_fa_le_foo',
            },
        };

        const actual = addFaqAccodionFooterLink(mockFieldContentItem);

        expect(actual).toEqual(expected);
    });
});

describe('#addDataToStudentHubTabItems', () => {
    const expected = [
        {
            fieldContent: [
                {
                    entity: {
                        entityId: '9824',
                        entityBundle: 'faqs_accordion',
                        fieldHeading: 'Test FAQ Accordion Heading',
                        footerLinkUrl: '/faq',
                        footerLinkText: 'See all FAQs',
                        fieldSubheading: {
                            processed: '<p>Test FAQ Accordion Subheading</p>',
                        },
                    },
                },
            ],
        },
        {
            fieldContent: [
                {
                    entity: {
                        entityId: '9823',
                        entityBundle: 'faqs_accordion',
                        footerLinkUrl: '/faq',
                        footerLinkText: 'See all FAQs',
                    },
                },
                {
                    entity: {
                        entityId: '9826',
                        entityBundle: 'tab_2_else',
                    },
                },
            ],
        },
    ];
    const mockTabItems = [
        {
            fieldContent: [
                {
                    entity: {
                        entityId: '9824',
                        entityBundle: 'faqs_accordion',
                        fieldHeading: 'Test FAQ Accordion Heading',
                        fieldSubheading: {
                            processed: '<p>Test FAQ Accordion Subheading</p>',
                        },
                    },
                },
            ],
        },
        {
            fieldContent: [
                {
                    entity: {
                        entityId: '9823',
                        entityBundle: 'faqs_accordion',
                    },
                },
                {
                    entity: {
                        entityId: '9826',
                        entityBundle: 'tab_2_else',
                    },
                },
            ],
        },
    ];

    const actual = addDataToStudentHubTabItems(mockTabItems);

    expect(actual).toEqual(expected);
});
