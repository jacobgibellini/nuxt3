import {
    mapBreadcrumbJsonLd,
    mapCourseJsonLd,
    mapOrgContactLd
} from './json-ld';

describe('#mapBreadcrumbJsonLd', () => {
    it('should prepend base url to relative path', () => {
        const expected = {
            '@context': 'https://schema.org',
            '@type': 'BreadcrumbList',
            itemListElement: [
                {
                    '@type': 'ListItem',
                    position: 1,
                    name: 'Home'
                },
                {
                    '@type': 'ListItem',
                    position: 2,
                    name: 'Business & Finance',
                    item: 'http://mocked-base-url:3000/business-finance'
                },
                {
                    '@type': 'ListItem',
                    position: 3,
                    name: 'Finance & Accounting'
                }
            ]
        };
        const mockProcessedTrails = [
            { label: 'Home', href: null },
            { label: 'Business & Finance', href: '/business-finance' },
            { label: 'Finance & Accounting', href: '/business-finance/finance-accounting' }
        ];
        const received = mapBreadcrumbJsonLd(mockProcessedTrails);

        expect(received).toEqual(expected);
    });

    it('should keep position number in order and ignore trail does not have both label and href', () => {
        const expected = {
            '@context': 'https://schema.org',
            '@type': 'BreadcrumbList',
            itemListElement: [
                {
                    '@type': 'ListItem',
                    position: 1,
                    name: 'Home',
                    item: 'http://mocked-base-url:3000/'
                },
                {
                    '@type': 'ListItem',
                    position: 2,
                    name: 'Finance & Accounting'
                }
            ]
        };
        const mockProcessedTrails = [
            { label: 'Home', href: '/' },
            { anything: 'not relevant' },
            { label: 'Finance & Accounting', href: '/business-finance/finance-accounting' }
        ];
        const received = mapBreadcrumbJsonLd(mockProcessedTrails);

        expect(received).toEqual(expected);
    });

    it('should map correctly when expected payload is valid', () => {
        const expected = {
            '@context': 'https://schema.org',
            '@type': 'BreadcrumbList',
            itemListElement: [
                {
                    '@type': 'ListItem',
                    position: 1,
                    name: 'Home',
                    item: 'http://mocked-base-url:3000/'
                },
                {
                    '@type': 'ListItem',
                    position: 2,
                    name: 'Business & Finance',
                    item: 'http://mocked-base-url:3000/business-finance'
                },
                {
                    '@type': 'ListItem',
                    position: 3,
                    name: 'Finance & Accounting'
                }
            ]
        };
        const mockProcessedTrails = [
            { label: 'Home', href: '/' },
            { label: 'Business & Finance', href: '/business-finance' },
            { label: 'Finance & Accounting' }
        ];
        const received = mapBreadcrumbJsonLd(mockProcessedTrails);

        expect(received).toEqual(expected);
    });

    it('should not prepend base url to absolute path', () => {
        const expected = {
            '@context': 'https://schema.org',
            '@type': 'BreadcrumbList',
            itemListElement: [
                {
                    '@type': 'ListItem',
                    position: 1,
                    name: 'Home',
                    item: 'https://online.rmit.edu.au/'
                },
                {
                    '@type': 'ListItem',
                    position: 2,
                    name: 'Business & Finance',
                    item: 'https://online.rmit.edu.au/business-finance'
                },
                {
                    '@type': 'ListItem',
                    position: 3,
                    name: 'Finance & Accounting'
                }
            ]
        };
        const mockProcessedTrails = [
            { label: 'Home', href: 'https://online.rmit.edu.au/' },
            { label: 'Business & Finance', href: 'https://online.rmit.edu.au/business-finance' },
            { label: 'Finance & Accounting' }
        ];
        const received = mapBreadcrumbJsonLd(mockProcessedTrails);

        expect(received).toEqual(expected);
    });
});

describe('#mapCourseJsonLd', () => {
    const EXPECTED_BASE_JSON = {
        '@context': 'https://schema.org',
        '@type': 'Course',
        Provider: {
            '@type': 'CollegeOrUniversity',
            name: 'RMIT Online'
        },
        isAccessibleForFree: 'http://schema.org/False',
        educationalCredentialAwarded: 'http://schema.org/True',
        inLanguage: [ 'en' ]
    };

    it('should map data into json-ld format with future skills short course or program / bundle API response data', () => {
        const expected = {
            ...EXPECTED_BASE_JSON,
            courseCode: 'hea102',
            name: 'Digital Health Strategy and Change',
            description: 'Learn how digital health technologies can drive transformations within your organisation...',
            Provider: {
                ...EXPECTED_BASE_JSON.Provider,
                url: 'https://depends-on.gql-res-data.com/course/sc-digital-health-strategy-and-change-hea102'
            }
        };
        const mockCourseDetailsContext = {
            entityMetatags: [
                {
                    key: 'canonical',
                    value: 'https://depends-on.gql-res-data.com/course/sc-digital-health-strategy-and-change-hea102'
                },
                {
                    key: 'og:description',
                    value: 'Learn how digital health technologies can drive transformations within your organisation...'
                }
            ],
            entityLabel: 'Digital Health Strategy and Change',
            fieldSalesforceCourseId: 'HEA102'
        };

        const received = mapCourseJsonLd(mockCourseDetailsContext);

        expect(received).toEqual(expected);
    });

    it('should map data into json-ld format with in-house accredit course API response data', () => {
        const expected = {
            ...EXPECTED_BASE_JSON,
            courseCode: 'gc036',
            name: 'Graduate Certificate in Cyber Security',
            description: 'Develop the strategic and technical skills to solve complex cyber security issues...',
            Provider: {
                ...EXPECTED_BASE_JSON.Provider,
                url: 'https://depends-on.gql-res-data.com/course/pg-graduate-certificate-cyber-security-gc036'
            }
        };
        const mockCourseDetailsContext = {
            entityMetatags: [
                {
                    key: 'canonical',
                    value: 'https://depends-on.gql-res-data.com/course/pg-graduate-certificate-cyber-security-gc036'
                },
                {
                    key: 'og:description',
                    value: 'Develop the strategic and technical skills to solve complex cyber security issues...'
                },
            ],
            entityLabel: 'Graduate Certificate in Cyber Security',
            fieldSalesforceProgramId: 'GC036'
        };

        const received = mapCourseJsonLd(mockCourseDetailsContext);

        expect(received).toEqual(expected);
    });
});

describe('#mapOrgContactLd', () => {
    it('should return static data as expected', () => {
        const expected = {
            '@context': 'https://schema.org',
            '@type': 'CollegeOrUniversity',
            name: 'RMIT Online',
            alternateName: 'RMIT University',
            url: 'https://online.rmit.edu.au/',
            logo: 'https://online.rmit.edu.au/themes/custom/rmit/images/RMIT-Online-Primary.png',
            contactPoint: {
                '@type': 'ContactPoint',
                telephone: '1300 145 032',
                contactType: 'customer service',
                areaServed: 'AU',
                availableLanguage: 'en'
            },
            sameAs: [
                'https://www.facebook.com/RMITOnline/',
                'https://twitter.com/RMITOnline',
                'https://www.instagram.com/rmitonline/',
                'https://www.youtube.com/channel/UCK1hm1z_Z62f6MldNqaoy6g',
                'https://www.linkedin.com/school/rmitonline/'
            ]
        };

        const received = mapOrgContactLd();

        expect(received).toEqual(expected);
    });
});