/**
 * This mapper was designed to map response data to
 * support `<rmit-course-card>` component.
 *
 * @param { Array<Object> } categoriesCourseList
 */
import { timeCommitmentByContentType } from '@/utils/helpers'
import { getStartDateInfo } from '~/utils/transformers/course'
import assertIsCourseDateless from '~/utils/helpers/methods/assert-is-course-dateless'

// - fieldCourseTagType.entity.name V.S. fieldCourseTagType.entity.entityLabel
// - Without totalBundleCourses V.S. With totalBundleCourses
// - entityUrl.path / fieldExternalLink.uri V.S. path.alias / fieldExternalLink.uri
// - courseTitle V.S. title
export const mapCourseResultsToCourseCardProps = coursesResults =>
    coursesResults.map(result => {
        const { machineName, name } = result?.fieldCourseTagType?.entity || {};

        return {
            id: result?.entityId || '',
            url: result?.fieldExternalLink?.uri || result?.entityUrl?.path || '',
            heroImageUrl: result?.fieldHeroImage?.derivative?.url || '',
            courseTitle: result?.entityLabel || '',
            courseType: name || '',
            courseCategoryMachineName: machineName || '',
            timeCommitment: timeCommitmentByContentType[result?.entityBundle]?.(result) || '',
            startDate: getStartDateInfo(
                result?.fieldActiveInstances?.entities,
                undefined,
                !assertIsCourseDateless(result)
            )
        }
    });