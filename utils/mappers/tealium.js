import { formatDate } from '@/utils/date';

export const mapUTagCourseNextStartDate = data => {
    const rawStartDate = data?.fieldActiveInstances?.entities?.[0]?.fieldStartDate?.date;

    return rawStartDate
        ? [ formatDate(rawStartDate, 'YYYY-MM-DD') ]
        : [];
}