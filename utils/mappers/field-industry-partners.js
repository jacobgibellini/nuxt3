import { isArrayAndHasChild } from '@/utils/array'

export const mapFieldIndustryPartners = (fieldIndustryPartners = {}) => {
    const { 
        fieldHeading: title, 
        fieldSubheading: description, 
        fieldIndustryPartnersList: partners = {}
    } = fieldIndustryPartners

    return {
        title,
        description: description?.processed, // This will be returned as a HTML string
        partners: mapPartnerList(partners?.entity?.itemsOfEntitySubqueueLandingPagePartnerLists)
    }
}

export const mapPartnerList = (partnerList = []) =>
    isArrayAndHasChild(partnerList)
        ? partnerList.map(partnerEntity => {
            const { entityId: id, fieldIndustryLogo } = partnerEntity?.entity || {}
            const { url: src, alt } = fieldIndustryLogo 
    
            return {
                id,
                src,
                alt
            }
        })
        : []