import { mapTestimonialsToReviewItems } from './field-client-testimonials-list';

describe('#mapTestimonialsToReviewItems', () => {
    it('should map to "RmitoTestimonial" props', () => {
        const expected = [
            {
                id: 'client_testimonial-594',
                image: '//assets.heha.com/mock-image-1.png?itok=jJxp3l69',
                name: 'J Mon',
                title: 'Mocked Client Designation 1',
                quote: '<p>Lorem ipsum dolor sit amet, sed 1.</p>',
            },
            {
                id: '',
                image: '',
                name: '',
                title: '',
                quote: '',
            },
            {
                id: '',
                image: '',
                name: '',
                title: '',
                quote: '',
            },
        ];
        const itemsOfEntitySubqueueLandingPageTestimonialsList = [
            {
                entity: {
                    entityId: '594',
                    entityBundle: 'client_testimonial',
                    entityLabel: 'J Mon',
                    body: {
                        processed: '<p>Lorem ipsum dolor sit amet, sed 1.</p>',
                    },
                    fieldClientDesignation: 'Mocked Client Designation 1',
                    fieldClientLogo: {
                        derivative: {
                            url: '//assets.heha.com/mock-image-1.png?itok=jJxp3l69',
                        },
                    },
                },
            },
            {
                entity: {
                    entityId: '595',
                    body: '<p>Lorem ipsum dolor sit amet, sed 2.</p>',
                    fieldClientLogo: {
                        derivative: '//assets.heha.com/mock-image-2.png?itok=jJxp3l69',
                    },
                },
            },
            {
                entity: {
                    entityBundle: 'client_testimonial',
                    fieldClientLogo: '//assets.heha.com/mock-image-3.png?itok=jJxp3l69',
                },
            },
        ];

        const actual = mapTestimonialsToReviewItems(
            itemsOfEntitySubqueueLandingPageTestimonialsList
        );

        expect(actual).toEqual(expected);
    });
});
