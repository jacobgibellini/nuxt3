import { isArrayAndHasChild } from '@/utils/array';

export const mapIndustryPartnersLogo = (results = []) =>
    isArrayAndHasChild(results)
        ? results.reduce((logos, x) => {
            const next = x?.fieldIndustryLogo?.url
                ? [ {
                    url: x.fieldIndustryLogo.url,
                    alt: x.fieldIndustryLogo.alt || ''
                 } ]
                : [];

            return [
                ...logos,
                ...next
            ];
          }, [])
        : [];