import { mapProgramOverviewCourses } from './inhouse-accredited-course-details';

describe('#mapProgramOverviewCourses', () => {
    it('should return empty array', () => {
        // Arrange
        const initial = [];
        const expected = [];

        // Act
        const result = mapProgramOverviewCourses(initial);

        // Assert
        expect(result).toEqual(expected);
    });

    it('should return empty array when given incorrect arg type', () => {
        // Arrange
        const initial = {};
        const expected = [];

        // Act
        const result = mapProgramOverviewCourses(initial);

        // Assert
        expect(result).toEqual(expected);
    });

    it('should return empty array when first and only program course populaed with falsey values', () => {
        // Arrange
        const initial = [
            {
                entity: {
                    fieldCourseName: null,
                    type: {
                        targetId: 'program_course',
                    },
                    fieldProgramCourseSummary: {
                        processed: '',
                    },
                    fieldTitle: null,
                    fieldTitleFutureskills: null,
                },
            },
        ];
        const expected = [];

        // Act
        const result = mapProgramOverviewCourses(initial);

        // Assert
        expect(result).toEqual(expected);
    });

    it('should return mapped program courses', () => {
        // Arrange
        const initial = [
            {
                entity: {
                    fieldCourseName: 'Field Course Name',
                    type: {
                        targetId: 'program_course',
                    },
                    fieldProgramCourseSummary: {
                        processed: 'I need a <br/>',
                    },
                    fieldTitle: null,
                    fieldTitleFutureskills: null,
                },
            },
        ];
        const expected = [
            {
                id: '0',
                expanded: true,
                label: 'Field Course Name',
                targetId: 'program_course',
                contentHtml: 'I need a <br/>',
                footer: [],
            },
        ];

        // Act
        const result = mapProgramOverviewCourses(initial);

        // Assert
        expect(result).toEqual(expected);
    });

    it('should return mapped program courses with empty strings for fields that are not provided', () => {
        // Arrange
        const initial = [
            {
                entity: {},
            },
        ];
        const expected = [];

        // Act
        const result = mapProgramOverviewCourses(initial);

        // Assert
        expect(result).toEqual(expected);
    });

    it('should only set the "expanded" property to true for the first mapped item', () => {
        // Arrange
        const initial = [
            {
                entity: {
                    fieldCourseName: 'Field Course Name 1',
                    type: {
                        targetId: 'program_course',
                    },
                    fieldProgramCourseSummary: {
                        processed: '<p>this is a summary</p>',
                    },
                    fieldTitle: null,
                    fieldTitleFutureskills: null,
                },
            },
            {
                entity: {
                    fieldCourseName: 'Field Course Name 2',
                    type: {
                        targetId: 'program_course',
                    },
                    fieldProgramCourseSummary: {
                        processed: '<p>this is a summary</p>',
                    },
                    fieldTitle: null,
                    fieldTitleFutureskills: null,
                },
            },
            {
                entity: {
                    fieldCourseName: 'Field Course Name 3',
                    type: {
                        targetId: 'program_course',
                    },
                    fieldProgramCourseSummary: {
                        processed: '<p>this is a summary</p>',
                    },
                    fieldTitle: null,
                    fieldTitleFutureskills: null,
                },
            },
        ];
        const expected = [
            {
                id: '0',
                expanded: true,
                label: 'Field Course Name 1',
                targetId: 'program_course',
                contentHtml: '<p>this is a summary</p>',
                footer: [],
            },
            {
                id: '1',
                label: 'Field Course Name 2',
                targetId: 'program_course',
                contentHtml: '<p>this is a summary</p>',
                footer: [],
            },
            {
                id: '2',
                label: 'Field Course Name 3',
                targetId: 'program_course',
                contentHtml: '<p>this is a summary</p>',
                footer: [],
            },
        ];

        // Act
        const result = mapProgramOverviewCourses(initial);

        // Assert
        expect(result).toEqual(expected);
    });

    describe('Footer Array', () => {
        it('should return empty footer when fieldTitle and fieldTitleFutueskills are empty', () => {
            // Arrange
            const initial = [
                {
                    entity: {
                        fieldCourseName: 'Field Course Name',
                        type: {
                            targetId: 'program_course',
                        },
                        fieldProgramCourseSummary: {
                            processed: 'Hello world',
                        },
                        fieldTitle: null,
                        fieldTitleFutureskills: null,
                    },
                },
            ];
            const expected = [];

            // Act
            const result = mapProgramOverviewCourses(initial);

            // Assert
            expect(result[0].footer).toEqual(expected);
        });

        it('should return mapped footer when fieldTitle contains a value', () => {
            // Arrange
            const initial = [
                {
                    entity: {
                        fieldCourseName: 'Field Course Name',
                        type: {
                            targetId: 'program_course',
                        },
                        fieldProgramCourseSummary: {
                            processed: 'Hello world',
                        },
                        fieldTitle: 'Digital Leadership',
                        fieldTitleFutureskills: null,
                    },
                },
            ];
            const expected = ['Digital Leadership'];

            // Act
            const result = mapProgramOverviewCourses(initial);

            // Assert
            expect(result[0].footer).toEqual(expected);
        });

        it('should return mapped footer when fieldTitle and fieldTitleFutureskills contain a value', () => {
            // Arrange
            const initial = [
                {
                    entity: {
                        fieldCourseName: 'Field Course Name',
                        type: {
                            targetId: 'program_course',
                        },
                        fieldProgramCourseSummary: {
                            processed: 'Hello world',
                        },
                        fieldTitle: 'Digital Leadership + Customer Experience Strategy & Design',
                        fieldTitleFutureskills: 'Digital Leadership + Digital Delivery with Agile',
                    },
                },
            ];
            const expected = [
                'Digital Leadership + Customer Experience Strategy & Design',
                'Digital Leadership + Digital Delivery with Agile',
            ];

            // Act
            const result = mapProgramOverviewCourses(initial);

            // Assert
            expect(result[0].footer).toEqual(expected);
        });

        it('should NOT return mapped footer when fieldTitle is null and fieldTitleFutureskills contains a value', () => {
            // Arrange
            const initial = [
                {
                    entity: {
                        fieldCourseName: 'Field Course Name',
                        type: {
                            targetId: 'program_course',
                        },
                        fieldProgramCourseSummary: {
                            processed: 'Hello world',
                        },
                        fieldTitle: null,
                        fieldTitleFutureskills: 'Digital Leadership',
                    },
                },
            ];
            const expected = [];

            // Act
            const result = mapProgramOverviewCourses(initial);

            // Assert
            expect(result[0].footer).toEqual(expected);
        });
    });
});
