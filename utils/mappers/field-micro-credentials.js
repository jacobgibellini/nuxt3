export const mapMicroCredentialsToCategoryCardsPod = ({
    fieldMicroCredentials,
    fieldSidebarColour
}) =>
    fieldMicroCredentials.map(({ entity = {} }) => ({
        label: entity.fieldLabel,
        description: entity.fieldDescription?.processed,
        color: fieldSidebarColour?.color
    }));