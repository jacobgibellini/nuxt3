import { mapMicroCredentialsToCategoryCardsPod } from './field-micro-credentials';

describe('#mapMicroCredentialsToCategoryCardsPod', () => {
    it('should map "fieldMicroCredentials" to "cards" prop compatible structure for "RmitoCategoryCardsPod" component', () => {
        const expected = [
            {
                label: 'Agile Ways of Working',
                description: 'Agile methodology in business',
                color: '#000',
            },
            {
                label: 'Business Problem Solving',
                description: '<p>Find solutions to any problem for any business</p>',
                color: '#000',
            },
        ];
        const payload = {
            fieldMicroCredentials: [
                {
                    entity: {
                        fieldLabel: 'Agile Ways of Working',
                        fieldDescription: {
                            processed: 'Agile methodology in business',
                        },
                    },
                },
                {
                    entity: {
                        fieldLabel: 'Business Problem Solving',
                        fieldDescription: {
                            processed: '<p>Find solutions to any problem for any business</p>',
                        },
                    },
                },
            ],
            fieldSidebarColour: {
                color: '#000',
            },
        };

        const actual = mapMicroCredentialsToCategoryCardsPod(payload);

        expect(actual).toEqual(expected);
    });

    it('should return undefined for "color" when "fieldSidebarColour" does not have "color" property', () => {
        const expected = [
            {
                label: 'Color Test Label',
                description: 'For Testing Color Only',
                color: undefined,
            },
        ];
        const payload = {
            fieldMicroCredentials: [
                {
                    entity: {
                        fieldLabel: 'Color Test Label',
                        fieldDescription: {
                            processed: 'For Testing Color Only',
                        },
                    },
                },
            ],
            fieldSidebarColour: {},
        };

        const actual = mapMicroCredentialsToCategoryCardsPod(payload);

        expect(actual).toEqual(expected);
    });

    it('should return undefined for "color" when "fieldSidebarColour" is not defined', () => {
        const expected = [
            {
                label: 'Color Test Label',
                description: 'For Testing Color Only',
                color: undefined,
            },
        ];
        const payload = {
            fieldMicroCredentials: [
                {
                    entity: {
                        fieldLabel: 'Color Test Label',
                        fieldDescription: {
                            processed: 'For Testing Color Only',
                        },
                    },
                },
            ],
        };

        const actual = mapMicroCredentialsToCategoryCardsPod(payload);

        expect(actual).toEqual(expected);
    });
});
