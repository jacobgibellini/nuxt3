import { getRatio } from '@/utils/helpers/methods/get-ratio'
import { isArrayAndHasChild } from '@/utils/array'

const NUMERIC_VALUE_FORMATS = {
    RATIO: 'ratio',
    PERCENTAGE: 'percentage',
    MULTIPLIER: 'multiplier'
}

const mapNumberValues = (numericValues = []) => {
    if (!isArrayAndHasChild(numericValues)) {
        return []
    }

    return numericValues.map(stat => {
        const numericValue = stat?.entity || {}
        const {
            fieldNumericValueType: type,
            fieldNumericValue: value,
            fieldOverrideLabel: overrideLabel,
            fieldLabel = '',
            fieldNumericValueItemColor
        } = numericValue
        
        if (!value || !Object.values(NUMERIC_VALUE_FORMATS).includes(type)) {
            return
        }

        const valueProps = {
            [ NUMERIC_VALUE_FORMATS.RATIO ]      : () => formatRatio(+value),
            [ NUMERIC_VALUE_FORMATS.PERCENTAGE ] : () => formatPercentage(+value),
            [ NUMERIC_VALUE_FORMATS.MULTIPLIER ] : () => formatMultiplier(+value)
        }[ type ]()

        if (overrideLabel) {
            valueProps.valueDisplay = overrideLabel
        }

        return {
            ...valueProps,
            label: fieldLabel,
            color: fieldNumericValueItemColor?.color || ""
        }
    })
    .filter(x => x !== undefined)
}

const formatPercentage = (numericValue) => {
    if (!numericValue) {
        return {}
    }

    return {
        value: numericValue,
        valueDisplay: numericValue + '%'
    }
}

const formatRatio = (numericValue) => {
    if (!numericValue) {
        return {}
    }

    const ratio = getRatio(numericValue, 100)

    return {
        value: numericValue,
        valueDisplay: `${ ratio.num1 } in ${ ratio.num2 }`
    }
}

const formatMultiplier = (numericValue) => {
    if (!numericValue) {
        return {}
    }

    return {
        value: 100,
        valueDisplay: numericValue + 'X'
    }
}

export { mapNumberValues, formatPercentage, formatRatio, formatMultiplier }