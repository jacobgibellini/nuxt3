import { mapNumberValues, formatPercentage, formatRatio, formatMultiplier } from './field-numeric-value-items'

describe('#mapNumberValues', () => {
    it('should return an empty array when nothing is passed', () => {
        // Arrange
        const initial = undefined
        const expected = []

        // Act
        const result = mapNumberValues(initial)

        // Assert
        expect(result).toEqual(expected)
    })

    it('should return an empty array when array contains falsy values', () => {
        // Arrange
        const initial = [ {}, false, [], null, undefined ]
        const expected = []

        // Act
        const result = mapNumberValues(initial)

        // Assert
        expect(result).toEqual(expected)
    })

    it('should map numeric value fields', () => {
        // Arrange
        const initial = [
            {
                "entity": {
                    "entityId": "14321",
                    "entityBundle": "numeric_value_item",
                    "fieldNumericValue": "75.0",
                    "fieldLabel": "Australians said their day-to-day tasks are different to what they were before the pandemic.",
                    "fieldNumericValueType": "ratio",
                    "fieldOverrideLabel": null,
                    "fieldNumericValueItemColor": {
                        "color": "#FFCD60"
                    }
                }
            },
            {
                "entity": {
                    "entityId": "14326",
                    "entityBundle": "numeric_value_item",
                    "fieldNumericValue": "5.0",
                    "fieldLabel": "Australians said they were worried about being made redundant.",
                    "fieldNumericValueType": "multiplier",
                    "fieldOverrideLabel": null,
                    "fieldNumericValueItemColor": {
                        "color": "#44FFC5"
                    }
                }
            },
            {
                "entity": {
                    "entityId": "14331",
                    "entityBundle": "numeric_value_item",
                    "fieldNumericValue": "33.3",
                    "fieldLabel": "Australians felt they didn’t have the skills they need.",
                    "fieldNumericValueType": "percentage",
                    "fieldOverrideLabel": null,
                    "fieldNumericValueItemColor": {
                        "color": "#7596FF"
                    }
                }
            }
        ]
        const expected = [
            {
                "color": "#FFCD60", 
                "label": "Australians said their day-to-day tasks are different to what they were before the pandemic.", 
                "value": 75, 
                "valueDisplay": "3 in 4"
            }, 
            {
                "color": "#44FFC5", 
                "label": "Australians said they were worried about being made redundant.", 
                "value": 100, 
                "valueDisplay": "5X"
            }, 
            {
                "color": "#7596FF", 
                "label": "Australians felt they didn’t have the skills they need.", 
                "value": 33.3, 
                "valueDisplay": "33.3%"
            }
        ]

        // Act
        const result = mapNumberValues(initial)

        // Assert
        expect(result).toEqual(expected)
    })

    it('should remove numeric value fields that do not have a type', () => {
        // Arrange
        const initial = [
            {
                "entity": {
                    "entityId": "14321",
                    "entityBundle": "numeric_value_item",
                    "fieldNumericValue": "75.0",
                    "fieldLabel": "Australians said their day-to-day tasks are different to what they were before the pandemic.",
                    "fieldNumericValueType": "ratio",
                    "fieldOverrideLabel": null,
                    "fieldNumericValueItemColor": {
                        "color": "#FFCD60"
                    }
                }
            },
            {
                "entity": {
                    "entityId": "14326",
                    "entityBundle": "numeric_value_item",
                    "fieldNumericValue": "5.0",
                    "fieldLabel": "Australians said they were worried about being made redundant.",
                    "fieldNumericValueType": "multiplier",
                    "fieldOverrideLabel": null,
                    "fieldNumericValueItemColor": {
                        "color": "#44FFC5"
                    }
                }
            },
            // This item does not have a valid type
            {
                "entity": {
                    "entityId": "14331",
                    "entityBundle": "numeric_value_item",
                    "fieldNumericValue": "33.3",
                    "fieldLabel": "Australians felt they didn’t have the skills they need.",
                    "fieldNumericValueType": "",
                    "fieldOverrideLabel": null,
                    "fieldNumericValueItemColor": {
                        "color": "#7596FF"
                    }
                }
            }
        ]
        const expected = [
            {
                "color": "#FFCD60", 
                "label": "Australians said their day-to-day tasks are different to what they were before the pandemic.", 
                "value": 75, 
                "valueDisplay": "3 in 4"
            }, 
            {
                "color": "#44FFC5", 
                "label": "Australians said they were worried about being made redundant.", 
                "value": 100, 
                "valueDisplay": "5X"
            }
        ]

        // Act
        const result = mapNumberValues(initial)

        // Assert
        expect(result).toEqual(expected)
    })

    it('should remove numeric value fields that have an invalid type', () => {
        // Arrange
        const initial = [
            {
                "entity": {
                    "entityId": "14321",
                    "entityBundle": "numeric_value_item",
                    "fieldNumericValue": "75.0",
                    "fieldLabel": "Australians said their day-to-day tasks are different to what they were before the pandemic.",
                    "fieldNumericValueType": "ratio",
                    "fieldOverrideLabel": null,
                    "fieldNumericValueItemColor": {
                        "color": "#FFCD60"
                    }
                }
            },
            // This item does not have a valid type
            {
                "entity": {
                    "entityId": "14331",
                    "entityBundle": "numeric_value_item",
                    "fieldNumericValue": "33.3",
                    "fieldLabel": "Australians felt they didn’t have the skills they need.",
                    "fieldNumericValueType": "notavalidtype",
                    "fieldOverrideLabel": null,
                    "fieldNumericValueItemColor": {
                        "color": "#7596FF"
                    }
                }
            }
        ]
        const expected = [
            {
                "color": "#FFCD60", 
                "label": "Australians said their day-to-day tasks are different to what they were before the pandemic.", 
                "value": 75, 
                "valueDisplay": "3 in 4"
            }
        ]

        // Act
        const result = mapNumberValues(initial)

        // Assert
        expect(result).toEqual(expected)
    })

    it('should override the display label when fieldOverrideLabel is provided', () => {
        // Arrange
        const initial = [
            {
                "entity": {
                    "entityId": "14321",
                    "entityBundle": "numeric_value_item",
                    "fieldNumericValue": "75.0",
                    "fieldLabel": "Australians said their day-to-day tasks are different to what they were before the pandemic.",
                    "fieldNumericValueType": "ratio",
                    "fieldOverrideLabel": "3 out of 4",
                    "fieldNumericValueItemColor": {
                        "color": "#FFCD60"
                    }
                }
            }
        ]
        const expected = [
            {
                "color": "#FFCD60", 
                "label": "Australians said their day-to-day tasks are different to what they were before the pandemic.", 
                "value": 75, 
                "valueDisplay": "3 out of 4"
            }
        ]

        // Act
        const result = mapNumberValues(initial)

        // Assert
        expect(result).toEqual(expected)
    })

    it('should not map a color when fieldNumericValueItemColor is null', () => {
        // Arrange
        const initial = [
            {
                "entity": {
                    "entityId": "14321",
                    "entityBundle": "numeric_value_item",
                    "fieldNumericValue": "75.0",
                    "fieldLabel": "Australians said their day-to-day tasks are different to what they were before the pandemic.",
                    "fieldNumericValueType": "ratio",
                    "fieldOverrideLabel": null,
                    "fieldNumericValueItemColor": null
                }
            }
        ]
        const expected = [
            {
                "color": "", 
                "label": "Australians said their day-to-day tasks are different to what they were before the pandemic.", 
                "value": 75, 
                "valueDisplay": "3 in 4"
            }
        ]

        // Act
        const result = mapNumberValues(initial)

        // Assert
        expect(result).toEqual(expected)
    })
})

describe('#formatPercentage', () => {
    it('should return an empty object when value is not provided', () => {
        // Arrange
        const initialValue = null
        const expected = {}

        // Act
        const result = formatPercentage(initialValue)

        // Assert
        expect(result).toEqual(expected)
    })

    it('should format a numeric value into a percentage', () => {
        // Arrange
        const initialValue = 50
        const expected = {
            value: 50,
            valueDisplay: '50%'
        }

        // Act
        const result = formatPercentage(initialValue)

        // Assert
        expect(result).toEqual(expected)
    })
})

describe('#formatRatio', () => {
    it('should return an empty object when value is not provided', () => {
        // Arrange
        const initialValue = null
        const expected = {}

        // Act
        const result = formatRatio(initialValue)

        // Assert
        expect(result).toEqual(expected)
    })

    it('should format a numeric value into a percentage', () => {
        // Arrange
        const initialValue = 50
        const expected = {
            value: 50,
            valueDisplay: '1 in 2'
        }

        // Act
        const result = formatRatio(initialValue)

        // Assert
        expect(result).toEqual(expected)
    })
})

describe('#formatMultiplier', () => {
    it('should return an empty object when value is not provided', () => {
        // Arrange
        const initialValue = null
        const expected = {}

        // Act
        const result = formatMultiplier(initialValue)

        // Assert
        expect(result).toEqual(expected)
    })

    it('should format a numeric value into a percentage', () => {
        // Arrange
        const value = 5
        const expected = {
            value: 100,
            valueDisplay: '5X'
        }

        // Act
        const result = formatMultiplier(value)

        // Assert
        expect(result).toEqual(expected)
    })
})