import { mapFieldTabItemsToRmitoTab } from './field-tab-items';

describe('#mapFieldTabItemsToRmitoTab', () => {
    it('should map "fieldLabel" to "title" as prop for <rmito-tab>', () => {
        const expected = [
            {
                id: null,
                title: 'Future Skills',
                fieldContent: undefined
            }
        ];
        const fieldTabItems = [
            {
                entity: {
                    fieldLabel: 'Future Skills'
                }
            }
        ];

        const actual = mapFieldTabItemsToRmitoTab(fieldTabItems);

        expect(actual).toEqual(expected);
    });

    it('should create id with "entityBundle" and "entityId"', () => {
        const expected = [
            {
                id: 'tab_item-9431',
                title: '',
                description: undefined,
                fieldContent: []
            }
        ];
        const fieldTabItems = [
            {
                entity: {
                    entityId: '9431',
                    entityBundle: 'tab_item',
                    fieldLabel: '',
                    fieldContent: []
                }
            }
        ];

        const actual = mapFieldTabItemsToRmitoTab(fieldTabItems);

        expect(actual).toEqual(expected);
    });

    it('should retain "fieldContent" data same as original', () => {
        const expected = [
            {
                id: null,
                title: undefined,
                description: undefined,
                fieldContent: [
                    1,
                    null,
                    { entity: {} }
                ]
            }
        ];
        const fieldTabItems = [
            {
                entity: {
                    fieldContent: [
                        1,
                        null,
                        { entity: {} }
                    ]
                }
            }
        ];

        const actual = mapFieldTabItemsToRmitoTab(fieldTabItems);

        expect(actual).toEqual(expected);
    });

    it('should resturn description', () => {
        const expected = [
            {
                id: null,
                title: undefined,
                description: '<p>Some HTML description</p>',
                fieldContent: []
            }
        ];
        const fieldTabItems = [
            {
                entity: {
                    fieldDescription: {
                        processed: '<p>Some HTML description</p>'
                    },
                    fieldContent: []
                }
            }
        ];

        const actual = mapFieldTabItemsToRmitoTab(fieldTabItems);

        expect(actual).toEqual(expected);
    })
});
