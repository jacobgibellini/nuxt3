import { mapUTagCourseNextStartDate } from './tealium';

describe('#mapUTagCourseNextStartDate', () => {
    it('should format and map raw start date to utag in array format', () => {
        const expected = [ '2021-12-25' ];
        const mockData = {
            fieldActiveInstances: {
                entities: [
                    {
                        fieldStartDate: {
                            date: '2021-12-25 12:00:00 UTC'
                        }
                    }
                ]
            }
        };

        const received = mapUTagCourseNextStartDate(mockData);

        expect(received).toEqual(expected);
    });

    it('should always format and map the first raw start dates (when more than 1 start dates) to utag in array format', () => {
        const expected = [ '2021-02-08' ];
        const mockData = {
            fieldActiveInstances: {
                entities: [
                    {
                        fieldStartDate: {
                            date: '2021-02-08 12:00:00 UTC'
                        }
                    },
                    {
                        fieldStartDate: {
                            date: '2021-04-12 12:00:00 UTC'
                        }
                    }
                ]
            }
        };

        const received = mapUTagCourseNextStartDate(mockData);

        expect(received).toEqual(expected);
    });

    it.each`
    description                                                      | invalidData
    ${'a string value'}                                              | ${'a string'}
    ${'null'}                                                        | ${null}
    ${'fieldActiveInstances property not found'}                     | ${{ noFieldActiveInstances: { entities: [ { fieldStartDate: { date: '2021-02-08 12:00:00 UTC' } } ] } } }}
    ${'fieldActiveInstances is an empty object'}                     | ${{ fieldActiveInstances: {} }}
    ${'entities is an empty array'}                                  | ${{ fieldActiveInstances: { entities: [] } }}
    ${'entities is an empty object'}                                 | ${{ fieldActiveInstances: { entities: {} } }}
    ${'entities is an array with an empty object'}                   | ${{ fieldActiveInstances: { entities: [ {} ] } }}
    ${'entities is a string'}                                        | ${{ fieldActiveInstances: { entities: 'entities is string' } }}
    ${'first entity object expected start date structure incorrect'} | ${{ fieldActiveInstances: { entities: [ { fieldStartDate: '2021-02-08 12:00:00 UTC' } ] } }}
    ${'first entity object start date value is Boolean - false'}     | ${{ fieldActiveInstances: { entities: [ { fieldStartDate: { date: false } } ] } }}
    ${'first entity object start date value is null'}                | ${{ fieldActiveInstances: { entities: [ { fieldStartDate: { date: null } } ] } }}
    ${'first entity object start date value is Number - 0'}          | ${{ fieldActiveInstances: { entities: [ { fieldStartDate: { date: 0 } } ] } }}
    ${'first entity object start date value is empty string'}        | ${{ fieldActiveInstances: { entities: [ { fieldStartDate: { date: '' } } ] } }}
    ${'first entity object expected start date structure incorrect'} | ${{ fieldActiveInstances: { entities: [ { date: '2021-02-08 12:00:00 UTC' } ] } }}
    `('should return empty array when data is $description', ({
        description,
        invalidData
    }) => {
        const expected = [];
        const mockData = invalidData;

        const received = mapUTagCourseNextStartDate(mockData);

        expect(received).toEqual(expected);
    });
});