export const mapTypenameToMetaPropertyName = typename =>
    typename === 'MetaProperty'
        ? 'property'
        : 'name';