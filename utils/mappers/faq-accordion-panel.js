export const mapFaqDetailsToAccordionPanel = ({
    entityId: id,
    info: label,
    body
}) => ({
    id,
    label,
    contentHtml: body?.processed
});