import { formatDate } from '~/utils/date';
import isStringButNotEmpty from '~/utils/string/is-string-but-not-empty';

export const mapShortCoursesToCategoryCardsPod = ({
    fieldShortCoursesList,
    fieldSidebarColour
}) =>
    fieldShortCoursesList.map(({ entity = {} }) => {
        const nextStartDate     = entity.fieldNextStartDate?.date;
        const nextStartDateHtml = isStringButNotEmpty(nextStartDate)
            ? `Next start ${formatDate(nextStartDate)}`
            : '';
        const descriptionHtml   = entity.fieldCourseDescription?.processed || '';

        return {
            label: entity.title,
            description: nextStartDateHtml + descriptionHtml,
            url: entity.entityUrl?.path,
            color: fieldSidebarColour?.color
        }
    });