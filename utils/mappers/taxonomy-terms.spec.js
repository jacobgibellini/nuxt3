import { 
    mapSubCategoryToCategory,
    mapTermListResults,
    mapToCategoryReference,
    mapToAreaOfStudy
} from './taxonomy-terms';

describe('#mapSubCategoryToCategory', () => {
    it('should map sub category names to specified category name', () => {
        const expected = {
            'blockchain': 'fake_category_name',
            'data_analytics': 'fake_category_name'
        };

        const output = mapSubCategoryToCategory(
            [
                {
                    "machineName": "blockchain"
                },
                {
                    "machineName": "data_analytics"
                }
            ],
            'fake_category_name'
        );

        expect(output).toEqual(expected);
    })
});

describe('#mapTermListResults', () => {
    it('should map term list results data grouped by category (machine) name', () => {
        const expected = {
            'artificial_intelligence': 'artificial_intelligence',
            'business': 'business',
            'blockchain': 'business',
            'data_analytics': 'business',
            'data_science_0': 'data_science_0',
            'data_analytics_0': 'data_science_0',
            'design': 'design',
            'cx_design_strategy': 'design',
            'design_thinking': 'design',
            'ux_ui_design': 'design'
        };
        const termListResults = [
            {
                "entityLabel": "Artificial Intelligence",
                "machineName": "artificial_intelligence",
                "description": {
                    "processed": "<p>Explore the possibilities of AI and how this emerging technology can drive business value and shape the future of work.</p>\n"
                },
                "children": {
                    "entities": []
                }
            },
            {
                "entityLabel": "Business & Finance",
                "machineName": "business",
                "description": {
                    "processed": "<p>Equip yourself with skills as a modern leader and learn critical strategy, leadership, and finance skills to drive growth and innovation.</p>\n"
                },
                "children": {
                    "entities": [
                        {
                            "entityLabel": "Blockchain",
                            "machineName": "blockchain"
                        },
                        {
                            "entityLabel": "Data & Analytics",
                            "machineName": "data_analytics"
                        }
                    ]
                }
            },
            {
                "entityLabel": "Data Science & Analytics",
                "machineName": "data_science_0",
                "description": {
                    "processed": "<p>Deliver accurate forecasts, observe patterns, and extract meaningful insights that will drive value for your team or business.</p>\n"
                },
                "children": {
                    "entities": [
                        {
                            "entityLabel": "Data Analytics",
                            "machineName": "data_analytics_0"
                        }
                    ]
                }
            },
            {
                "entityLabel": "Design",
                "machineName": "design",
                "description": {
                    "processed": "<p>Craft human-centred solutions that are intuitive, user-friendly, visually appealing and address your customers&#039; pain points.</p>\n"
                },
                "children": {
                    "entities": [
                        {
                            "entityLabel": "CX Design & Strategy",
                            "machineName": "cx_design_strategy"
                        },
                        {
                            "entityLabel": "Design Thinking",
                            "machineName": "design_thinking"
                        },
                        {
                            "entityLabel": "UX & UI Design",
                            "machineName": "ux_ui_design"
                        }
                    ]
                }
            }
        ];

        const output = mapTermListResults(termListResults);

        expect(output).toEqual(expected);
    });
});

describe('#mapToCategoryReference', () => {
    it('should flattern categories', () => {
        const expected = {
            'design': 'design',
            'cx_design_strategy': 'design'
        };

        const output = mapToCategoryReference({
            "entityLabel": "Design",
            "machineName": "design",
            "children": {
                "entities": [
                    {
                        "entityLabel": "CX Design & Strategy",
                        "machineName": "cx_design_strategy"
                    }
                ]
            }
        });

        expect(output).toEqual(expected);
    });
});

describe('#mapToAreaOfStudy', () => {
    it('should map taxonomy term list results to properties for Area of Study Component', () => {
        const expected = [
            {
                label: 'Category Name 1',
                description: '<p>Explore the possibilities</p>',
                url: '/category-name-1'
            },
            {
                label: 'Category Name 2',
                description: '<p>how this emerging technology work.</p>',
                url: ''
            },
        ]
        const responseData = [
            {
                "entityLabel": "Category Name 1",
                "entityUrl": {
                    "path": "/category-name-1"
                },
                "machineName": "category_name_1",
                "description": {
                    "processed": "<p>Explore the possibilities</p>"
                }
            },
            {
                "entityLabel": "Category Name 2",
                "entityUrl": "/category-name-2",
                "description": {
                    "processed": "<p>how this emerging technology work.</p>"
                }
            },
        ];

        const output = mapToAreaOfStudy(responseData);

        expect(output).toEqual(expected);
    });

    it('should map missing fields to empty string', () => {
        const expected = [
            {
                label: 'Category Name 1',
                description: '<p>Explore the possibilities</p>',
                url: ''
            },
            {
                label: '',
                description: '<p>how this emerging technology work.</p>',
                url: '/category-name-2'
            },
            {
                label: '',
                description: '',
                url: '/category-name-3'
            },
            {
                label: '',
                description: '',
                url: ''
            },
            {
                label: '',
                description: '',
                url: ''
            }
        ]
        const responseData = [
            {
                "entityLabel": "Category Name 1",
                "description": {
                    "processed": "<p>Explore the possibilities</p>"
                }
            },
            {
                "entityUrl": {
                    "path": "/category-name-2"
                },
                "description": {
                    "processed": "<p>how this emerging technology work.</p>"
                }
            },
            {
                "entityUrl": {
                    "path": "/category-name-3"
                },
            },
            {
                "description": null
            },
            {
                "entityLabel": null,
            }
        ];

        const output = mapToAreaOfStudy(responseData);

        expect(output).toEqual(expected);
    });
});