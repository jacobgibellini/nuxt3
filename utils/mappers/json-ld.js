import isStringButNotEmpty from '~/utils/string/is-string-but-not-empty';

const JSON_LD_CONTEXT = 'https://schema.org';

export const mapOrgContactLd = () => ({
    '@context': JSON_LD_CONTEXT,
    '@type': 'CollegeOrUniversity',
    name: 'RMIT Online',
    alternateName: 'RMIT University',
    url: 'https://online.rmit.edu.au/',
    logo: 'https://online.rmit.edu.au/themes/custom/rmit/images/RMIT-Online-Primary.png',
    contactPoint: {
        '@type': 'ContactPoint',
        telephone: '1300 145 032',
        contactType: 'customer service',
        areaServed: 'AU',
        availableLanguage: 'en'
    },
    sameAs: [
        'https://www.facebook.com/RMITOnline/',
        'https://twitter.com/RMITOnline',
        'https://www.instagram.com/rmitonline/',
        'https://www.youtube.com/channel/UCK1hm1z_Z62f6MldNqaoy6g',
        'https://www.linkedin.com/school/rmitonline/'
    ]
});

export const mapCourseJsonLd = courseDetailsContext => {
    const courseCode  = courseDetailsContext?.fieldSalesforceCourseId?.toLowerCase?.() || courseDetailsContext?.fieldSalesforceProgramId?.toLowerCase?.();
    const description = courseDetailsContext?.entityMetatags?.find?.(({ key }) => key === 'og:description')?.value;
    const name        = courseDetailsContext?.entityLabel;
    const providerUrl = courseDetailsContext?.entityMetatags?.find?.(({ key }) => key === 'canonical')?.value;

    return {
        '@context': JSON_LD_CONTEXT,
        '@type': 'Course',
        ...courseCode && { courseCode },
        ...name && { name },
        ...description && { description },
        Provider: {
            '@type': 'CollegeOrUniversity',
            name: 'RMIT Online',
            ...providerUrl && { url: providerUrl }
        },
        isAccessibleForFree: 'http://schema.org/False',
        educationalCredentialAwarded: 'http://schema.org/True',
        inLanguage: [ 'en' ]
    }
}

export const mapBreadcrumbJsonLd = processedTrails => ({
    '@context': JSON_LD_CONTEXT,
    '@type': 'BreadcrumbList',
    itemListElement: processedTrails.reduce(
        (
            list,
            trail,
            index
        ) => {
            const isNameValid = isStringButNotEmpty(trail?.label?.trim?.());
            const isItemValid = isStringButNotEmpty(trail?.href?.trim?.());
            const isLastItem  = processedTrails.length === index + 1;

            if (!isNameValid) {
                return list;
            }

            const item = isItemValid && trail.href.startsWith('http')
                ? trail.href
                : process.env.baseUrl + trail.href;

            const nextTrail = {
                '@type': 'ListItem',
                position: list.length + 1,
                name: trail.label,
                ...isItemValid && !isLastItem && { item }
            };

            return [
                ...list,
                nextTrail
            ];
        },
        []
    )
});
