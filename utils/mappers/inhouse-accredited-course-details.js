import { isArrayAndHasChild } from '~/utils/array';

export const mapProgramOverviewCourses = (programOverviewCourses) => {
    if (!isArrayAndHasChild(programOverviewCourses)) {
        return [];
    }

    const mappedProgramOverviewCourses = programOverviewCourses
        .map((module, index) => {
            const { entity } = module || {};

            const footer = [];

            if (entity.fieldTitle) {
                footer.push(entity.fieldTitle.trim());

                // Existing behaviour:
                // Only display the following futureskills title if the first title exists
                if (entity.fieldTitleFutureskills) {
                    footer.push(entity.fieldTitleFutureskills.trim());
                }
            }

            return {
                id: index.toString(),
                targetId: entity.type?.targetId || '',
                label: entity.fieldCourseName || '',
                contentHtml: entity.fieldProgramCourseSummary?.processed || '',
                footer,
            };
        })
        .filter((module) => {
            return module.label && module.contentHtml;
        });

    // Always expand the first panel by default
    if (mappedProgramOverviewCourses.length > 0) {
        mappedProgramOverviewCourses[0].expanded = true;
    }

    return mappedProgramOverviewCourses;
};
