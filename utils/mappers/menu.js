export const mapMainMenuLinks = links =>
    links?.map?.(link => ({
        text: link?.label,
        path: link?.url?.path || '#!',
        links: link?.links || [],
        ...(link?.url?.path === '/courses' ? { event: 'clickHeaderCatalogue' } : {})
    })) || [];