import { mapFieldTopicInProductDetailsPage } from './field-topic';

describe('#mapFieldTopicInProductDetailsPage', () => {
    it('should invalid arguments return empty array', () => {
        const actual = mapFieldTopicInProductDetailsPage();

        expect(actual).toEqual([]);
    });

    it('should map all raw "fieldTopic" items to "x" component prop "collection" format', () => {
        const expected = [
            {
                label: 'Topic Label',
                href: '/some-topic-path'
            },
            {
                label: 'Topic Label 2',
                href: undefined
            }
        ];
        const fieldTopic = [
            {
                entity: {
                    entityLabel: 'Topic Label',
                    entityUrl: {
                        path: '/some-topic-path'
                    }
                }
            },
            {
                entity: {
                    entityLabel: 'Topic Label 2',
                    entityUrl: '/some-topic-path-2'
                }
            }
        ];

        const actual = mapFieldTopicInProductDetailsPage(fieldTopic);

        expect(actual).toEqual(expected);
    });
});