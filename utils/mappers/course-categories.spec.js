import { mapAreasOfStudy } from './course-categories'

describe('#mapAreasOfStudy', () => {
    it('should transform areas of study array', () => {
        // Arrange
        const areasOfStudy = [
            {
              "entityLabel": "Blockchain",
              "entityUrl": { "path": "/business-finance/blockchain" },
              "description": { "processed": "<p>When Forbes say blockchain is where the internet was 20 years ago, you sit up and take notice. Career opportunities for blockchain specialists are exploding. There is a huge demand for people who can think, design and develop all things blockchain. RMIT is the first Australian university to offer a comprehensive suite of blockchain courses partnering with industry to equip people with cutting-edge skills to succeed in this area.</p>" }
            },
            {
              "entityLabel": "Finance & Accounting",
              "entityUrl": { "path": "/business-finance/finance-accounting" },
              "description": { "processed": "<p>Develop a deep knowledge of integral business concepts and principles across key business units. Our programs are composed of practical and industry-relevant courses and the knowledge you learn can be applied immediately to impact your career and organisation.</p>" }
            }
        ]
        const expectedArray = [
            {
                "label": "Blockchain",
                "url":"/business-finance/blockchain",
                "description": "<p>When Forbes say blockchain is where the internet was 20 years ago, you sit up and take notice. Career opportunities for blockchain specialists are exploding. There is a huge demand for people who can think, design and develop all things blockchain. RMIT is the first Australian university to offer a comprehensive suite of blockchain courses partnering with industry to equip people with cutting-edge skills to succeed in this area.</p>"
            },
            {
                "label": "Finance & Accounting",
                "url":"/business-finance/finance-accounting",
                "description": "<p>Develop a deep knowledge of integral business concepts and principles across key business units. Our programs are composed of practical and industry-relevant courses and the knowledge you learn can be applied immediately to impact your career and organisation.</p>"
            }
        ]

        // Act
        const mappedArray = mapAreasOfStudy(areasOfStudy)

        // Assert
        expect(mappedArray).toEqual(expectedArray)
    })

    it('should transform areas of study array when object does not contain values', () => {
        // Arrange
        const areasOfStudy = [ {} ]
        const expectedArray = [
            {
                "label": "",
                "url":"",
                "description": ""
            }
        ]

        // Act
        const mappedArray = mapAreasOfStudy(areasOfStudy)

        // Assert
        expect(mappedArray).toEqual(expectedArray)
    })

    it('should return empty array when passing an empty array', () => {
        // Arrange
        const areasOfStudy = []
        const expectedArray = []

        // Act
        const mappedArray = mapAreasOfStudy(areasOfStudy)

        // Assert
        expect(mappedArray).toEqual(expectedArray)
    })

    it('should return empty array when passing an invalid value type', () => {
        // Arrange
        const areasOfStudy = 'this is a string'
        const expectedArray = []

        // Act
        const mappedArray = mapAreasOfStudy(areasOfStudy)

        // Assert
        expect(mappedArray).toEqual(expectedArray)
    })
})