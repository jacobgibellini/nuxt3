export const mapFieldTabItemsToRmitoTab = fieldTabItems =>
    fieldTabItems.map(({ entity = {} }) => {
        return {
            id: entity.entityId && entity.entityBundle ? `${entity.entityBundle}-${entity.entityId}` : null,
            title: entity.fieldLabel,
            description: entity.fieldDescription?.processed,
            fieldContent: entity.fieldContent
        }
    });