import { mapTypenameToMetaPropertyName } from './meta';

describe('#mapTypenameToMetaPropertyName', () => {
    it('should return property name of "property" when called with "MetaProperty"', () => {
        expect(mapTypenameToMetaPropertyName('MetaProperty'))
            .toBe('property');
    });

    it('should return property name of "name" when called with "MetaLink"', () => {
        expect(mapTypenameToMetaPropertyName('MetaProp'))
            .toBe('name');
    });
});