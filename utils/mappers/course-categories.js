import { isArrayAndHasChild } from '../array'

export const mapAreasOfStudy = areasOfStudyArr => {
    return isArrayAndHasChild(areasOfStudyArr) 
        ? areasOfStudyArr.map(x => ({
            description: x.description?.processed || '',
            label: x.entityLabel || '',
            url: x.entityUrl?.path || ''
        }))
        : []
}