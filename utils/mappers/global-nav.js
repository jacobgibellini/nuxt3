import { isArrayAndHasChild } from '@/utils/array'

export const mapMobileMenu = (menu = []) => {
    if (!isArrayAndHasChild(menu)) {
        return []
    }

    const mappedMenu = menu.map(menuItem => {
        // no links are available
        if (!isArrayAndHasChild(menuItem.links)) {
            return menuItem
        }

        const mappedLinks = menuItem.links.map(category => {
            if (!isArrayAndHasChild(category?.links)) {
                return category
            }
            // when there are sub categories, add another item at the start of the array
            // which will direct users to the main category page
            const linkToCategory = {
                label: 'All ' + category.label,
                url: {
                    path: category.url?.path
                },
                bold: true
            }
            return {
                ...category,
                links: [linkToCategory].concat(category.links)
            }
        })
        
        return {
            ...menuItem,
            links: mappedLinks
        }
    })
    
    return mappedMenu
}