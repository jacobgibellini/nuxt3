/* eslint-disable no-case-declarations */
import { isArrayAndHasChild } from '~/utils/array';
import { mapShortCoursesToCategoryCardsPod } from '~/utils/mappers/field-short-courses-list';
import { mapMicroCredentialsToCategoryCardsPod } from '~/utils/mappers/field-micro-credentials';

export const mapFieldMultiPodVideos = (fieldMultiPodVideos) =>
    fieldMultiPodVideos.map(({ entity = {} }) => ({
        id:
            entity.entityId && entity.entityBundle
                ? `${entity.entityBundle}-${entity.entityId}`
                : null,
        videoUrl: entity.fieldVideoUrl,
        duration: entity.fieldVideoDuration,
        description: entity.fieldVideoDescription?.processed,
    }));

export const mapItemsOfEntitySubqueueLandingPageFaqsLists = (
    itemsOfEntitySubqueueLandingPageFaqsLists
) =>
    itemsOfEntitySubqueueLandingPageFaqsLists.map(({ entity }) => ({
        id: entity.entityId,
        label: entity.info,
        contentHtml: entity.body?.processed,
    }));

export const mapEntityBundleToRenderConfig = (entity = {}) => {
    const entityBundle = entity?.entityBundle;
    const fieldSidebarColour = entity?.fieldSidebarColour;

    if (!entityBundle) return;

    const id = entity.entityId ? `${entityBundle}-${entity.entityId}` : null;

    switch (entityBundle) {
        case 'block_quote':
            const { fieldQuote, fieldAuthorName } = entity;

            return {
                name: 'RmitoBlockquote',
                props: {
                    id,
                    content: fieldQuote,
                    footer: fieldAuthorName,
                },
            };

        case 'pull_quote':
            return {
                name: 'RmitoPullquote',
                props: {
                    id,
                    content: entity.fieldQuote,
                },
            };

        case 'image':
            const {
                fieldImage: { title, alt, derivative },
            } = entity;

            return {
                name: 'RmitoImageWithCaption',
                props: {
                    id,
                    alt,
                    src: derivative?.url,
                    caption: title,
                },
            };

        case 'content':
            return {
                name: 'RmitoRichTextContent',
                props: {
                    id,
                    'content-html': entity.fieldBody?.processed,
                },
            };

        case 'hero_video_component':
            return {
                name: 'RmitoSupportiveVideo',
                props: {
                    id,
                    duration: entity.fieldVideoDuration,
                    sectionDescriptionHtml: entity.fieldSubheading?.processed,
                    sectionTitle: entity.fieldHeading,
                    videoDescriptionHtml: entity.fieldVideoDescription?.processed,
                    videoPosition: entity.fieldVideoPosition,
                    videoUrl: entity.fieldVideoUrl,
                },
            };

        case 'hero_image_component':
            return {
                name: 'RmitoSupportiveImage',
                props: {
                    id,
                    title: entity.fieldHeading,
                    description: entity.fieldDescription?.processed,
                    position: entity.fieldImagePosition?.toLowerCase(),
                    src: entity.fieldImage?.derivative?.url,
                    button: {
                        secondary: entity.fieldButtonColour?.toLowerCase() === 'secondary',
                        label: entity.fieldLink?.title,
                        url: entity.fieldLink?.url?.path,
                    },
                },
            };

        case 'multi_pod_videos_component':
            return {
                name: 'RmitoMultiVideoPod',
                props: {
                    id,
                    sectionDescriptionHtml: entity.fieldSubheading?.processed,
                    sectionTitle: entity.fieldHeading,
                    videos: isArrayAndHasChild(entity.fieldMultiPodVideos)
                        ? mapFieldMultiPodVideos(entity.fieldMultiPodVideos)
                        : [],
                },
            };

        case 'faqs_accordion':
            const { itemsOfEntitySubqueueLandingPageFaqsLists } =
                entity.fieldFaqsEntitySubqueue?.entity || {};

            return {
                name: 'RmitAccordionPanels',
                props: {
                    sectionDescriptionHtml: entity.fieldSubheading?.processed,
                    sectionTitle: entity.fieldHeading,
                    footerLinkUrl: entity.footerLinkUrl,
                    footerLinkText: entity.footerLinkText,
                    rawPanelsCollection: isArrayAndHasChild(
                        itemsOfEntitySubqueueLandingPageFaqsLists
                    )
                        ? mapItemsOfEntitySubqueueLandingPageFaqsLists(
                              itemsOfEntitySubqueueLandingPageFaqsLists
                          )
                        : [],
                    multiOpen: true,
                    gaEventOnClick: 'clickFaqPanel',
                    gaEventFooterLink: 'clickFaqBrowseAll',
                },
            };

        case 'short_courses':
            const { fieldShortCoursesList, fieldAllCoursesLink } = entity;

            return {
                name: 'RmitoCategoryCardsPod',
                props: {
                    cards: isArrayAndHasChild(fieldShortCoursesList)
                        ? mapShortCoursesToCategoryCardsPod({
                              fieldShortCoursesList,
                              fieldSidebarColour,
                          })
                        : [],
                    footerLinkText: fieldAllCoursesLink?.title,
                    footerLinkUrl: fieldAllCoursesLink?.url?.path,
                    footerLinkType: 'url',
                    footerLinkEvent: 'clickBrowseAllStudyOptions',
                    cardClickEvent: 'clickBrowseCourse',
                    extendedLine: true,
                },
            };

        case 'micro_credential_list':
            const { fieldMicroCredentials, fieldDownloadBrochureLabel, fieldDownloadBrochureLink } =
                entity;

            return {
                name: 'RmitoCategoryCardsPod',
                props: {
                    cards: isArrayAndHasChild(fieldMicroCredentials)
                        ? mapMicroCredentialsToCategoryCardsPod({
                              fieldMicroCredentials,
                              fieldSidebarColour,
                          })
                        : [],
                    footerLinkText: fieldDownloadBrochureLabel,
                    footerLinkUrl: fieldDownloadBrochureLink?.entity?.url,
                    footerLinkEvent: 'clickDownloadBrochureCredentials',
                    footerLinkType: 'file',
                },
            };
    }
};

export const mapFieldContentToComponent = (raw) =>
    raw.reduce((renderConfigs, fieldContentItem) => {
        const next =
            fieldContentItem?.entity && mapEntityBundleToRenderConfig(fieldContentItem.entity);
        return [...renderConfigs, ...(next ? [next] : [])];
    }, []);

export const mapFieldContentWithData = (fieldContent, rawDataHackFn) =>
    isArrayAndHasChild(fieldContent)
        ? fieldContent.map((item) =>
              typeof rawDataHackFn === 'function' ? rawDataHackFn(item) : item
          )
        : fieldContent;

export const addFaqAccodionFooterLink = (fieldContentItem) => {
    if (fieldContentItem?.entity?.entityBundle === 'faqs_accordion') {
        return {
            entity: {
                ...fieldContentItem.entity,
                footerLinkUrl: '/faq',
                footerLinkText: 'See all FAQs',
            },
        };
    }

    return fieldContentItem;
};

export const addDataToStudentHubTabItems = (tabItems) =>
    isArrayAndHasChild(tabItems)
        ? tabItems.map((tabItem) => ({
              ...tabItem,
              fieldContent: mapFieldContentWithData(
                  tabItem?.fieldContent,
                  addFaqAccodionFooterLink
              ),
          }))
        : tabItems;
