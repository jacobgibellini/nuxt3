import { mapShortCoursesToCategoryCardsPod } from './field-short-courses-list';

describe('#mapShortCoursesToCategoryCardsPod', () => {
    it('should map "fieldShortCoursesList" to "cards" prop compatible structure for "RmitoCategoryCardsPod" component', () => {
        const expected = [
            {
                label: 'Digital Delivery with Agile',
                description:
                    'Next start 2 Nov 2020<p>Harness agile and lean methodologies and collaborate more effectively within your team.</p>',
                url: '/course/sc-digital-delivery-agile-dtr104',
                color: '#fff',
            },
            {
                label: 'Product Management',
                description:
                    '<p>Product Management isn`t just for product managers. Learn how to create value through scalable solutions and drive business success from some of the best employers in industry. </p>',
                url: undefined,
                color: '#fff',
            },
            {
                label: 'Design Thinking for Innovation',
                description:
                    '<p>Use creative design thinking to develop innovative business solutions.</p>',
                url: undefined,
                color: '#fff',
            },
        ];
        const payload = {
            fieldShortCoursesList: [
                {
                    entity: {
                        title: 'Digital Delivery with Agile',
                        fieldCourseDescription: {
                            processed:
                                '<p>Harness agile and lean methodologies and collaborate more effectively within your team.</p>',
                        },
                        fieldNextStartDate: {
                            date: '2020-11-02 12:00:00 UTC',
                        },
                        entityUrl: {
                            path: '/course/sc-digital-delivery-agile-dtr104',
                        },
                    },
                },
                {
                    entity: {
                        title: 'Product Management',
                        fieldCourseDescription: {
                            processed:
                                '<p>Product Management isn`t just for product managers. Learn how to create value through scalable solutions and drive business success from some of the best employers in industry. </p>',
                        },
                    },
                },
                {
                    entity: {
                        title: 'Design Thinking for Innovation',
                        fieldCourseDescription: {
                            processed:
                                '<p>Use creative design thinking to develop innovative business solutions.</p>',
                        },
                        fieldNextStartDate: {
                            date: false,
                        },
                        entityUrl: null,
                    },
                },
            ],

            fieldSidebarColour: {
                color: '#fff',
            },
        };

        const actual = mapShortCoursesToCategoryCardsPod(payload);

        expect(actual).toEqual(expected);
    });

    it('should return undefined for "color" when "fieldSidebarColour" does not have "color" property', () => {
        const expected = [
            {
                label: 'title',
                description: '<p>description html</p>',
                url: '/course/blahblah',
                color: undefined,
            },
        ];
        const payload = {
            fieldShortCoursesList: [
                {
                    entity: {
                        title: 'title',
                        fieldCourseDescription: {
                            processed: '<p>description html</p>',
                        },
                        entityUrl: {
                            path: '/course/blahblah',
                        },
                    },
                },
            ],

            fieldSidebarColour: {},
        };

        const actual = mapShortCoursesToCategoryCardsPod(payload);

        expect(actual).toEqual(expected);
    });

    it('should return undefined for "color" when "fieldSidebarColour" is not defined', () => {
        const expected = [
            {
                label: 'title',
                description: '<p>description html</p>',
                url: '/course/blahblah',
                color: undefined,
            },
        ];
        const payload = {
            fieldShortCoursesList: [
                {
                    entity: {
                        title: 'title',
                        fieldCourseDescription: {
                            processed: '<p>description html</p>',
                        },
                        entityUrl: {
                            path: '/course/blahblah',
                        },
                    },
                },
            ],
        };

        const actual = mapShortCoursesToCategoryCardsPod(payload);

        expect(actual).toEqual(expected);
    });
});
