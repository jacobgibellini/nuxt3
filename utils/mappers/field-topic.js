import { isArrayAndHasChild } from '~/utils/array';

export const mapFieldTopicInProductDetailsPage = fieldTopic =>
    isArrayAndHasChild(fieldTopic)
        ? fieldTopic.map(({ entity }) => ({
                label: entity?.entityLabel,
                href: entity?.entityUrl?.path
            })
        )
        : [];