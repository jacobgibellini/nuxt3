import { mapMobileMenu } from './global-nav'

describe('#mapMobileMenu', () => {
    it.each`
        valueType     | valueName      
        ${[]}         | ${'an empty array'} 
        ${undefined}  | ${'undefined'}      
        ${null}       | ${'null'}      
    `('should return empty array when "$valueName" is passed', ({
        valueType,
        valueName
    }) => {
        // Arrange
        const initial = valueType
        const expected = []

        // Act
        const result = mapMobileMenu(initial)

        // Assert
        expect(result).toEqual(expected)
    })

    it('should return empty array when a falsey value is passed', () => {
        // Arrange
        const initial = undefined
        const expected = []

        // Act
        const result = mapMobileMenu(initial)

        // Assert
        expect(result).toEqual(expected)
    })

    it('should return same result when there are items but no values', () => {
        // Arrange
        const initial = [{}, {}, {}]
        const expected = [{}, {}, {}]

        // Act
        const result = mapMobileMenu(initial)

        // Assert
        expect(result).toEqual(expected)
    })

    it('should return the same array when there are no links', () => {
        // Arrange
        const initial = [ 
            {
                label: "Digimon",
                url: { path: "/digimon" },
                links: []
            }
        ]
        const expected = [ 
            {
                label: "Digimon",
                url: { path: "/digimon" },
                links: []
            }
        ]

        // Act
        const result = mapMobileMenu(initial)

        // Assert
        expect(result).toEqual(expected)
    })

    it('should return the same array if there are no sublinks', () => {
        // Arrange
        const initial = [ 
            {
                label: "Courses & Degrees",
                url: { path: "/courses" },
                links: [
                    {
                        label: "Pokemon",
                        url: { path: "/pokemon" },
                        links: []
                    }
                ]
            }
        ]
        const expected = [ 
            {
                label: "Courses & Degrees",
                url: { path: "/courses" },
                links: [
                    {
                        label: "Pokemon",
                        url: { path: "/pokemon" },
                        links: []
                    }
                ]
            }
        ]

        // Act
        const result = mapMobileMenu(initial)

        // Assert
        expect(result).toEqual(expected)
    })

    it('should add an "All" link when there are sublinks', () => {
        // Arrange
        const initial = [ 
            {
                label: "Courses & Degrees",
                url: { path: "/courses" },
                links: [
                    {
                        label: "Pokemon",
                        url: { path: "/pokemon" },
                        links: [
                            {
                                label: "Training",
                                url: {
                                    path: "/pokemon/training"
                                }
                            }
                        ]
                    }
                ]
            }
        ]
        const expected = [ 
            {
                label: "Courses & Degrees",
                url: { path: "/courses" },
                links: [
                    {
                        label: "Pokemon",
                        url: { path: "/pokemon" },
                        links: [
                            // This first item should be added when mapped
                            {
                                label: "All Pokemon",
                                url: { path: "/pokemon" },
                                bold: true
                            },
                            {
                                label: "Training",
                                url: { path: "/pokemon/training" }
                            }
                        ]
                    }
                ]
            }
        ]

        // Act
        const result = mapMobileMenu(initial)

        // Assert
        expect(result).toEqual(expected)
    })
})