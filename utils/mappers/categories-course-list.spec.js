import { mapCourseResultsToCourseCardProps } from './categories-course-list';

describe('#mapCourseResultsToCourseCardProps', () => {
    it('should map courses results into RmitoCourseCard props', () => {
        const coursesResults = [
            {
                entityId: '1234',
                entityBundle: 'course_page_v2',
                entityLabel: 'Business Analytics',
                entityUrl: {
                    path: '/course/sc-business-analytics-dat103',
                },
                fieldHeroImage: {
                    derivative: {
                        url: '//assets.mockurl.com/image-file-name-1.jpg',
                    },
                },
                fieldActiveInstances: {
                    entities: [
                        {
                            fieldStartDate: {
                                date: '2020-05-18 12:00:00 UTC',
                            },
                            fieldUrgencyStatus: 'started',
                            fieldUrgencyMessage: ['Course started', 'Enrolment still open'],
                        },
                    ],
                },
                fieldCourseTagType: {
                    entity: {
                        name: 'Future Skills short course',
                        machineName: 'future_skills',
                    },
                },
                fieldCourseTimeCommitment: '6 weeks (5-8 hours per week)',
            },
            {
                entityId: '5678',
                entityBundle: 'inhouse_accredited',
                entityLabel: 'Implementing Digital Marketing Campaigns',
                entityUrl: {
                    path: '/course/sc-implementing-digital-marketing-campaigns-dmk102',
                },
                fieldHeroImage: {
                    derivative: {
                        url: '//assets.mockurl.com/image-file-name-2.jpg',
                    },
                },
                fieldActiveInstances: {
                    entities: [
                        {
                            fieldStartDate: {
                                date: '2020-05-20 12:00:00 UTC',
                            },
                            fieldUrgencyStatus: 'started',
                            fieldUrgencyMessage: ['Course started', 'Enrolment still open'],
                        },
                        {
                            fieldStartDate: {
                                date: '2020-08-17 12:00:00 UTC',
                            },
                            fieldUrgencyStatus: null,
                            fieldUrgencyMessage: [],
                        },
                    ],
                },
                fieldCourseTagType: {
                    entity: {
                        name: 'Postgraduate',
                        machineName: 'postgraduate',
                    },
                },
                fieldDuration: 12,
                fieldAcceleratedDuration: 6,
            },
        ];

        const expected = [
            {
                id: '1234',
                url: '/course/sc-business-analytics-dat103',
                heroImageUrl: '//assets.mockurl.com/image-file-name-1.jpg',
                courseTitle: 'Business Analytics',
                courseType: 'Future Skills short course',
                courseCategoryMachineName: 'future_skills',
                timeCommitment: '6 weeks (5-8 hours per week)',
                startDate: {
                    date: '2020-05-18 12:00:00 UTC',
                    formattedDate: '18 May 2020',
                    urgency: {
                        message: ['Course started', 'Enrolment still open'],
                        status: 'closing',
                    },
                },
            },
            {
                id: '5678',
                url: '/course/sc-implementing-digital-marketing-campaigns-dmk102',
                heroImageUrl: '//assets.mockurl.com/image-file-name-2.jpg',
                courseTitle: 'Implementing Digital Marketing Campaigns',
                courseType: 'Postgraduate',
                courseCategoryMachineName: 'postgraduate',
                timeCommitment: '6 - 12 months',
                startDate: {
                    date: '2020-05-20 12:00:00 UTC',
                    formattedDate: '20 May 2020',
                    urgency: {
                        message: ['Course started', 'Enrolment still open'],
                        status: 'closing',
                    },
                },
            },
        ];

        const output = mapCourseResultsToCourseCardProps(coursesResults);

        expect(output).toEqual(expected);
    });

    it('should map external courses link correctly', () => {
        const coursesResults = [
            {
                entityId: '1106',
                entityBundle: 'external_course',
                entityLabel: 'Bachelor of Information Technology',
                entityUrl: {
                    path: '/node/1106',
                },
                fieldHeroImage: {
                    derivative: {
                        url: '//assets.mockurl.com/heroimage-file-name-2.jpg',
                    },
                },
                fieldActiveInstances: {
                    entities: [
                        {
                            fieldStartDate: {
                                date: '2020-06-01 12:00:00 UTC',
                            },
                            fieldUrgencyStatus: 'closing_today',
                            fieldUrgencyMessage: ['Closing Today', 'Apply Now'],
                        },
                        {
                            fieldStartDate: {
                                date: '2020-08-31 12:00:00 UTC',
                            },
                            fieldUrgencyStatus: null,
                            fieldUrgencyMessage: [],
                        },
                        {
                            fieldStartDate: {
                                date: '2020-11-30 12:00:00 UTC',
                            },
                            fieldUrgencyStatus: null,
                            fieldUrgencyMessage: [],
                        },
                    ],
                },
                fieldCourseTagType: {
                    entity: {
                        name: 'Undergraduate',
                        machineName: 'undergraduate',
                    },
                },
                fieldExternalLink: {
                    uri: 'https://www.open.edu.au/degrees/bachelor-of-information-technology-rmit-university-rmi-cpt-deg',
                },
                fieldExternalCourseType: 'OUA',
                fieldCourseTimeCommitment: '3 - 4 Years',
            },
        ];
        const expected = [
            {
                id: '1106',
                url: 'https://www.open.edu.au/degrees/bachelor-of-information-technology-rmit-university-rmi-cpt-deg',
                heroImageUrl: '//assets.mockurl.com/heroimage-file-name-2.jpg',
                courseTitle: 'Bachelor of Information Technology',
                courseType: 'Undergraduate',
                courseCategoryMachineName: 'undergraduate',
                timeCommitment: '3 - 4 Years',
                startDate: {
                    date: '2020-06-01 12:00:00 UTC',
                    formattedDate: '1 Jun 2020',
                    urgency: {
                        message: ['Closing Today', 'Apply Now'],
                        status: 'closing',
                    },
                },
            },
        ];

        const output = mapCourseResultsToCourseCardProps(coursesResults);

        expect(output).toEqual(expected);
    });

    it('should non future skills course "formattedDate" value not "Coming soon" when "fieldIsActive" is false despite the fact that "fieldActiveInstances" have valid start date entities', () => {
        const coursesResults = [
            {
                fieldActiveInstances: {
                    entities: [
                        {
                            fieldStartDate: {
                                date: '2021-06-01 12:00:00 UTC',
                            },
                            fieldUrgencyStatus: 'closing_today',
                            fieldUrgencyMessage: [
                                'mock urgency message 1',
                                'mock urgency message 2',
                            ],
                        },
                    ],
                },
                fieldIsActive: false,
            },
        ];
        const expected = [
            {
                id: '',
                url: '',
                heroImageUrl: '',
                courseTitle: '',
                courseType: '',
                courseCategoryMachineName: '',
                timeCommitment: '',
                startDate: {
                    date: '2021-06-01 12:00:00 UTC',
                    formattedDate: '1 Jun 2021',
                    urgency: {
                        message: ['mock urgency message 1', 'mock urgency message 2'],
                        status: 'closing',
                    },
                },
            },
        ];

        const output = mapCourseResultsToCourseCardProps(coursesResults);

        expect(output).toEqual(expected);
    });

    it('should future skills "startDate" return expected object when "fieldIsActive" is true', () => {
        const coursesResults = [
            {
                entityBundle: 'course_page_v2',
                fieldActiveInstances: {
                    entities: [
                        {
                            fieldStartDate: {
                                date: '2022-05-18 12:00:00 UTC',
                            },
                            fieldUrgencyStatus: 'started',
                            fieldUrgencyMessage: [
                                'mock urgency message 1',
                                'mock urgency message 2',
                            ],
                        },
                    ],
                },
                fieldIsActive: true,
            },
        ];
        const expected = [
            {
                id: '',
                url: '',
                heroImageUrl: '',
                courseTitle: '',
                courseType: '',
                courseCategoryMachineName: '',
                timeCommitment: '',
                startDate: {
                    date: '2022-05-18 12:00:00 UTC',
                    formattedDate: '18 May 2022',
                    urgency: {
                        message: ['mock urgency message 1', 'mock urgency message 2'],
                        status: 'closing',
                    },
                },
            },
        ];

        const output = mapCourseResultsToCourseCardProps(coursesResults);

        expect(output).toEqual(expected);
    });

    it('should future skills "startDate" return expected object when "fieldIsActive" is false', () => {
        const coursesResults = [
            {
                entityBundle: 'course_page_v2',
                fieldActiveInstances: {
                    entities: [
                        {
                            fieldStartDate: {
                                date: '2020-05-18 12:00:00 UTC',
                            },
                            fieldUrgencyStatus: 'started',
                            fieldUrgencyMessage: [
                                'mock urgency message 1',
                                'mock urgency message 2',
                            ],
                        },
                    ],
                },
                fieldIsActive: false,
            },
        ];
        const expected = [
            {
                id: '',
                url: '',
                heroImageUrl: '',
                courseTitle: '',
                courseType: '',
                courseCategoryMachineName: '',
                timeCommitment: '',
                startDate: {
                    formattedDate: 'Coming soon',
                },
            },
        ];

        const output = mapCourseResultsToCourseCardProps(coursesResults);

        expect(output).toEqual(expected);
    });

    it('should future skills "formattedDate" value be "Coming soon" when "fieldActiveInstances" is invalid despite the fact that "fieldIsActive" is true', () => {
        const coursesResults = [
            {
                entityBundle: 'course_page_v2',
                fieldActiveInstances: [
                    {
                        fieldStartDate: {
                            date: '2020-05-18 12:00:00 UTC',
                        },
                        fieldUrgencyStatus: 'started',
                        fieldUrgencyMessage: ['mock urgency message 1', 'mock urgency message 2'],
                    },
                ],
                fieldIsActive: true,
            },
            {
                entityBundle: 'course_page_v2',
                fieldActiveInstances: {
                    entities: [],
                },
                fieldIsActive: true,
            },
            {
                entityBundle: 'course_page_v2',
                fieldActiveInstances: null,
                fieldIsActive: true,
            },
        ];
        const expected = [
            {
                id: '',
                url: '',
                heroImageUrl: '',
                courseTitle: '',
                courseType: '',
                courseCategoryMachineName: '',
                timeCommitment: '',
                startDate: {
                    formattedDate: 'Coming soon',
                },
            },
            {
                id: '',
                url: '',
                heroImageUrl: '',
                courseTitle: '',
                courseType: '',
                courseCategoryMachineName: '',
                timeCommitment: '',
                startDate: {
                    formattedDate: 'Coming soon',
                },
            },
            {
                id: '',
                url: '',
                heroImageUrl: '',
                courseTitle: '',
                courseType: '',
                courseCategoryMachineName: '',
                timeCommitment: '',
                startDate: {
                    formattedDate: 'Coming soon',
                },
            },
        ];

        const output = mapCourseResultsToCourseCardProps(coursesResults);

        expect(output).toEqual(expected);
    });
});
