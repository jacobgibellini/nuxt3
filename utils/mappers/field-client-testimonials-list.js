import isStringButNotEmpty from '@/utils/string/is-string-but-not-empty';

export const mapTestimonialsToReviewItems = itemsOfEntitySubqueueLandingPageTestimonialsList =>
    itemsOfEntitySubqueueLandingPageTestimonialsList.map(({ entity = {} }) => {
        const {
            body,
            entityBundle,
            entityId,
            entityLabel,
            fieldClientDesignation,
            fieldClientLogo
        } = entity;

        const id = isStringButNotEmpty(entityId) && isStringButNotEmpty(entityBundle)
            ? `${entityBundle}-${entityId}`
            : '';

        return {
            id,
            image: fieldClientLogo?.derivative?.url || '',
            name: entityLabel || '',
            title: fieldClientDesignation || '',
            quote: body?.processed || ''
        }
    });
