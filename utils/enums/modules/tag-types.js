const tagTypes = {
    FS_SHORT_COURSE: 'future_skills',
    FS_BUNDLE: 'future_skills_bundle',
    POSTGRADUATE: 'postgraduate',
    UNDERGRADUATE: 'undergraduate'
}

export default tagTypes