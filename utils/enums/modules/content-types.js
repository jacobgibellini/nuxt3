const CONTENT_TYPES = {
    PROGRAM_PAGE: 'program_page',
    COURSE_PAGE: 'course_page_v2',
    INHOUSE_ACCREDITED_PAGE: 'inhouse_accredited',
    EXTERNAL_COURSE: 'external_course'
};

export default CONTENT_TYPES;