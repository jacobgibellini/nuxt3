// Breakpoints must stay inline with ~/assets/sass/base/_breakpoints.sass
const BREAKPOINTS = {
    SMALL: 576,
    MEDIUM: 768,
    LARGE: 992,
    EXTRA_LARGE: 1440
}

export default BREAKPOINTS