const staticFormType = {
    inhouse_accredited: "Brochure",
    course_page_v2: "Course Guide"
}

export default staticFormType