const URGENCY_LEVELS = {
    closing_soon: 'urgent',
    urgent: 'urgent',
    closing_today: 'closing',
    started: 'closing'
}

export default URGENCY_LEVELS