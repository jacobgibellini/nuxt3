const BANNER_ICONS = Object.freeze({
    EYE: 'eye',
    BELL: 'bell'
})

export default BANNER_ICONS