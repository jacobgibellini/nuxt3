const PORTFOLIOS = {
    FS_SHORT_COURSE: 'course_page_v2',
    INHOUSE_ACCREDITED: 'inhouse_accredited',
};

export default PORTFOLIOS;
