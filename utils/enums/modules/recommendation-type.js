const RECOMMENDATION_TYPE = Object.freeze({
    FUTURE_SKILLS           : 'course-short_course',
    FUTURE_SKILLS_ENROLMENT : 'enrolment-short_course',
    ACCREDITED              : 'course-long_course',
    ACCREDITED_ENROLMENT    : 'enrolment-long_course',
    BUNDLE                  : 'course-course_bundle',
    BUNDLE_ENROLMENT        : 'enrolment-course_bundle',
    TOPIC                   : 'topic',
    CATEGORY                : 'category',
    SUBCATEGORY             : 'subcategory'
})

export default RECOMMENDATION_TYPE