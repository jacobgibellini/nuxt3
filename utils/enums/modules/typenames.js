const typenames = {
    ACCREDITED: 'NodeInhouseAccredited',
    EXTERNAL: 'NodeExternalCourse',
    FUTURE_SKILL: 'NodeCoursePageV2',
    PROGRAM: 'NodeProgramPage',
    CATEGORY: 'TaxonomyTermIndustry',
    TOPIC: 'TaxonomyTermTopic',
    MLP: 'marketing_landing_page',
};

export default typenames;
