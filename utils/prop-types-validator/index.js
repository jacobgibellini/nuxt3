export const haveSpecifiedPropertyNames = (...requirePropertyNames) => obj => {
    const objKeys = Object.keys(obj);

    if (objKeys.length < requirePropertyNames.length) return false;
    return requirePropertyNames.every(objKey => objKeys.includes(objKey));
};

export const propertyNames = (...requirePropertyNames) => {
    const assertHaveSpecifiedPropertyNames = haveSpecifiedPropertyNames(...requirePropertyNames);

    return collection => collection.every(assertHaveSpecifiedPropertyNames);
};

export const oneOf = acceptableList => value => acceptableList.indexOf(value) !== -1;