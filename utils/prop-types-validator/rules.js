import isFunction from 'lodash/isFunction';

export default function rulesValidator(rules) {
    return rules.every(rule => isFunction(rule));
}