import rulesValidator from './rules';

describe('#rulesValidator', () => {
    it('should return true when validator call with empty array', () => {
        const mockValue = [];

        const received = rulesValidator(mockValue);

        expect(received).toBe(true);
    });

    it('should return true when validator call with array of functions', () => {
        const mockValue = [
            () => {}
        ];

        const received = rulesValidator(mockValue);

        expect(received).toBe(true);
    });

    it('should return false when validator call with array of non function types', () => {
        const mockValue = [
            'string',
            null,
            {},
            false
        ];

        const received = rulesValidator(mockValue);

        expect(received).toBe(false);
    });

    it('should return false when validator call with array of other types even there is a function', () => {
        const mockValue = [
            () => {},
            'string',
            null,
            {},
            false
        ];

        const received = rulesValidator(mockValue);

        expect(received).toBe(false);
    });
});