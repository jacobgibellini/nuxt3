import { formatPrice, formatAuNumber } from './index'

describe('Formatters', () => {
    describe('fn: formatPrice', () => {
        it.each`
            nonNumericValue
            ${ undefined }
            ${ null }
            ${ '' }  
            ${ true }   
            ${ false }
            ${ 'hello world' }
        `('should return an empty string when "$nonNumericValue" is passed', ({ nonNumericValue }) => {
            const expectedResult = ''

            const result = formatPrice(nonNumericValue)

            expect(result).toBe(expectedResult)
        })

        it.each`
            price          | expectedResult
            ${ '10' }      | ${ '$10' }
            ${ '100' }     | ${ '$100' }
            ${ '1000' }    | ${ '$1,000' }
            ${ '0.99' }    | ${ '$1' }
            ${ 2345345 }   | ${ '$2,345,345' }
            ${ 953454 }    | ${ '$953,454' }
            ${ 0 }         | ${ '$0' }
        `('should return $expectedResult when $price is passed', ({ price, expectedResult }) => {
            const result = formatPrice(price)

            expect(result).toBe(expectedResult)
        })
    })

    describe('fn: formatAuNumber', () => {
        it.each`
            ausMobileNumber         | expectedResult
            ${ '0488888888' }       | ${ '+61 488 888 888' }  
            ${ '+61422555888' }     | ${ '+61 422 555 888' } 
            ${ '+61 412 345 678' }  | ${ '+61 412 345 678' } 
            ${ '+6 141 234 5678' }  | ${ '+61 412 345 678' } 
        `('should format $ausMobileNumber to $expectedResult', ({ ausMobileNumber, expectedResult }) => {
            const result = formatAuNumber(ausMobileNumber)

            expect(result).toBe(expectedResult)
        })

        it.each`
            ausMobileNumber         | expectedResult
            ${ '4412345678' }       | ${ '4412345678' }  
            ${ 'test' }             | ${ 'test' } 
            ${ '+61 412 345 678' }  | ${ '+61 412 345 678' } 
            ${ '4412345678234234' } | ${ '4412345678234234' } 
            ${ undefined }          | ${ '' } 
            ${ null }               | ${ '' } 
        `('should not format $ausMobileNumber', ({ ausMobileNumber, expectedResult }) => {
            const result = formatAuNumber(ausMobileNumber)

            expect(result).toBe(expectedResult)
        })
    })
})