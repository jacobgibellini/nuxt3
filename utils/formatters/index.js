import { replaceFirstChars } from '@/utils/helpers'

/**
 * Converts a string or number into a price format.
 * eg. 1000 will become $1,000
 * 
 * @param { String | Number } value 
 */
export const formatPrice = (value = '') => {
    if (typeof value !== 'number' && typeof value !== 'string' || value === '' || isNaN(value)) {
        return ''
    }

    let val = (value/1).toFixed(0).replace(',', '.')
    return '$' + val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

/**
 * Formats a standard australian mobile number
 * eg. "0412345678" -> "+61 412 345 678"
 * 
 * @param { number } phoneNumber 
 */
export const formatAuNumber = phoneNumber => {
    let number = (phoneNumber || '').replace(/ /g, '')
  
    const startsWithZero = number.startsWith('0')
    const startsWithArea = number.startsWith('+61')
    const invalidNumberLength = number.length < 10 || number.length > 12

    if (isNaN(number) || invalidNumberLength || !startsWithZero && !startsWithArea) {
        return number
    }

    if (startsWithZero) {
        number = replaceFirstChars(number, '0', '+61')
    }
    
    // Add a space after every third character
    number = number.split('').map((character, index) => {
        return (index + 1) % 3 === 0 ? character + ' ' : character
    })

    return number.join('').trim()
}