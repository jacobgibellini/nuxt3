export const ASYNC_TIMEOUT_ERROR_NAME = 'AsyncTimeoutError';

export class AsyncTimeoutError extends Error {
    constructor(ms, ...params) {
        super(...params);

        this.name = ASYNC_TIMEOUT_ERROR_NAME;
        this.message = `Async timeout: ${ms}`;
    }
}

export const asyncTimeout = (
    asyncFn,
    ms = 5000
) => {
    return Promise.race([
        new Promise(asyncFn),
        new Promise((_, reject) => setTimeout(
            reject,
            ms,
            new AsyncTimeoutError(ms)
        ))
    ]);
};