import { asyncTimeout, AsyncTimeoutError } from './async-timeout';

describe('#asyncTimeout', () => {
    it('should not receive AsyncTimeoutError when callback function triggered before timeout', async () => {
        const output = await asyncTimeout(
            resolve => setTimeout(() => {
                resolve('pass')
            }, 10),
            300
        );

        expect(output).toBe('pass');
    });

    it('should reject with AsyncTimeoutError when timeout reached', async () => {
        expect.assertions(1);

        await expect(
            asyncTimeout(
                resolve => setTimeout(() => {
                    resolve('pass')
                }, 300),
                10
            )
        ).rejects.toBeInstanceOf(AsyncTimeoutError);
    })
});