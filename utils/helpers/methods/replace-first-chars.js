/**
 * @param { string } str - String to be formatted.
 * @param { string } firstChars - The first characters of the string to compare.
 * @param { string } stringReplacement - The string to replace the first characters.
 * @returns { string }
 */
const replaceFirstChars = (str, firstChars, stringReplacement) => {
    return str.startsWith(firstChars) ? str.replace(firstChars, stringReplacement) : str;
};

export default replaceFirstChars;
