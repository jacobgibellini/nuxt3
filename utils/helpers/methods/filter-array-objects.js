import { isArrayAndHasChild } from '@/utils/array';
/**
 * @author Jacob Gibellini
 * @date 19/07/2019
 * @description
 * Filters through an array based on multiple search items.
 * @param { array<string> || string } items - List of search terms.
 * @param { string } key - For when the items in the array are objects, this helps match against the search items.
 * @returns { array }
 * @example
 * ['banana', 'apple', 'orange'].rmitoFilter(['apple']) // returns - ['apple']
 */
const filterArrayObjects = (arr, items, key) => {
    // Filter through the current array if there are items.
    return isArrayAndHasChild(arr) 
    ? arr.filter(x => {
        // Get the item to be searched against.
        const searchItem = key ? x[key] : x
        // Add the item if there is a match.
        return Array.isArray(items) ? items.some(item => searchItem === item) : searchItem === items
    })
    : []
}

export default filterArrayObjects