/**
 * @description
 * Compares two arrays on whether they contain the same items in the same order.
 * Referenced from the top answer: https://stackoverflow.com/questions/7837456/how-to-compare-arrays-in-javascript
 * @param { array } arr1
 * @param { array } arr2
 * @returns { boolean }
 */
const arraysAreEqual = (arr1, arr2) => {
    // if the other array is a falsy value, return
    if (!arr1) return false;

    // compare lengths - can save a lot of time
    if (arr2.length !== arr1.length) return false;

    for (var i = 0, l = arr2.length; i < l; i++) {
        // Check if we have nested arrays
        if (arr2[i] instanceof Array && arr1[i] instanceof Array) {
            // recurse into the nested arrays
            if (!arraysAreEqual(arr2[i], arr1[i])) return false;
        } else if (arr2[i] !== arr1[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
};

export default arraysAreEqual;
