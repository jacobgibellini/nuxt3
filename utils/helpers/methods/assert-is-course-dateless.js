import { CONTENT_TYPES } from '@/utils/enums';

export default function assertIsCourseDateless(course) {
    return course?.entityBundle === CONTENT_TYPES.COURSE_PAGE
        && course?.fieldIsActive === false;
}