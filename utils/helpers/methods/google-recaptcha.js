// TODO: remove async to test
export const getReCaptchaToken = async (action = 'submit') =>
    new Promise((resolve, reject) => {
        if ( !grecaptcha ) {
            return reject(ReferenceError('grecaptcha is not defined'));
        }

        try {
            grecaptcha.ready(() => {
                grecaptcha
                    .execute(
                        process.env.reCaptchaSiteKey,
                        { action }
                    )
                    .then(resolve)
                    .catch(reject);
            });
        } catch(err) {
            reject(err);
        }
    });