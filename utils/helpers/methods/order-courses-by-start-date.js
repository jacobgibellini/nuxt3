import { utcTo8601 } from '~/utils/date'

/**
 * @param { Array<Object> } arr - A list of objects (Must contain a property of "startDate")
 * @param { Boolean } descending - The direction of the sort, ascending by default
 */
const orderCoursesByStartDate = (arr, descending = false) =>
    arr.sort((firstItem, secondItem) => {
        const rawFirstStartDate  = firstItem.startDate.date
        const rawSecondStartDate = secondItem.startDate.date

        if(!rawFirstStartDate && !rawSecondStartDate) {
            return 0
        } else if(!rawSecondStartDate) {
            return descending ? 1 : -1
        } else if(!rawFirstStartDate) {
            return descending ? -1 : 1
        }

        const firstDate  = new Date(utcTo8601(rawFirstStartDate))
        const secondDate = new Date(utcTo8601(rawSecondStartDate))

        return descending ? secondDate - firstDate : firstDate - secondDate
    })

export default orderCoursesByStartDate