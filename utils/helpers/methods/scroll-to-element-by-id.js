/**
 * Scrolls to the element on the screen based on the given element's id.
 *
 * @param { string } elementId
 * @param { string } behavior - transition / scrolling behaviour
 * @param { number } yOffset - Offsets the y axis by a given amount
 */
const scrollToElementById = (elementId, { behavior = 'smooth', yOffset = 0 } = {}) => {
    if (!elementId) {
        return;
    }

    let id = elementId;

    // There are occassions where we have the ID string starting with a '#'.
    // We need to remove it before proceeding.
    if (id.startsWith('#')) {
        id = id.substr(1, id.length);
    }

    const element = document.getElementById(id);

    if (element) {
        window.scrollTo({
            top: element.offsetTop + yOffset,
            behavior,
        });
    }
};

export default scrollToElementById;
