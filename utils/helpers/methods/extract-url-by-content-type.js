import CONTENT_TYPES from '@/utils/enums/modules/content-types';

// Url / Path
const extractUrlByContentType = contentType => data => {
    switch (contentType) {
        case CONTENT_TYPES.EXTERNAL_COURSE:
            return data.fieldExternalLink?.uri

        default:
            return data.path?.alias
    }
}
    

export default extractUrlByContentType