/**
 * @description
 * For mapping the existing properties to a new syntax.
 * @param { array<object> } propsArr - The array to transform.
 * @param { function } mapper - a function that acts like a factory to transform the objects within the array.
 * @return { array } new array of mapped properties or an empty array.
 */
const mapObjectArray = (propsArr, mapper) => {
    return Array.isArray(propsArr) && propsArr.length > 0 ? propsArr.map(mapper) : [];
};

export default mapObjectArray;
