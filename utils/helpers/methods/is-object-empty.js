/**
 * Checks if the object does not contain any properties.
 * 
 * @param { object } obj 
 * @return { boolean }
 */
const isObjectEmpty = (obj) => {
    return typeof obj !== 'object' || Object.keys(obj).length === 0
}

export default isObjectEmpty