const whenFalseyThenEmptyString = (inputToAssert, truthyValue) =>
    inputToAssert ? truthyValue : '';

export default whenFalseyThenEmptyString;
