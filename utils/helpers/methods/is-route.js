import { isString } from 'lodash';

const isRoute = (href) =>
    Boolean((isString(href) && href[0] === '/') || /^(http|https)/.test(href));

export default isRoute;
