/**
 * @description
 * Temporary function to re-route the page not using vue-router.
 * This is only used so the page can reload and be served from the drupal backend.
 * @param { string } path
 */
const windowRoute = (path) => {
    if (process.client) {
        window.location.href = window.location.origin + path;
    }
};

export default windowRoute;
