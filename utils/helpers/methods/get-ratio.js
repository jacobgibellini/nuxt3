// Greatest common divisor to get the highest number that evenly divides by both numbers
const gcd = (a, b) => {
    return b === 0 ? a : gcd(b, a % b);
};

const getRatio = (num1, num2) => {
    const divisor = gcd(num1 < 1 ? num1 : Math.floor(num1), num2 < 1 ? num2 : Math.floor(num2));

    return {
        num1: num1 / divisor,
        num2: num2 / divisor,
    };
};

export { gcd, getRatio };
