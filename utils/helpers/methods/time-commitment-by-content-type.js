import CONTENT_TYPES from '@/utils/enums/modules/content-types';

const {
    PROGRAM_PAGE,
    COURSE_PAGE,
    INHOUSE_ACCREDITED_PAGE,
    EXTERNAL_COURSE
} = CONTENT_TYPES;

// Time Commitment / Duration
const extractCoursePageAndExternalCourseTimeCommitment = ({ fieldCourseTimeCommitment }) => fieldCourseTimeCommitment;

const TIME_COMMITMENT_BY_CONTENT_TYPE = {
    [PROGRAM_PAGE]: ({ fieldCourseDuration }) => fieldCourseDuration?.processed,
    [COURSE_PAGE]: extractCoursePageAndExternalCourseTimeCommitment,
    [EXTERNAL_COURSE]: extractCoursePageAndExternalCourseTimeCommitment,
    [INHOUSE_ACCREDITED_PAGE]: ({
        fieldAcceleratedDuration,
        fieldDuration
    }) => {
        const durationPeriod = fieldAcceleratedDuration && fieldDuration
            ? `${ fieldAcceleratedDuration } - ${ fieldDuration }`
            : fieldAcceleratedDuration || fieldDuration || ''

        return durationPeriod
            ? `${durationPeriod} months`
            : '';
    }
};

export default TIME_COMMITMENT_BY_CONTENT_TYPE