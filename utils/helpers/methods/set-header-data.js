import filterArrayObjects from './filter-array-objects'

const KEY_TYPENAME = '__typename'
const KEY_ROBOT    = 'robots'
const KEY_TITLE    = 'title'
const IGNORED_ROBOT_VALUES = [ 'max-image-preview:large' ]

/**
 * Created an object based on the incoming metadata from drupal
 * which will be implemented in all pages in it's appropriate store (Vuex).
 * 
 * @param { array<object> } metadataList - List of metadata objects with a key, value pair
 */
const setHeaderData = (metadataList) => {
    //TODO: Please remove the `robots` filter when working on FOYA-1426
    const filtered = metadataList?.filter(x => {
        return x.key === KEY_ROBOT && IGNORED_ROBOT_VALUES.includes(x.value)
            || x.key !== KEY_TITLE && x.key !== KEY_ROBOT
    })

    return {
        title: metadataList?.find(x => x.key === KEY_TITLE)?.value || '',
        links: filterArrayObjects(filtered, 'MetaLink', KEY_TYPENAME),
        metas: filterArrayObjects(filtered, ['MetaValue', 'MetaProperty'], KEY_TYPENAME)
    }
}

export default setHeaderData
