import { getRatio } from '../get-ratio';

describe('#getRatio', () => {
    it.each`
        num1     | num2    | expectedNum1 | expectedNum2
        ${50}    | ${100}  | ${1}         | ${2}
        ${75}    | ${100}  | ${3}         | ${4}
        ${33.33} | ${100}  | ${33.33}     | ${100}
        ${20}    | ${60}   | ${1}         | ${3}
        ${50}    | ${60}   | ${5}         | ${6}
        ${1080}  | ${1920} | ${9}         | ${16}
        ${0.25}  | ${1}    | ${1}         | ${4}
        ${0.1}   | ${0.8}  | ${1}         | ${8}
        ${2342}  | ${9367} | ${2342}      | ${9367}
    `(
        'should return a ratio of $expectedNum1:$expectedNum2 when $num1 and $num2 are provided',
        ({ num1, num2, expectedNum1, expectedNum2 }) => {
            // Arrange
            const expected = {
                num1: expectedNum1,
                num2: expectedNum2,
            };

            // Act
            const ratio = getRatio(num1, num2);

            // Assert
            expect(ratio).toEqual(expected);
        }
    );
});
