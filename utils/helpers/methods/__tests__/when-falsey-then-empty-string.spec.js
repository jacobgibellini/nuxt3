/* eslint-disable no-self-compare */
import whenFalseyThenEmptyString from '../when-falsey-then-empty-string';

describe('fn: whenFalseyThenEmptyString', () => {
    it.each`
        value                | expected
        ${0}                 | ${''}
        ${false}             | ${''}
        ${null}              | ${''}
        ${undefined}         | ${''}
        ${1 > 2}             | ${''}
        ${'test' === 'tes'}  | ${''}
        ${1}                 | ${'truthy'}
        ${1 < 2}             | ${'truthy'}
        ${'test' === 'test'} | ${'truthy'}
    `('should return $expected', ({ value, expected }) => {
        const result = whenFalseyThenEmptyString(value, 'truthy');

        expect(result).toBe(expected);
    });
});
