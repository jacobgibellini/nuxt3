import mapObjectArray from '../map-object-array';

describe('fn: mapObjectArray', () => {
    it('should return empty array when array is not passed', () => {
        // Arrange
        const initial = undefined;
        const expected = [];
        const mapper = (item) => item;

        // Act
        const result = mapObjectArray(initial, mapper);

        // Assert
        expect(result).toEqual(expected);
    });

    it('should return mapped array', () => {
        // Arrange
        const initial = [0, 1, 2];
        const expected = [1, 2, 3];
        const mapper = (item) => item + 1;

        // Act
        const result = mapObjectArray(initial, mapper);

        // Assert
        expect(result).toEqual(expected);
    });
});
