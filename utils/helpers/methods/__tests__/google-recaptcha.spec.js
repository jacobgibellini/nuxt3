import { getReCaptchaToken } from '../google-recaptcha';

describe('#getReCaptchaToken', () => {
    const mockGrecaptcha = {
        ready: jest.fn().mockImplementation((cb) => cb()),
        execute: jest.fn(),
    };

    beforeAll(() => {
        global.grecaptcha = mockGrecaptcha;
    });

    afterAll(() => {
        delete global.grecaptcha;
    });

    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('should called with default action "submit"', async () => {
        mockGrecaptcha.execute.mockResolvedValue('MOCKEDTOKEN');

        const actual = await getReCaptchaToken();

        expect(mockGrecaptcha.execute).toHaveBeenCalledWith('MOCKEDRECAPTCHASITEKEY', {
            action: 'submit',
        });
        expect(actual).toBe('MOCKEDTOKEN');
    });

    it('should called with custom action "submit"', async () => {
        mockGrecaptcha.execute.mockResolvedValue('MOCKEDTOKEN');

        const actual = await getReCaptchaToken('submitLead');

        expect(mockGrecaptcha.execute).toHaveBeenCalledWith('MOCKEDRECAPTCHASITEKEY', {
            action: 'submitLead',
        });
        expect(actual).toBe('MOCKEDTOKEN');
    });

    it('should reject error throw by recaptcha', async () => {
        const expected = 'grecaptcha rejected reason(s)';
        mockGrecaptcha.execute.mockRejectedValue('grecaptcha rejected reason(s)');

        try {
            await getReCaptchaToken();
        } catch (error) {
            expect(error).toEqual(expected);
        }
    });

    it('should rejected with an error when recaptcha is not defined', async () => {
        delete global.grecaptcha;
        const expected = ReferenceError('grecaptcha is not defined');

        try {
            await getReCaptchaToken();
        } catch (error) {
            expect(error).toEqual(expected);
        }
    });
});
