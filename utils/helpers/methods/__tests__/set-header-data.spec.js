import setHeaderData from '../set-header-data';

describe('#setHeaderData', () => {
    it('should create empty header data', () => {
        // Arrange
        const initialData = [];
        const expectedResult = {
            links: [],
            metas: [],
            title: '',
        };

        // Act
        const result = setHeaderData(initialData);

        // Assert
        expect(result).toEqual(expectedResult);
    });

    it('should map and populate a header data array into a new object', () => {
        // Arrange
        const initialData = [
            {
                __typename: 'MetaValue',
                key: 'title',
                value: "How to steal Jonny's peanut butter | RMIT Online",
            },
            {
                __typename: 'MetaLink',
                key: 'canonical',
                value: 'https://online.marketstg.rmitonlinestg.com/for-business',
            },
            {
                __typename: 'MetaValue',
                key: 'google-site-verification',
                value: 'somecrazylongvaluethatidontunderstand',
            },
            {
                __typename: 'MetaProperty',
                key: 'og:title',
                value: 'Jonny',
            },
        ];
        const expectedResult = {
            title: "How to steal Jonny's peanut butter | RMIT Online",
            links: [
                {
                    __typename: 'MetaLink',
                    key: 'canonical',
                    value: 'https://online.marketstg.rmitonlinestg.com/for-business',
                },
            ],
            metas: [
                {
                    __typename: 'MetaValue',
                    key: 'google-site-verification',
                    value: 'somecrazylongvaluethatidontunderstand',
                },
                {
                    __typename: 'MetaProperty',
                    key: 'og:title',
                    value: 'Jonny',
                },
            ],
        };

        // Act
        const result = setHeaderData(initialData);

        // Assert
        expect(result).toEqual(expectedResult);
    });

    it('should have an empty title when no data is passed', () => {
        // Arrange
        const initialData = [];
        const expectedResult = '';

        // Act
        const result = setHeaderData(initialData);

        // Assert
        expect(result.title).toEqual(expectedResult);
    });

    it('should ignore robots', () => {
        // Arrange
        const initialData = [
            {
                __typename: 'MetaValue',
                key: 'title',
                value: 'How to ignore robots | RMIT Online',
            },
            {
                __typename: 'MetaLink',
                key: 'canonical',
                value: 'https://online.marketstg.rmitonlinestg.com/for-business',
            },
            {
                __typename: 'MetaValue',
                key: 'google-site-verification',
                value: 'somecrazylongvaluethatidontunderstand',
            },
            {
                __typename: 'MetaValue',
                key: 'robots',
                value: 'Beep Boop',
            },
        ];
        const expectedResult = {
            title: 'How to ignore robots | RMIT Online',
            links: [
                {
                    __typename: 'MetaLink',
                    key: 'canonical',
                    value: 'https://online.marketstg.rmitonlinestg.com/for-business',
                },
            ],
            metas: [
                {
                    __typename: 'MetaValue',
                    key: 'google-site-verification',
                    value: 'somecrazylongvaluethatidontunderstand',
                },
            ],
        };

        // Act
        const result = setHeaderData(initialData);

        // Assert
        expect(result).toEqual(expectedResult);
    });

    it('should not ignore robots when value is "max-image-preview:large"', () => {
        // Arrange
        const initialData = [
            {
                __typename: 'MetaValue',
                key: 'title',
                value: 'How to ignore robots | RMIT Online',
            },
            {
                __typename: 'MetaLink',
                key: 'canonical',
                value: 'https://online.marketstg.rmitonlinestg.com/for-business',
            },
            {
                __typename: 'MetaValue',
                key: 'google-site-verification',
                value: 'somecrazylongvaluethatidontunderstand',
            },
            {
                __typename: 'MetaValue',
                key: 'robots',
                value: 'max-image-preview:large',
            },
        ];
        const expectedResult = {
            title: 'How to ignore robots | RMIT Online',
            links: [
                {
                    __typename: 'MetaLink',
                    key: 'canonical',
                    value: 'https://online.marketstg.rmitonlinestg.com/for-business',
                },
            ],
            metas: [
                {
                    __typename: 'MetaValue',
                    key: 'google-site-verification',
                    value: 'somecrazylongvaluethatidontunderstand',
                },
                {
                    __typename: 'MetaValue',
                    key: 'robots',
                    value: 'max-image-preview:large',
                },
            ],
        };

        // Act
        const result = setHeaderData(initialData);

        // Assert
        expect(result).toEqual(expectedResult);
    });
});
