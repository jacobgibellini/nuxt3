import orderCoursesByStartDate from '../order-courses-by-start-date';

describe('#orderCoursesByStartDate', () => {
    const mockCourses = [
        { startDate: { date: '2021-05-24 12:00:00 UTC' } },
        { startDate: { mockProp: 'anything' } },
        { startDate: { date: '2021-03-22 12:00:00 UTC' } },
        { startDate: {} },
        { startDate: { date: '2021-03-09 12:00:00 UTC' } },
        { startDate: { date: '2021-04-26 12:00:00 UTC' } },
    ];

    it('should courses sorted by date in ascending order by default', () => {
        const expected = [
            { startDate: { date: '2021-03-09 12:00:00 UTC' } },
            { startDate: { date: '2021-03-22 12:00:00 UTC' } },
            { startDate: { date: '2021-04-26 12:00:00 UTC' } },
            { startDate: { date: '2021-05-24 12:00:00 UTC' } },
            { startDate: { mockProp: 'anything' } },
            { startDate: {} },
        ];
        const mockArr = mockCourses.slice();

        const received = orderCoursesByStartDate(mockArr);

        expect(received).toEqual(expected);
    });

    it('should courses sorted by date in descending order when specified', () => {
        const expected = [
            { startDate: { mockProp: 'anything' } },
            { startDate: {} },
            { startDate: { date: '2021-05-24 12:00:00 UTC' } },
            { startDate: { date: '2021-04-26 12:00:00 UTC' } },
            { startDate: { date: '2021-03-22 12:00:00 UTC' } },
            { startDate: { date: '2021-03-09 12:00:00 UTC' } },
        ];
        const mockArr = mockCourses.slice();

        const received = orderCoursesByStartDate(mockArr, true);

        expect(received).toEqual(expected);
    });
});
