import isObjectEmpty from '../is-object-empty';

describe('#isObjectEmpty', () => {
    it.each`
        value                      | expected
        ${true}                    | ${true}
        ${{}}                      | ${true}
        ${'Jonny'}                 | ${true}
        ${{ name: 'Jonny Bravo' }} | ${false}
    `('should return $expected when $value is passed', ({ value, expected }) => {
        // Act
        const result = isObjectEmpty(value);

        // Assert
        expect(result).toBe(expected);
    });
});
