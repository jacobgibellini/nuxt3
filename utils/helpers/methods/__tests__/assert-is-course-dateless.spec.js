import assertIsCourseDateless from '~/utils/helpers/methods/assert-is-course-dateless';

describe('#assertIsCourseDateless', () => {
    it.each`
        entityBundle            | fieldIsActive | expected
        ${'course_page_v2'}     | ${false}      | ${true}
        ${'course_page_v2'}     | ${true}       | ${false}
        ${'external_course'}    | ${false}      | ${false}
        ${'external_course'}    | ${true}       | ${false}
        ${'inhouse_accredited'} | ${false}      | ${false}
        ${'inhouse_accredited'} | ${true}       | ${false}
        ${'program_page'}       | ${false}      | ${false}
        ${'program_page'}       | ${true}       | ${false}
    `(
        'should return $expected when course\'s "entityBundle" is "$entityBundle" and "fieldIsActive" is $fieldIsActive',
        ({ entityBundle, fieldIsActive, expected }) => {
            const mockCourse = {
                entityBundle,
                fieldIsActive,
            };
            const received = assertIsCourseDateless(mockCourse);

            expect(received).toBe(expected);
        }
    );
});
