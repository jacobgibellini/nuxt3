import filterArrayObjects from '../filter-array-objects';

describe('#filterArrayObjects', () => {
    it('should not throw when value is undefined', () => {
        expect(() => filterArrayObjects(undefined, [], 'searchTerm')).not.toThrow();
    });

    it('should return empty array when falsy value is passed', () => {
        // Arrange
        const initialValue = false;
        const expectedValue = [];

        // Act
        const result = filterArrayObjects(initialValue);

        // Assert
        expect(result).toEqual(expectedValue);
    });
});
