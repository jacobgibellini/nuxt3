import scrollToElementById from '../scroll-to-element-by-id';

describe('fn: scrollToElementById', () => {
    global.window = Object.create(window);
    global.window.scrollTo = jest.fn();

    global.document = Object.create(document);
    global.document.getElementById = jest.fn();

    it('should not "scroll" to the element when id is not provided', () => {
        // Arrange
        const spy = jest.spyOn(global.document, 'getElementById');

        // Act
        scrollToElementById();

        // Assert
        expect(spy).not.toHaveBeenCalled();
    });

    it('should not "scroll" to the element if it cannot be found', () => {
        // Arrange
        const scrollSpy = jest.spyOn(global.window, 'scrollTo');
        const documentSpy = jest.spyOn(global.document, 'getElementById');
        const elementId = 'element-id';

        documentSpy.mockReturnValue(undefined);

        // Act
        scrollToElementById(elementId);

        // Assert
        expect(documentSpy).toHaveBeenCalledWith(elementId);
        expect(scrollSpy).not.toHaveBeenCalledWith();
    });

    it('should "scroll" to the element', () => {
        // Arrange
        const scrollSpy = jest.spyOn(global.window, 'scrollTo');
        const documentSpy = jest.spyOn(global.document, 'getElementById');
        const elementId = 'element-id';

        documentSpy.mockReturnValue({ offsetTop: 100 });

        // Act
        scrollToElementById(elementId);

        // Assert
        expect(documentSpy).toHaveBeenCalledWith(elementId);
        expect(scrollSpy).toHaveBeenCalledWith({
            top: 100,
            behavior: 'smooth',
        });
    });

    it('should "scroll" to the element when id starts with #', () => {
        // Arrange
        const scrollSpy = jest.spyOn(global.window, 'scrollTo');
        const documentSpy = jest.spyOn(global.document, 'getElementById');
        const elementId = '#element-id';

        documentSpy.mockReturnValue({ offsetTop: 100 });

        // Act
        scrollToElementById(elementId);

        // Assert
        expect(documentSpy).toHaveBeenCalledWith('element-id');
        expect(scrollSpy).toHaveBeenCalledWith({
            top: 100,
            behavior: 'smooth',
        });
    });
});
