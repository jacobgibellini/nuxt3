import isEven from '../is-even';

describe('fn: isEven', () => {
    it.each`
        num   | expected
        ${0}  | ${true}
        ${1}  | ${false}
        ${2}  | ${true}
        ${3}  | ${false}
        ${4}  | ${true}
        ${5}  | ${false}
        ${6}  | ${true}
        ${7}  | ${false}
        ${8}  | ${true}
        ${9}  | ${false}
        ${10} | ${true}
    `('should return $expected', ({ num, expected }) => {
        const result = isEven(num);

        expect(result).toBe(expected);
    });
});
