import arraysAreEqual from '../arrays-are-equal';

describe('fn: arraysAreEqual', () => {
    it.each`
        arrOne                 | arrTwo                 | expected
        ${[]}                  | ${[]}                  | ${true}
        ${['']}                | ${['']}                | ${true}
        ${[[], []]}            | ${[[], []]}            | ${true}
        ${[[[[]]]]}            | ${[[[[]]]]}            | ${true}
        ${[undefined]}         | ${[undefined]}         | ${true}
        ${[false]}             | ${[false]}             | ${true}
        ${[true, false, 0, 1]} | ${[true, false, 0, 1]} | ${true}
        ${undefined}           | ${[]}                  | ${false}
        ${['']}                | ${['', '']}            | ${false}
        ${['']}                | ${['test']}            | ${false}
        ${[['', '']]}          | ${[['', '', '']]}      | ${false}
    `('should return $expected', ({ arrOne, arrTwo, expected }) => {
        const result = arraysAreEqual(arrOne, arrTwo);

        expect(result).toBe(expected);
    });
});
