import windowRoute from '../window-route';

describe('fn: windowRoute', () => {
    beforeEach(() => {
        global.window = Object.create(window);
        Object.defineProperty(window, 'location', {
            value: {
                href: '/courses',
                origin: 'http://online.rmit.edu.test',
            },
        });
    });

    it('should not navigate to another page on the server', () => {
        // Arrange
        process.client = false;
        const expected = '/courses';

        // Act
        windowRoute('/for-business');

        // Assert
        expect(global.window.location.href).toBe(expected);
    });

    it('should navigate to another page', () => {
        // Arrange
        process.client = true;
        const expected = 'http://online.rmit.edu.test/for-business';

        // Act
        windowRoute('/for-business');

        // Assert
        expect(global.window.location.href).toBe(expected);
    });
});
