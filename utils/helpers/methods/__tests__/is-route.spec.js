import isRoute from '../is-route';

describe('fn: isRoute', () => {
    it.each`
        route                               | expected
        ${'/courses'}                       | ${true}
        ${'http://localhost:3000/courses'}  | ${true}
        ${'https://localhost:3000/courses'} | ${true}
        ${'htp://localhost:3000/courses'}   | ${false}
        ${'courses'}                        | ${false}
        ${''}                               | ${false}
        ${null}                             | ${false}
        ${undefined}                        | ${false}
    `('should return $expected', ({ route, expected }) => {
        const result = isRoute(route);

        expect(result).toBe(expected);
    });
});
