import replaceFirstChars from '../replace-first-chars';

describe('fn: replaceFirstChars', () => {
    it('should return new string with changed first characters', () => {
        // Arrange
        const initial = {
            str: 'Hi World',
            firstChars: 'Hi',
            replacement: 'Hello',
        };
        const expected = 'Hello World';

        // Act
        const result = replaceFirstChars(initial.str, initial.firstChars, initial.replacement);

        // Assert
        expect(result).toEqual(expected);
    });

    it('should return same string if first characters do not match', () => {
        // Arrange
        const initial = {
            str: 'Hi World',
            firstChars: 'Does not match',
            replacement: 'Hello',
        };
        const expected = 'Hi World';

        // Act
        const result = replaceFirstChars(initial.str, initial.firstChars, initial.replacement);

        // Assert
        expect(result).toEqual(expected);
    });
});
