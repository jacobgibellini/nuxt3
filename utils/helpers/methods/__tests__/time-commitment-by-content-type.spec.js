import TIME_COMMITMENT_BY_CONTENT_TYPE from '../time-commitment-by-content-type';

describe('TIME_COMMITMENT_BY_CONTENT_TYPE', () => {
    it('should return time commitment (duration) correctly when content type is "program_page"', () => {
        const expected = 'duration from program page';
        const input = {
            fieldCourseDuration: {
                processed: expected,
            },
        };

        const output = TIME_COMMITMENT_BY_CONTENT_TYPE.program_page(input);

        expect(output).toBe(expected);
    });

    it.each`
        fieldAcceleratedDuration | fieldDuration | expected
        ${6}                     | ${12}         | ${'6 - 12 months'}
        ${6}                     | ${undefined}  | ${'6 months'}
        ${undefined}             | ${12}         | ${'12 months'}
        ${undefined}             | ${undefined}  | ${''}
    `(
        'should return time commitment (duration) correctly when content type is "inhouse_accredited"',
        ({ fieldAcceleratedDuration, fieldDuration, expected }) => {
            const input = {
                fieldAcceleratedDuration,
                fieldDuration,
            };

            const output = TIME_COMMITMENT_BY_CONTENT_TYPE.inhouse_accredited(input);

            expect(output).toBe(expected);
        }
    );

    it('should return time commitment (duration) correctly when content type is "course_page_v2"', () => {
        const expected = 'duration from course page v2';
        const input = {
            fieldCourseTimeCommitment: expected,
        };

        const output = TIME_COMMITMENT_BY_CONTENT_TYPE.course_page_v2(input);

        expect(output).toBe(expected);
    });

    it('should return time commitment (duration) correctly when content type is "external_course"', () => {
        const expected = 'duration from external course';
        const input = {
            fieldCourseTimeCommitment: expected,
        };

        const output = TIME_COMMITMENT_BY_CONTENT_TYPE.external_course(input);

        expect(output).toBe(expected);
    });

    it('should return undefined when attempt lookup for invalid content type', () => {
        const output = TIME_COMMITMENT_BY_CONTENT_TYPE['invalid-example'];

        expect(output).toBeUndefined();
    });
});
