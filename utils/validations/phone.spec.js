import {
    isValidAustralianNumber,
    isValidOtherCountryNumber
} from './phone'

describe('#isValidAustralianNumber', () => {
    it.each`
    phoneNumber          | expected
    ${'+60411111111'}    | ${false}
    ${'+61 411 111 111'} | ${true}
    ${'+61'}             | ${false}
    ${'+61411111111'}    | ${true}
    ${'+61411111aus'}    | ${false}
    ${'+6141111a111'}    | ${false}
    ${'+614156789112'}   | ${false}
    ${'+6141567899'}     | ${false}
    ${'0411111111'}      | ${false}
    ${'614345678912'}    | ${false}
    `('should return $expected when phone number is $phoneNumber', ({
        phoneNumber,
        expected
    }) => {
        const received = isValidAustralianNumber(phoneNumber)

        expect(received).toBe(expected)
    });
});

describe('#isValidOtherCountryNumber', () => {
    it.each`
    phoneNumber          | expected
    ${'+'}               | ${false}
    ${'+6'}              | ${true}
    ${'+60411111111'}    | ${true}
    ${'+61 411 111 111'} | ${true}
    ${'+61'}             | ${true}
    ${'+61411111111'}    | ${true}
    ${'+6141111111a'}    | ${false}
    ${'+6141111a111'}    | ${false}
    ${'+614156789112'}   | ${true}
    ${'+6141567899'}     | ${true}
    ${'0411111111'}      | ${false}
    ${'614345678912'}    | ${false}
    `('should return $expected when phone number is $phoneNumber', ({
        phoneNumber,
        expected
    }) => {
        const received = isValidOtherCountryNumber(phoneNumber)

        expect(received).toBe(expected)
    });
});