export const isValidAustralianNumber = (phoneNumber) => {
    const _phoneNumber  = phoneNumber.replace(/\s/g, '')
    const len           = _phoneNumber.length

    return !isNaN(_phoneNumber)
        && _phoneNumber.startsWith('+61')
        && len === 12
}

export const isValidOtherCountryNumber = (phoneNumber) => {
    const _phoneNumber = phoneNumber.replace(/\s/g, '')
    const len          = _phoneNumber.length

    return !isNaN(_phoneNumber)
        && _phoneNumber.startsWith('+')
        && len > 1
}