/* eslint-disable */
export const isEmpty = (str) => {
    return !!str;
};

export const isAlphaChars = (str) => {
    return /^[a-zA-Z][a-zA-Z\-\s\']{0,20}$/.test(str);
};

export const hasMinChars = (str, minLength) => {
    return str.length > minLength;
};

// Combination of isEmpty and isAlphaNumeric validations for basic input field validation
export const inputRulesEmptyAndAlphaNumeric = (fieldName = 'Field') => {
    return [
        (val) => isEmpty(val) || `${fieldName} is required`,
        (val) => isAlphaChars(val) || `${fieldName} must be valid`,
    ];
};

export const inputRulesFreeTextWithMaxLength = (fieldName, minLength = 2, maxLength = 250) => [
    (val) => isEmpty(val) || `${fieldName} is required`,
    (val) => !val.startsWith(' ') || `${fieldName} must be valid`,
    (val) => val.length < maxLength || `${fieldName} must be valid`,
    (val) =>
        hasMinChars(val, minLength) || `${fieldName} must have more than ${minLength} characters.`,
];

export const isEmail = (str) => {
    return /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(str);
};

export const isValidInput = (fieldName) => {
    return [
        ...inputRulesEmptyAndAlphaNumeric(fieldName),
        (val) => /^[a-zA-Z][a-zA-Z\-\s\']{1,20}$/.test(val) || `${fieldName} must be valid`,
    ];
};
