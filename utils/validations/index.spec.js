import { inputRulesFreeTextWithMaxLength } from './index';

describe('#inputRulesFreeTextWithMaxLength', () => {
    const mockFieldName      = 'Mock Field Name';
    const defaultValidations = inputRulesFreeTextWithMaxLength(mockFieldName);

    it('should return expected error message when input value was empty', () => {
        const expected = [
            'Mock Field Name is required',
            true,
            true,
            'Mock Field Name must have more than 2 characters.'
        ];
        const mockInputValue = '';
        const received = defaultValidations.map(fn => fn(mockInputValue));

        expect(received).toEqual(expected);
    });

    it('should return expected error message when input value started with blank space', () => {
        const expected = [
            true,
            'Mock Field Name must be valid',
            true,
            'Mock Field Name must have more than 2 characters.'
        ];
        const mockInputValue = ' ';
        const received = defaultValidations.map(fn => fn(mockInputValue));

        expect(received).toEqual(expected);
    });

    it.each`
    mockInputCharacters | expectedTestResult
    ${249}              | ${true}
    ${250}              | ${'Mock Field Name must be valid'}
    ${251}              | ${'Mock Field Name must be valid'}
    `('should max length validation return $expectedTestResult when input value have $mockInputCharacters characters', ({
        mockInputCharacters,
        expectedTestResult
    }) => {
        const expected = [
            true,
            true,
            expectedTestResult,
            true
        ];
        const mockInputValue = 'x'.repeat(mockInputCharacters);
        const received = defaultValidations.map(fn => fn(mockInputValue));

        expect(received).toEqual(expected);
    });

    it.each`
    mockInputCharacters | expectedTestResult
    ${3}                | ${true}
    ${2}                | ${'Mock Field Name must have more than 2 characters.'}
    ${1}                | ${'Mock Field Name must have more than 2 characters.'}
    `('should min length validation return $expectedTestResult when input value have $mockInputCharacters characters', ({
        mockInputCharacters,
        expectedTestResult
    }) => {
        const expected = [
            true,
            true,
            true,
            expectedTestResult
        ];
        const mockInputValue = 'x'.repeat(mockInputCharacters);
        const received = defaultValidations.map(fn => fn(mockInputValue));

        expect(received).toEqual(expected);
    });

    describe('Given minLength has set to 5 and maxLength has set to 8', () => {
        const customizedValidation = inputRulesFreeTextWithMaxLength(mockFieldName, 5, 8);

        it.each`
        mockInputCharacters | expectedTestResult
        ${7}                | ${true}
        ${8}                | ${'Mock Field Name must be valid'}
        ${9}                | ${'Mock Field Name must be valid'}
        `('should max length validation return $expectedTestResult when input value have $mockInputCharacters characters', ({
            mockInputCharacters,
            expectedTestResult
        }) => {
            const expected = [
                true,
                true,
                expectedTestResult,
                true
            ];
            const mockInputValue = 'x'.repeat(mockInputCharacters);
            const received = customizedValidation.map(fn => fn(mockInputValue));

            expect(received).toEqual(expected);
        });

        it.each`
        mockInputCharacters | expectedTestResult
        ${6}                | ${true}
        ${5}                | ${'Mock Field Name must have more than 5 characters.'}
        ${4}                | ${'Mock Field Name must have more than 5 characters.'}
        `('should min length validation return $expectedTestResult when input value have $mockInputCharacters characters', ({
            mockInputCharacters,
            expectedTestResult
        }) => {
            const expected = [
                true,
                true,
                true,
                expectedTestResult
            ];
            const mockInputValue = 'x'.repeat(mockInputCharacters);
            const received = customizedValidation.map(fn => fn(mockInputValue));

            expect(received).toEqual(expected);
        });
    });
});