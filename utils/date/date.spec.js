import moment from 'moment';
import {
    formatDate,
    formatNextStartDate,
    getDateDifference,
    getDateLabelByTagType,
    utcTo8601
} from './index'

describe('Date', () => {
    describe('formatDate', () => {
        const validDates = [
            {
                utcDate: '2020-02-03 12:00:00 UTC',
                format: undefined,
                expectedFormattedDate: '3 Feb 2020'
            },
            {
                utcDate: '2020-02-03 12:00:00 UTC',
                format: 'DD MMM YYYY',
                expectedFormattedDate: '03 Feb 2020'
            },
            {
                utcDate: '2020-03-05 12:00:00 UTC',
                format: 'D MMM YYYY',
                expectedFormattedDate: '5 Mar 2020'
            },
            {
                utcDate: '2020-05-05 12:00:00 UTC',
                format: 'MMM YYYY',
                expectedFormattedDate: 'May 2020'
            },
            {
                utcDate: '2021-01-30 12:00:00 UTC',
                format: 'DD/MM/YYYY',
                expectedFormattedDate: '30/01/2021'
            }
        ]

        const invalidDates = [
            {
                invalidDate: undefined,
                testCase: 'date is undefined'
            },
            {
                invalidDate: '',
                testCase: 'date is an empty string'
            },
            {
                invalidDate: '2020-03- 12:00:00 UTC',
                testCase: 'utc date is invalid'
            }      
        ]

        /** VALID DATES TEST CASES */
        it('should convert a UTC date in the format of D MM YYYY by default when a format is not passed', () => {
            // Arrange
            const utcDate = '2020-12-09 12:00:00 UTC'
            const expectedFormattedDate = '9 Dec 2020'

            // Act
            const formattedDate = formatDate(utcDate)

            // Assert
            expect(formattedDate).toBe(expectedFormattedDate)
        })

        validDates.forEach(date => {
            it('should convert a UTC date in the format of ' + date.format, () => {
                // Act
                const formattedDate = formatDate(date.utcDate, date.format)
    
                // Assert
                expect(formattedDate).toBe(date.expectedFormattedDate)
            })
        })

        /** INVALID DATES TEST CASES */
        invalidDates.forEach(date => {
            it('should return Invalid Date when ' + date.testCase, () => {
                // Arrange
                const expectedFormattedDate = 'Invalid date'

                // Act
                const formattedDate = formatDate(date.invalidDate, 'DD MMM YYYY')
    
                // Assert
                expect(formattedDate).toBe(expectedFormattedDate)
            })
        })
    })

    describe('formatNextStartDate', () => {
        it.each`
        inputDate                    | expected
        ${'2020-09-21 12:00:00 UTC'} | ${'21 Sep 2020'}
        ${'2020-11-02 12:00:00 UTC'} | ${'2 Nov 2020'}
        `('should format drupal date from "$inputDate" to "$expected"', ({
            inputDate,
            expected
        }) => {
            // Act
            const formattedDate = formatNextStartDate(inputDate)

            // Assert
            expect(formattedDate).toBe(expected)
        })
    })

    describe('getDateLabelByTagType', () => {
        it('should return "NEXT START" label when course machinename is futureskills course or bundle', () => {
            // Arrange
            const fsMachineName = 'future_skills'
            const fsBundleMachineName = 'future_skills_bundle'
            const expectedLabel = 'NEXT START'

            // Act
            const dateLabelFutureSkills = getDateLabelByTagType(fsMachineName)
            const dateLabelFutureSkillsBundle = getDateLabelByTagType(fsBundleMachineName)

            // Assert
            expect(dateLabelFutureSkills).toBe(expectedLabel)
            expect(dateLabelFutureSkillsBundle).toBe(expectedLabel)
        })

        it('should return "APPLICATIONS CLOSE" when course machinename is postgraduate or undergraduate', () => {
            // Arrange
            const postgraduateMachineName = 'postgraduate'
            const undergraduateMachineName = 'undergraduate'
            const expectedLabel = 'APPLICATIONS CLOSE'

            // Act
            const dateLabelPostgrad = getDateLabelByTagType(postgraduateMachineName)
            const dateLabelUndergrad = getDateLabelByTagType(undergraduateMachineName)

            // Assert
            expect(dateLabelPostgrad).toBe(expectedLabel)
            expect(dateLabelUndergrad).toBe(expectedLabel)
        })
    })

    describe('utcTo8601', () => {
        it.each`
        inputDate                    | expected
        ${'2020-11-16 12:00:00 UTC'} | ${'2020-11-16T12:00:00'}
        ${'2020-09-21 12:00:00 UTC'} | ${'2020-09-21T12:00:00'}
        ${'2020-10-05 12:00:00 UTC'} | ${'2020-10-05T12:00:00'}
        ${'2019-08-19 12:00:00 UTC'} | ${'2019-08-19T12:00:00'}
        `('should normalize drupal date from "$inputDate" to ISO format - "$expected"', ({
            inputDate,
            expected
        }) => {
            // Act
            const actual = utcTo8601(inputDate)

            // Assert
            expect(actual).toBe(expected)
        })
    })


    describe('getDateDifference', () => {
        it.each`
        inputDate   | expected
        ${moment().format('YYYY-MM-DD')} | ${0}
        ${moment().add('5','days').format('YYYY-MM-DD')} | ${5}
        ${moment().add('25','days').format('YYYY-MM-DD')} | ${25}
        ${moment().subtract('5','days').format('YYYY-MM-DD')} | ${-5}
        `('should return difference in number of days between today and "$inputDate" as - "$expected"', ({
             inputDate,
             expected
         }) => {
            const actual = getDateDifference(inputDate)
            expect(actual).toBe(expected)
        })
    })
})