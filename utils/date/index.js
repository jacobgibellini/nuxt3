import moment from 'moment';
import { DATE_HEADING_LABELS, TAG_TYPES } from '@/utils/enums'

/*
 * @param { string } inputDate - 2019-08-19 12:00:00 UTC
 */
export const utcTo8601 = inputDate => {
    const [ date, time ] = inputDate.split(' ');

    return `${date}T${time}`;
}

export const formatNextStartDate = inputDate =>
    moment(utcTo8601(inputDate))
        .format('D MMM YYYY')

export const formatDate = (inputDate = '', dateFormat = 'D MMM YYYY') =>
    moment(utcTo8601(inputDate))
        .format(dateFormat)

/**
 * This will give the correct date heading label to be displayed within a course card
 * for a particular category type (Futureskills short course, bundle or accredited). 
 * 
 * @param { String } tagTypeMachineName - the category machine name: future_skills_bundle, postgraduate etc.
 * @returns { String }
 */
export const getDateLabelByTagType = tagTypeMachineName => {
    return {
        [TAG_TYPES.FS_BUNDLE]: DATE_HEADING_LABELS.FS_BUNDLE,
        [TAG_TYPES.FS_SHORT_COURSE]: DATE_HEADING_LABELS.FS_SHORT,
        [TAG_TYPES.POSTGRADUATE]: DATE_HEADING_LABELS.ACCREDITED,
        [TAG_TYPES.UNDERGRADUATE]: DATE_HEADING_LABELS.ACCREDITED
    }[ tagTypeMachineName ]
}

export const getDateDifference = (date) => {
    const today = moment().startOf('day');
    const startDate = moment(date).startOf('day')
    const dateDiff = startDate.diff(today, 'days')
    return dateDiff;
}