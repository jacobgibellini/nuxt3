import filterMissingProps from '../filter-missing-props';

describe('fn: filterMissingProps', () => {
    it('should filter missing properties', () => {
        // Arrange
        const requiredPropertiesList = ['fruit', 'vegetable'];
        const propsObject = {
            fruit: true,
        };
        const expected = ['vegetable'];

        // Act
        const result = filterMissingProps(requiredPropertiesList, propsObject);

        // Assert
        expect(result).toEqual(expected);
    });
});
