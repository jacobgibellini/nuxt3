/**
 * Filters through an array of required properties / keys
 * that an object must have.
 * If an object key is required and is undefined, it will be returned in the filtered array.
 *
 * @param { Array<string> } requiredPropertiesList - List containing the required object keys
 * @param { Object } fieldsObject - Object that contains the keys and values
 * @returns { Array<string> }
 */
const filterMissingProps = (requiredPropertiesList, propsObject) =>
    requiredPropertiesList.filter((prop) => {
        if (propsObject[prop] === undefined) {
            return prop;
        }
    });

export default filterMissingProps;
