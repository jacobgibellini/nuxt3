export const transformCourseCategoriesFilters = (
    matchList,
    course
) => {
    const categoryName = course.fieldPrimaryIndustry?.entity?.machineName;

    const subCategoryParentNames = course.fieldIndustry?.reduce((
        subCategoryNames,
        subCategory
    ) => {
        const subCategoryName = subCategory?.entity?.machineName;
        const matchedCategory = matchList[subCategoryName];

        return [
            ...subCategoryNames,
            ...(matchedCategory ? [matchedCategory] : [])
        ]
    }, []);

    return [
        ...new Set([
            ...(categoryName ? [categoryName] : []),
            ...subCategoryParentNames
        ])
    ];
};