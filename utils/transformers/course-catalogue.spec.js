import { transformCourseCategoriesFilters } from './course-catalogue';

const MATCH_LIST = {
    'artificial_intelligence': 'artificial_intelligence',
    'business': 'business',
    'blockchain': 'business',
    'data_analytics': 'business',
    'finance_and_accounting': 'business',
    'human_resources': 'business',
    'law': 'business',
    'product_management': 'business',
    'project_management': 'business',
    'strategy': 'business',
    'supply_chain_and_logistics': 'business',
    'data_science_0': 'data_science_0',
    'data_analytics_0': 'data_science_0',
    'design': 'design',
    'cx_design_strategy': 'design',
    'design_thinking': 'design',
    'ux_ui_design': 'design',
    'development': 'development',
    'app_development': 'development',
    'cloud_software_solutions': 'development',
    'data_science': 'development',
    'programming': 'development',
    'technology': 'development',
    'vr_ar': 'development',
    'web_development': 'development',
    'it_security': 'it_security',
    'cyber_security': 'it_security',
    'marketing': 'marketing',
    'digital_marketing': 'marketing',
    'science_engineering': 'science_engineering',
    'engineering': 'science_engineering',
    'science': 'science_engineering'
};

describe('#transformCourseCategoriesFilters', () => {
    it('should return a list of primary categories', () => {
        const expected = [
            'design',
            'marketing',
            'business'
        ];
        const course = {
            fieldPrimaryIndustry: {
                entity: {
                    machineName: 'design'
                }
            },
            fieldIndustry: [
                {
                    entity: {
                        machineName: 'digital_marketing'
                    }
                },
                {
                    entity: {
                        machineName: 'human_resources'
                    }
                }
            ]
        };

        const output = transformCourseCategoriesFilters(MATCH_LIST, course);

        expect(output).toEqual(expected);
    });

    it('should return the categories associated with it when there is an invalid category and a valid sub category', () => {
        const expected = [ 'business' ];
        const course = {
            fieldPrimaryIndustry: {
                entity: null
            },
            fieldIndustry: [
                {
                    entity: {
                        machineName: 'project_management'
                    }
                }
            ]
        };

        const output = transformCourseCategoriesFilters(MATCH_LIST, course);

        expect(output).toEqual(expected);
    });

    it('should return array of values that have no duplicate value(s)', () => {
        const expected = ['design'];
        const course = {
            fieldPrimaryIndustry: {
                entity: 'design'
            },
            fieldIndustry: [
                {
                    entity: {
                        machineName: 'cx_design_strategy'
                    }
                },
                {
                    entity: {
                        machineName: 'design'
                    }
                }
            ]
        };

        const output = transformCourseCategoriesFilters(MATCH_LIST, course);

        expect(output).toEqual(expected);
    });

    it('should return empty array when all categories are invalid', () => {
        const expected = [];
        const course = {
            fieldPrimaryIndustry: {
                entity: null
            },
            fieldIndustry: [
                {
                    entity: null
                },
                {
                    entity: null
                }
            ]
        };

        const output = transformCourseCategoriesFilters(MATCH_LIST, course);

        expect(output).toEqual(expected);
    });
});