import { mapFaqDetailsToAccordionPanel } from '@/utils/mappers';

const GROUP_COUNTER_START_INDEX = 0;

const generateGroupIdByIndex = index => `group${index}`;

export const assertIsFaqHeader = ({ entityBundle }) => entityBundle === 'faq_header';
export const assertIsFaqItem   = ({ entityBundle }) => entityBundle === 'faq_item';

export const generateInitialItem = firstItem => ({
    [generateGroupIdByIndex(GROUP_COUNTER_START_INDEX)]: {
        title: assertIsFaqHeader(firstItem)
            ? firstItem?.info
            : '',
        items: assertIsFaqItem(firstItem)
            ? [ mapFaqDetailsToAccordionPanel({
                    ...firstItem,
                    entityId: generateItemFakeEntityId(firstItem, GROUP_COUNTER_START_INDEX)
                })]
            : []
    }
});

export const generateItemFakeEntityId = (
    {
        entityId,
        entityBundle = '',
        fieldCategory = ''
    },
    idx = 0
) => (typeof entityId === 'string' && entityId.trim().length > 0) || typeof entityId === 'number'
    ? entityId
    : [ entityBundle, fieldCategory, idx ].join('-');

export const transformFaqCategoryDetailsToGroups = category => {
    if (category?.length < 1)
        return [];

    const [firstItem, ...remainingItems] = category;
    const initialItem = generateInitialItem(firstItem);

    if (remainingItems?.length === 0)
        return initialItem;

    return remainingItems.reduce(
        (acc, item, idx) => {
            const isHeader         = assertIsFaqHeader(item);
            const isItem           = assertIsFaqItem(item);
            const prevGroupCounter = acc.groupCounter;
            const prevGroupItem    = acc[generateGroupIdByIndex(prevGroupCounter)];
            const nextGroupCounter = isHeader ? prevGroupCounter + 1 : prevGroupCounter;

            return {
                ...acc,
                [generateGroupIdByIndex(nextGroupCounter)]: {
                    title: isHeader ? item.info : prevGroupItem.title,
                    items: isHeader
                        ? []
                        : [
                            ...prevGroupItem.items,
                            ...(isItem ? [ mapFaqDetailsToAccordionPanel({ ...item, entityId: generateItemFakeEntityId(item, idx) }) ] : [])
                        ]
                },
                groupCounter: nextGroupCounter
            };
        },
        {
            groupCounter: GROUP_COUNTER_START_INDEX,
            ...initialItem
        }
    )
};
