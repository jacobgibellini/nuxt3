import {
    transformParentsToBreadcrumbTrails,
    transformBreadcrumbTrail,
    validateBreadcrumbHasLabelAndUrl
} from './breadcrumb';

describe('#validateBreadcrumbHasLabelAndUrl', () => {
    const PAYLOAD_WITHOUT_REQUIRE_PROPERTIES = {};
    const PAYLOAD_WITHOUT_ENTITY_LABEL       = { entityUrl: { path: '/category-name' } };
    const PAYLOAD_WITHOUT_ENTITY_URL         = { entityLabel: 'Category Name' };
    const PAYLOAD_WITHOUT_PATH               = { entityLabel: 'Category Name', entityUrl: null };
    const VALID_PAYLOAD                      = { entityLabel: '', entityUrl: { path: '' } }

    it.each`
        payload                               | description                                       | expected
        ${PAYLOAD_WITHOUT_REQUIRE_PROPERTIES} | ${'entityLabel and entityUrl does not exist'}     | ${false}
        ${PAYLOAD_WITHOUT_ENTITY_LABEL}       | ${'entityLabel property does not exist'}          | ${false}
        ${PAYLOAD_WITHOUT_ENTITY_URL}         | ${'entityUrl property does not exist'}            | ${false}
        ${PAYLOAD_WITHOUT_PATH}               | ${'entityUrl doesn not have a path property'}     | ${false}
        ${VALID_PAYLOAD}                      | ${'both entityLabel and entityUrl.path is valid'} | ${true}
    `('should return $expected when $description', ({
        payload,
        expected
    }) => {
        const output = validateBreadcrumbHasLabelAndUrl(payload);
        expect(output).toBe(expected);
    });
});

describe('#transformBreadcrumbTrail', () => {
    it('should return an object with "label" and "href" both with null value', () => {
        const expected = {
            label: null,
            href: null
        };

        const output = transformBreadcrumbTrail();

        expect(output).toEqual(expected);
    });

    it('should successfully transform payload data to an object with valid "label" and "href" values', () => {
        const payload = {
            entityLabel: 'Category Name',
            entityUrl: {
                path: '/category-name'
            }
        };
        const expected = {
            label: 'Category Name',
            href: '/category-name'
        };

        const output = transformBreadcrumbTrail(payload);

        expect(output).toEqual(expected);
    });

    it('should successfully transform breadcrumb array payload data to an object with valid "label" and "href" values', () => {
        const payload = {
            text: 'Category Name',
            url: {
                path: '/category-name'
            }
        };
        const expected = {
            label: 'Category Name',
            href: '/category-name'
        };

        const output = transformBreadcrumbTrail(payload);

        expect(output).toEqual(expected);
    });
});

describe('#transformParentsToBreadcrumbTrails', () => {
    it('should return 1 breadcrumb trail only while 2nd payload doesn\'t have all required fields', () => {
        const payload = [
            {
                "entity": {
                    "entityLabel": "Level 2",
                    "entityUrl": {
                        "path": "/level-2"
                    }
                }
            },
            {
                "entity": {
                    "entityLabel": "Level 3",
                }
            }
        ];
        const expected = [
            { label: 'Level 2', href: '/level-2' }
        ];

        const output = transformParentsToBreadcrumbTrails(payload);

        expect(output).toEqual(expected);
    });

    it('should return both (x2) transformed breadcrumb trails while every items have all required fields', () => {
        const payload = [
            {
                "entity": {
                    "entityLabel": "Level 2",
                    "entityUrl": {
                        "path": "/level-2"
                    }
                }
            },
            {
                "entity": {
                    "entityLabel": "Level 3",
                    "entityUrl": {
                        "path": "/level-3"
                    }
                }
            }
        ];
        const expected = [
            { label: 'Level 2', href: '/level-2' },
            { label: 'Level 3', href: '/level-3' }
        ];

        const output = transformParentsToBreadcrumbTrails(payload);

        expect(output).toEqual(expected);
    });

    it('should return empty array if payload value is "ilovermitonlinethebest"', () => {
        const output = transformParentsToBreadcrumbTrails('ilovermitonlinethebest');

        expect(output).toEqual(undefined);
    });
});