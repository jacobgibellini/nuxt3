import {
    assertIsFaqHeader,
    assertIsFaqItem,
    generateInitialItem,
    generateItemFakeEntityId,
    transformFaqCategoryDetailsToGroups
} from './faq';

describe('#assertIsFaqHeader', () => {
    it('should return true when "entityBundle" value was "faq_header"', () => {
        const output = assertIsFaqHeader({ entityBundle: 'faq_header' });

        expect(output).toBe(true);
    });

    it('should return false when "entityBundle" value wasn\'t "faq_header"', () => {
        const output = assertIsFaqHeader({ entityBundle: 'faq_item' });

        expect(output).toBe(false);
    });

    it('should return false when payload value was a string', () => {
        const output = assertIsFaqHeader('faq_header');

        expect(output).toBe(false);
    });
});

describe('#assertIsFaqItem', () => {
    it('should return true when "entityBundle" value was "faq_item"', () => {
        const output = assertIsFaqItem({ entityBundle: 'faq_item' });

        expect(output).toBe(true);
    });

    it('should return false when "entityBundle" value wasn\'t "faq_item"', () => {
        const output = assertIsFaqItem({ entityBundle: 'faq_header' });

        expect(output).toBe(false);
    });

    it('should return false when payload value was a string', () => {
        const output = assertIsFaqItem('faq_item');

        expect(output).toBe(false);
    });
});

describe('#generateInitialItem', () => {
    it('should item.entityBundle is "faq_header" map item.info value to group0.title', () => {
        const item = {
            entityBundle: 'faq_header',
            info: 'Credentials and qualifications',
            fieldCategory: 'futureskills'
        };
        const expected = {
            group0: {
                title: 'Credentials and qualifications',
                items: []
            }
        };

        const output = generateInitialItem(item);

        expect(output).toEqual(expected);
    });

    it('should item.entityBundle is "faq_item" map item value to be the only children of group0.items', () => {
        const item = {
            "entityBundle": "faq_item",
            "info": "What recognition will I get for completing this course?",
            "body": {
                "processed": "<p>you will earn a digital credential by Credly</p>"
            },
            "fieldCategory": "futureskills"
        };
        const expected = {
            group0: {
                title: '',
                items: [{
                    id: 'faq_item-futureskills-0',
                    label: 'What recognition will I get for completing this course?',
                    contentHtml: '<p>you will earn a digital credential by Credly</p>'
                }]
            }
        };

        const output = generateInitialItem(item);

        expect(output).toEqual(expected);
    });
});

describe('#generateItemFakeEntityId', () => {
    it('should return entityId when available', () => {
        const item = {
            "entityId": "123",
            "entityBundle": "faq_item",
            "fieldCategory": "futureskills"
        };
        const output = generateItemFakeEntityId(item);

        expect(output).toBe('123');
    });

    it('should return "faq_item-futureskills-0" when entityId is not available and index is not provided', () => {
        const item = {
            "entityBundle": "faq_item",
            "fieldCategory": 'futureskills'
        };
        const output = generateItemFakeEntityId(item);

        expect(output).toBe('faq_item-futureskills-0');
    });

    it('should return "faq_item-futureskills-5" when entityId is not available and index of 5 is provided', () => {
        const item = {
            "entityBundle": "faq_item",
            "fieldCategory": 'futureskills'
        }
        const output = generateItemFakeEntityId(item, 5);

        expect(output).toBe('faq_item-futureskills-5');
    });
});

describe('#transformFaqCategoryDetailsToGroups', () => {
    it('should group category data\'s entityBundle "faq_item" and "faq_header" items in ascending order', () => {
        const faqDetails = [
            {
                "entityBundle": "faq_item",
                "info": "first no faq_item and no faq_header label",
                "body": {
                    "processed": "first no faq_item and no faq_header contentHtml"
                },
                "fieldCategory": 'justcategoryplaceholder'
            },
            {
                "entityBundle": "invalid_item",
                "info": "invalid item label",
                "body": {
                    "processed": "invalid item contentHtml"
                },
                "fieldCategory": 'justcategoryplaceholder'
            },
            {
                "entityBundle": "faq_header",
                "info": "first faq_header in the list",
                "fieldCategory": "justcategoryplaceholder"
            },
            {
                "entityBundle": "faq_item",
                "info": "first faq_item after first faq_header in the list label",
                "fieldCategory": 'justcategoryplaceholder',
                "body": {
                    "processed": "first faq_item after first faq_header in the list contentHtml"
                }
            },
            {
                "entityBundle": "faq_item",
                "info": "another faq_item after first faq_header in the list label",
                "fieldCategory": 'justcategoryplaceholder',
                "body": {
                    "processed": "another faq_item after first faq_header in the list contentHtml"
                }
            },
            {
                "entityBundle": "faq_header",
                "info": "last blank header",
                "fieldCategory": "justcategoryplaceholder"
            }
        ];
        const expected = {
            group0: {
                title: '',
                items: [
                    {
                        id: 'faq_item-justcategoryplaceholder-0',
                        label: 'first no faq_item and no faq_header label',
                        contentHtml: 'first no faq_item and no faq_header contentHtml'
                    }
                ]
            },
            group1: {
                title: 'first faq_header in the list',
                items: [
                    {
                        id: 'faq_item-justcategoryplaceholder-2',
                        label: 'first faq_item after first faq_header in the list label',
                        contentHtml: 'first faq_item after first faq_header in the list contentHtml'
                    },
                    {
                        id: 'faq_item-justcategoryplaceholder-3',
                        label: 'another faq_item after first faq_header in the list label',
                        contentHtml: 'another faq_item after first faq_header in the list contentHtml'
                    }
                ]
            },
            group2: {
                title: 'last blank header',
                items: []
            },
            groupCounter: 2
        };

        const output = transformFaqCategoryDetailsToGroups(faqDetails)

        expect(output).toEqual(expected);
    });
});

