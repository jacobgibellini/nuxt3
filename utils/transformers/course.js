import pick from 'lodash/pick';
import values from 'lodash/values';
import { whenFalseyThenEmptyString } from '@/utils/helpers';
import { isArrayAndHasChild } from '@/utils/array';
import { formatDate } from '@/utils/date';
import URGENCY_LEVELS from '@/utils/enums/modules/urgency-levels';

export const transformTotalHoursMessage = (fieldWorkload) => {
    const fromAndToHours = values(pick(fieldWorkload, ['from', 'to']));

    return whenFalseyThenEmptyString(fromAndToHours.length, `${fromAndToHours.join('-')} hours`);
};

/**
 * Combines the fieldPrimaryIndustry and fieldIndustry drupal fields
 * into one array that contains the industry labels.
 *
 * @param { Object } primaryIndustry
 * @param { Array } industries
 * @returns { Array<string> }
 */
export const combineIndustries = (primaryIndustry = {}, industries = []) => {
    const combined = [];

    if (primaryIndustry?.entity?.entityLabel) {
        combined.push(primaryIndustry.entity.entityLabel);
    }

    if (isArrayAndHasChild(industries)) {
        combined.push(...industries.map((x) => x?.entity?.entityLabel).filter((x) => x));
    }

    return [...new Set(combined)];
};

/**
 * Separates the first item of the fieldActiveInstances list
 *
 * @param { Array } activeInstances
 * @returns { Object }
 */
export const transformStartDates = (activeInstances = [], dateFormat) =>
    activeInstances.map((x) => {
        const { date } = x.fieldStartDate || '';
        const { fieldUrgencyStatus, enrolLink, fieldUrgencyMessage } = x;

        return {
            date,
            formattedDate: formatDate(date, dateFormat),
            enrolLink,
            urgency: {
                status: fieldUrgencyStatus ? URGENCY_LEVELS[fieldUrgencyStatus] : null,
                message: fieldUrgencyMessage,
            },
        };
    });

/**
 * Retrieves the first date from the list of active instances
 *
 * @param { Array } activeInstances
 * @param { string } dateFormat
 * @param { Boolean } isActiveCourse
 */
export const getStartDateInfo = (activeInstances = [], dateFormat, isActiveCourse) => {
    if (isActiveCourse === false || activeInstances.length === 0) {
        return {
            formattedDate: 'Coming soon',
        };
    }

    const dates = transformStartDates(activeInstances, dateFormat);
    return dates[0] || {};
};
