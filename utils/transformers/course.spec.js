import { transformStartDates, combineIndustries, getStartDateInfo } from './course';

describe('course', () => {
    describe('#transformStartDates', () => {
        it('should transform the initial dates array into an object', () => {
            // Arrange
            const initialList = [
                {
                    fieldStartDate: {
                        date: '2020-01-20 12:00:00 UTC',
                    },
                    enrolLink: 'testlink',
                    fieldUrgencyStatus: 'urgent',
                    fieldUrgencyMessage: ['Starting soon'],
                    fieldIsActive: true,
                },
                {
                    fieldStartDate: {
                        date: '2020-02-17 12:00:00 UTC',
                    },
                    enrolLink: 'testlink2',
                    fieldUrgencyStatus: null,
                    fieldUrgencyMessage: [],
                    fieldIsActive: true,
                },
                {
                    fieldStartDate: {
                        date: '2020-05-18 12:00:00 UTC',
                    },
                    enrolLink: 'testlink3',
                    fieldUrgencyStatus: null,
                    fieldUrgencyMessage: [],
                    fieldIsActive: true,
                },
            ];
            const expectedResult = [
                {
                    date: '2020-01-20 12:00:00 UTC',
                    formattedDate: '20 Jan 2020',
                    enrolLink: 'testlink',
                    urgency: {
                        status: 'urgent',
                        message: ['Starting soon'],
                    },
                },
                {
                    date: '2020-02-17 12:00:00 UTC',
                    formattedDate: '17 Feb 2020',
                    enrolLink: 'testlink2',
                    urgency: {
                        status: null,
                        message: [],
                    },
                },
                {
                    date: '2020-05-18 12:00:00 UTC',
                    formattedDate: '18 May 2020',
                    enrolLink: 'testlink3',
                    urgency: {
                        status: null,
                        message: [],
                    },
                },
            ];

            // Act
            const transformedList = transformStartDates(initialList);

            // Assert
            expect(transformedList).toEqual(expectedResult);
        });

        it('should return an object with empty values when array is empty', () => {
            // Arrange
            const initialList = [];
            const expectedResult = [];

            // Act
            const transformedList = transformStartDates(initialList);

            // Assert
            expect(transformedList).toEqual(expectedResult);
        });
    });

    describe('#combineIndustries', () => {
        it('should combine primary industry and industries list', () => {
            // Arrange
            const primaryIndustry = { entity: { entityLabel: 'Agile Delivery' } };
            const industries = [
                { entity: { entityLabel: 'AWS' } },
                { entity: { entityLabel: 'Blockchain' } },
                { entity: { entityLabel: 'Digital Marketing' } },
            ];
            const expectedCombinedIndustries = [
                'Agile Delivery',
                'AWS',
                'Blockchain',
                'Digital Marketing',
            ];

            // Act
            const combined = combineIndustries(primaryIndustry, industries);

            // Assert
            expect(combined).toEqual(expectedCombinedIndustries);
        });

        it('should combine industries and filter out industries that are not valid', () => {
            // Arrange
            const primaryIndustry = { entity: { entityLabel: 'Blockchain' } };
            const industries = [
                { entity: { entityLabel: 'AWS' } },
                {}, // Invalid
                null, // Invalid
            ];
            const expectedCombinedIndustries = ['Blockchain', 'AWS'];

            // Act
            const combined = combineIndustries(primaryIndustry, industries);

            // Assert
            expect(combined).toEqual(expectedCombinedIndustries);
        });

        it('should return that industries list when primary industry is undefined', () => {
            // Arrange
            const primaryIndustry = undefined;
            const industries = [
                { entity: { entityLabel: 'AWS' } },
                { entity: { entityLabel: 'Blockchain' } },
                { entity: { entityLabel: 'Digital Marketing' } },
            ];
            const expectedCombinedIndustries = ['AWS', 'Blockchain', 'Digital Marketing'];

            // Act
            const combined = combineIndustries(primaryIndustry, industries);

            // Assert
            expect(combined).toEqual(expectedCombinedIndustries);
        });

        it('should return only primary industry when industries list is undefined', () => {
            // Arrange
            const primaryIndustry = { entity: { entityLabel: 'Blockchain' } };
            const industries = undefined;
            const expectedCombinedIndustries = ['Blockchain'];

            // Act
            const combined = combineIndustries(primaryIndustry, industries);

            // Assert
            expect(combined).toEqual(expectedCombinedIndustries);
        });

        it('should return an empty array when both the primary industry and industry list are undefined', () => {
            // Arrange
            const primaryIndustry = undefined;
            const industries = undefined;
            const expectedCombinedIndustries = [];

            // Act
            const combined = combineIndustries(primaryIndustry, industries);

            // Assert
            expect(combined).toEqual(expectedCombinedIndustries);
        });
    });

    describe('#getStartDateInfo', () => {
        it('should return "Coming soon" when course it not active', () => {
            // Arrange
            const activeInstances = [
                {
                    fieldStartDate: {
                        date: '2020-01-20 12:00:00 UTC',
                    },
                    enrolLink: 'testlink',
                    fieldUrgencyStatus: null,
                    fieldUrgencyMessage: [],
                },
            ];
            const expected = {
                formattedDate: 'Coming soon',
            };

            // Act
            const result = getStartDateInfo(activeInstances, 'DD MM YYYY', false);

            // Assert
            expect(result).toEqual(expected);
        });

        it('should return "Coming soon" when the formatted course list is empty', () => {
            // Arrange
            const activeInstances = [];
            const expected = {
                formattedDate: 'Coming soon',
            };

            // Act
            const result = getStartDateInfo(activeInstances, 'DD MM YYYY');

            // Assert
            expect(result).toEqual(expected);
        });

        it('should return the first course from a list of valid dates', () => {
            // Arrange
            const activeInstances = [
                {
                    fieldStartDate: {
                        date: '2020-01-20 12:00:00 UTC',
                    },
                    enrolLink: 'firstCourseLink',
                    fieldUrgencyStatus: 'urgent',
                    fieldUrgencyMessage: ['Starting soon'],
                    fieldIsActive: true,
                },
                {
                    fieldStartDate: {
                        date: '2020-02-17 12:00:00 UTC',
                    },
                    enrolLink: 'secondCourseLink',
                    fieldUrgencyStatus: null,
                    fieldUrgencyMessage: [],
                    fieldIsActive: true,
                },
            ];
            const expected = {
                date: '2020-01-20 12:00:00 UTC',
                formattedDate: '20-01-2020',
                enrolLink: 'firstCourseLink',
                urgency: {
                    message: ['Starting soon'],
                    status: 'urgent',
                },
            };

            // Act
            const result = getStartDateInfo(activeInstances, 'DD-MM-YYYY');

            // Assert
            expect(result).toEqual(expected);
        });
    });
});
