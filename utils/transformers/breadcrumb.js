import isNil from 'lodash/isNil';

export const validateBreadcrumbHasLabelAndUrl = context => !isNil(context?.entityLabel) && !isNil(context?.entityUrl?.path);

export const transformBreadcrumbTrail = (context = {}) => ({
    label: context.entityLabel || context.text || null,
    href: context.entityUrl?.path || context.url?.path || null
});

export const transformParentsToBreadcrumbTrails = rawTrails =>
    rawTrails.reduce?.((processedBreadcrumbTrails, rawTrail) => {
        const { entity } = rawTrail;

        const nextBreadcrumbTrail = entity && validateBreadcrumbHasLabelAndUrl(entity)
            ? [ transformBreadcrumbTrail(entity) ]
            : [];

        return [
            ...processedBreadcrumbTrails,
            ...nextBreadcrumbTrail
        ];
    }, []);