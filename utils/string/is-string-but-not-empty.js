import isString from 'lodash/isString';

const isStringButNotEmpty = (value) => isString(value) && value.length > 0;

export default isStringButNotEmpty;
