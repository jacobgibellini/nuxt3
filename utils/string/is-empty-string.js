import isString from 'lodash/isString';

const isEmptyString = value => isString(value) && value.length === 0;

export default isEmptyString;