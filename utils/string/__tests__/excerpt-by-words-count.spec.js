import { excerptByWordsCount } from '../excerpt-by-words-count';

describe('#excerptByWordsCount', () => {
    const contentHtml =
        'Once upon a time there were three bears, Papa Bear, Mamma Bear and Baby Bear.';

    it('should return excerpted and rest value when cut off words less than total words from contentHtml', () => {
        const expected = 'Once upon a time there';

        const received = excerptByWordsCount(5, contentHtml);

        expect(received).toEqual(expected);
    });

    it('should return excerpted and rest value when cut off words same total words from contentHtml', () => {
        const expected =
            'Once upon a time there were three bears, Papa Bear, Mamma Bear and Baby Bear.';

        const received = excerptByWordsCount(15, contentHtml);

        expect(received).toEqual(expected);
    });

    it('should return excerpted and rest value when cut off words more than total words from contentHtml', () => {
        const expected =
            'Once upon a time there were three bears, Papa Bear, Mamma Bear and Baby Bear.';

        const received = excerptByWordsCount(19, contentHtml);

        expect(received).toEqual(expected);
    });

    it('should treat HTML open tag as one word', () => {
        const expected = '<p>Once upon a time there were three <a href="#">bears</a>,';
        const _contentHtml =
            '<p>Once upon a time there were three <a href="#">bears</a>, Papa Bear, <a href="#">Mamma Bear</a> and Baby Bear.</p>';

        const received = excerptByWordsCount(8, _contentHtml);

        expect(received).toEqual(expected);
    });

    it('should return string with 50 words', () => {
        const expected =
            '<p>RMIT Online has built our courses in partnership with industry, providing you with the latest industry insights and best practices from the world of work. Our subject matter experts on this course include people like Martin Carlill, who is a digital marketing specialist with over 20 years experience in inbound';
        const _contentHtml =
            '<p>RMIT Online has built our courses in partnership with industry, providing you with the latest industry insights and best practices from the world of work. Our subject matter experts on this course include people like Martin Carlill, who is a digital marketing specialist with over 20 years experience in inbound marketing<br />\nRMIT Online has built our courses in partnership with industry, providing you with the latest industry insights and best practices from the world of work. Our subject matter experts on this course include people like Martin Carlill, who is a digital marketing specialist with over 20 years experience in inbound marketing<br />\nRMIT Online has built our courses in partnership with industry, providing you with the latest industry insights and best practices from the world of work. Our subject matter experts on this course include people like Martin Carlill, who is a digital marketing specialist with over 20 years experience in inbound marketing</p>\n';

        const received = excerptByWordsCount(50, _contentHtml);

        expect(received).toEqual(expected);
    });
});
