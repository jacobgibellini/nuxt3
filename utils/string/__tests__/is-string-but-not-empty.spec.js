import isStringButNotEmpty from '../is-string-but-not-empty';

describe('fn: isStringButNotEmpty', () => {
    it.each`
        value            | expected
        ${null}          | ${false}
        ${undefined}     | ${false}
        ${0}             | ${false}
        ${1}             | ${false}
        ${''}            | ${false}
        ${' '}           | ${true}
        ${'hello there'} | ${true}
    `('should return $expected when value is $value', ({ value, expected }) => {
        const result = isStringButNotEmpty(value);

        expect(result).toBe(expected);
    });
});
