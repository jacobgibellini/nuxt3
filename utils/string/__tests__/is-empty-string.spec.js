import isEmptyString from '../is-empty-string';

describe('fn: isEmptyString', () => {
    it('should return true when string is empty', () => {
        expect(isEmptyString('')).toBe(true);
    });

    it('should return false when string is not empty', () => {
        expect(isEmptyString('text goes here')).toBe(false);
    });
});
