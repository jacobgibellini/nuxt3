export const excerptByWordsCount = (
    wordsCount,
    contentHtml
) => {
    const wordsWithHtml = contentHtml.split(' ');
    const stitched      = [];

    for (let i=0; i < wordsWithHtml.length; i++) {
        const currWord              = wordsWithHtml[i];
        const nextWord              = wordsWithHtml[i + 1];
        const currOpenBracketIndex  = currWord.indexOf('<');
        const currCloseBracketIndex = currWord.indexOf('>');

        if (currOpenBracketIndex > -1 && currCloseBracketIndex < 0 && nextWord) {
            stitched.push(currWord + ' ' + nextWord);
            continue;
        }

        if (currCloseBracketIndex > -1) {
            const stringBeforeCloseBracket = currWord.slice(0, currCloseBracketIndex + 1)

            if (stringBeforeCloseBracket.indexOf('<') < 0) {
                continue;
            }
        }

        if (stitched.length < wordsCount) {
            stitched.push(currWord);
        }

        if (stitched.length >= wordsCount) {
            break;
        }
    }

    return stitched
        .join(' ');
}