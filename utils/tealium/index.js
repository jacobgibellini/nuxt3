// Run the tealium script after the utag_data has been set.
// This is to avoid the possibility of the tealium script executing and finishing before the data is available.
export const runTealiumScript = () => {
    require(`@/static/js/tealium/${ process.env.tealiumEnv }.js`);
}

// Assigns incoming data from drupal to the utag window object
export const setUtagData = (utag_data) => {
    window.utag_data = utag_data || {}
}

const setTealium = (utag_data) => {
    if (process.client) {
        setUtagData(utag_data)
        runTealiumScript()
    }
}

export default setTealium