import {
    fireTealiumFormEvent,
    getTealiumClidForLead
} from './lead-form';

// Test Helper(s):
import { getExpiryDaysOffset } from '~/jest/helpers/tealium';

describe('#fireTealiumFormEvent', () => {
    const mockFormValues = {
        email: 'tester@rmitonline.edu.au',
        phone: '+61 433 003 361',
        first_name: 'test local',
        last_name: 'stg design thinking i',
        citizen: 'No',
        state: 'VIC',
        startStudying: 'Immediately',
    };

    const EXPECTED_COMMON_USER_DETAILS = {
        user_email: 'tester@rmitonline.edu.au',
        user_email_hashed: 'tester@rmitonline.edu.au',
        user_mobile: '+61 433 003 361',
        user_mobile_hashed: '+61 433 003 361',
        user_name_first: 'test local',
        user_name_last: 'stg design thinking i',
        user_residency_status: 'No',
        user_state: 'VIC',
        user_study_start: 'Immediately'
    };

    beforeAll(() => {
        global.utag = {
            link: jest.fn()
        };
    });

    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('should utag.link not called when "utag_data" is not available', () => {
        delete global.window.utag_data

        fireTealiumFormEvent();

        expect(utag.link).not.toHaveBeenCalled();
    });

    describe('Given "utag_data" and "utag" is available globally', () => {
        beforeEach(() => {
            global.window.utag_data = {};
        });

        it('should utag.link call with expected without "formValues" payload and "tealium_event" is not "course_detail_view"', () => {
            const expected = {
                link_action: 'Button',
                link_category: 'User interaction',
                link_label: 'Register interest',
                course_category: window.utag_data.course_category,
                page_name: window.utag_data.page_name,
                site_section: window.utag_data.site_section,
                tealium_event: 'register_interest_success',
                user_email: undefined,
                user_email_hashed: undefined,
                user_mobile: undefined,
                user_mobile_hashed: undefined,
                user_name_first: undefined,
                user_name_last: undefined,
                user_residency_status: undefined,
                user_state: undefined,
                user_study_start: undefined
            };

            fireTealiumFormEvent();

            expect(utag.link).toHaveBeenCalledWith(expected);
        });

        it('should utag.link call with expected without "formValues" payload and "tealium_event" is "course_detail_view"', () => {
            global.window.utag_data = {
                course_category: undefined,
                page_name: 'Topic page',
                site_section: 'topic',
                tealium_event: 'course_detail_view'
            };
            const expected = {
                link_action: 'Button',
                link_category: 'User interaction',
                link_label: 'Request a course guide',
                course_category: window.utag_data.course_category,
                page_name: window.utag_data.page_name,
                site_section: window.utag_data.site_section,
                tealium_event: 'course_guide_request_success',
                user_email: undefined,
                user_email_hashed: undefined,
                user_mobile: undefined,
                user_mobile_hashed: undefined,
                user_name_first: undefined,
                user_name_last: undefined,
                user_residency_status: undefined,
                user_state: undefined,
                user_study_start: undefined,
            };

            fireTealiumFormEvent();

            expect(utag.link).toHaveBeenCalledWith(expected);
        });

        it('should utag.link call with expected when tealium_event value is not "course_detail_view"', () => {
            global.window.utag_data = {
                course_category: undefined,
                page_name: 'Topic page',
                site_section: 'topic',
                tealium_event: 'register_interest_success'
            };
            const expected = {
                ...EXPECTED_COMMON_USER_DETAILS,
                link_action: 'Button',
                link_category: 'User interaction',
                link_label: 'Register interest',
                course_category: undefined,
                page_name: 'Topic page',
                site_section: 'topic',
                tealium_event: 'register_interest_success',
                user_residency_status: 'Yes'
            };
            const payload = {
                ...mockFormValues,
                last_name: 'stg design thinking i',
                citizen: 'Yes'
            };

            fireTealiumFormEvent(payload);

            expect(utag.link).toHaveBeenCalledWith(expected);
        });

        it('should utag.link call with expected when tealium_event value is "course_detail_view"', () => {
            global.window.utag_data = {
                course_category: ['Design Thinking'],
                page_name: 'Course detail page',
                site_section: 'courses',
                course_id: ['DTR101'],
                course_title: ['Design Thinking for Innovation'],
                course_type: ['short'],
                tealium_event: 'course_detail_view'
            };
            const expected = {
                ...EXPECTED_COMMON_USER_DETAILS,
                course_category: ['Design Thinking'],
                course_id: ['DTR101'],
                course_title: ['Design Thinking for Innovation'],
                course_type: ['short'],
                link_action: 'Button',
                link_category: 'User interaction',
                link_label: 'Request a course guide',
                page_name: 'Course detail page',
                site_section: 'courses',
                tealium_event: 'course_guide_request_success'
            };

            fireTealiumFormEvent(mockFormValues);

            expect(utag.link).toHaveBeenCalledWith(expected);
        });
    });
});

describe('#getTealiumClidForLead', () => {
    afterEach(() => {
        localStorage.clear();
    });

    it('should return clid value if not expired', () => {
        const mockValidClid = JSON.stringify({
            expiryDate: getExpiryDaysOffset(1),
            name: 'gclid',
            value: 'mocked-gclid-value'
        });
        localStorage.setItem('clid', mockValidClid);

        const received = getTealiumClidForLead();

        expect(received).toEqual('mocked-gclid-value');
    });

    it('should return undefined if expired equal or more than a day passed', () => {
        const mockValidClid = JSON.stringify({
            expiryDate: getExpiryDaysOffset(-1),
            name: 'fbclid',
            value: 'mocked-fbclid-value'
        });
        localStorage.setItem('clid', mockValidClid);

        const received = getTealiumClidForLead();

        expect(received).toBeUndefined();
    });

    it('should return undefined if no expiryDate even there is a value', () => {
        const mockValidClid = JSON.stringify({
            value: 'mocked-fbclid-value'
        });
        localStorage.setItem('clid', mockValidClid);

        const received = getTealiumClidForLead();

        expect(received).toBeUndefined();
    });

    it('should return value\'s length maximum 255 when its not expired', () => {
        const expected = 'IwAR05Oyi7uJOeb72doabGgt7TwZMpjgvU3Bd0tMDbgnoQjVXSedYmybfuHI8+aem_AS0m25b3BUfEQOoHreNd7gQqypDfTjTuCs1VaCi6H_QmOjzHDLKKCMzE-Hr_8DslhXEzBUPcL5MZG2pRLtAxQ5LdEgMpdMVhrpcs-nGm7yYJxgIwAR05Oyi7uJOeb72doabGgt7TwZMpjgvU3Bd0tMDbgnoQjVXSedYmybfuHI8+aem_AS0m25b3BUfEQ'
        const mockValidClid = JSON.stringify({
            expiryDate: getExpiryDaysOffset(1),
            name: 'fbclid',
            value: 'IwAR05Oyi7uJOeb72doabGgt7TwZMpjgvU3Bd0tMDbgnoQjVXSedYmybfuHI8+aem_AS0m25b3BUfEQOoHreNd7gQqypDfTjTuCs1VaCi6H_QmOjzHDLKKCMzE-Hr_8DslhXEzBUPcL5MZG2pRLtAxQ5LdEgMpdMVhrpcs-nGm7yYJxgIwAR05Oyi7uJOeb72doabGgt7TwZMpjgvU3Bd0tMDbgnoQjVXSedYmybfuHI8+aem_AS0m25b3BUfEQ256'
        });
        localStorage.setItem('clid', mockValidClid);

        const received = getTealiumClidForLead();

        expect(received).toBe(expected);
        expect(received).toHaveLength(255);
    });

    it.each`
    value              | expected
    ${false}           | ${undefined}
    ${0}               | ${undefined}
    ${'1'}             | ${'1'}
    `('should return $expected when value is $value and its not expired', ({
        value,
        expected
    }) => {
        const mockValidClid = JSON.stringify({
            expiryDate: getExpiryDaysOffset(1),
            name: 'fbclid',
            value
        });
        localStorage.setItem('clid', mockValidClid);

        const received = getTealiumClidForLead();

        expect(received).toBe(expected);
    });

    it.each`
    value
    ${false}
    ${0}
    ${'1'}
    `('should return $value and its expired', ({
        value
    }) => {
        const mockValidClid = JSON.stringify({
            expiryDate: getExpiryDaysOffset(-1),
            name: 'fbclid',
            value
        });
        localStorage.setItem('clid', mockValidClid);

        const received = getTealiumClidForLead();

        expect(received).toBeUndefined();
    });
});