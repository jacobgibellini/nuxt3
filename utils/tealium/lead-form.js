// Note: Copied from Legacy Form.vue
export const fireTealiumFormEvent = formValues => {
    // Escape hatch for form that doesn't have tealium integrated while to avoid preventing user submit a form
    if (!window.utag_data || !window.utag) return
    const { email, phone, first_name, last_name, citizen, state, startStudying } = formValues || {}
    // Base event is for categories
    // Hashed values are to be left blank, tealium will handle this.
    const utagLinkData = {
        "link_action"             : "Button",
        "link_category"           : "User interaction",
        "link_label"              : "Register interest",
        "course_category"         : window.utag_data.course_category,
        "page_name"               : window.utag_data.page_name,
        "site_section"            : window.utag_data.site_section,
        "tealium_event"           : "register_interest_success",
        "user_email"              : email,
        "user_email_hashed"       : email,
        "user_mobile"             : phone,
        "user_mobile_hashed"      : phone,
        "user_name_first"         : first_name,
        "user_name_last"          : last_name,
        "user_residency_status"   : citizen,
        "user_state"              : state,
        "user_study_start"        : startStudying,
    }

    // When the page is a course details page
    if (window.utag_data.tealium_event === 'course_detail_view') {
        utagLinkData.course_id     = window.utag_data.course_id
        utagLinkData.course_title  = window.utag_data.course_title
        utagLinkData.course_type   = window.utag_data.course_type
        utagLinkData.tealium_event = "course_guide_request_success"
        utagLinkData.link_label    = "Request a course guide"
    }

    utag.link(utagLinkData)
}

export const getTealiumClidForLead = () => {
    const clid               = JSON.parse(localStorage.getItem('clid'));
    const hasValidExpiryDate = !!clid?.expiryDate;
    const hasExpired         = new Date().getTime() >= +clid?.expiryDate;

    if (clid !== null && hasValidExpiryDate && !hasExpired) {
        return clid?.value?.slice?.(0, 255);
    }
}
