import { setUtagData } from './index.js';

describe('#setUtagData', () => {
    beforeEach(() => {
        window.utag_data = undefined
    })

    it('should populate the utag data', () => {
        // Arrange
        const utagDataToBePassed = {
            page_home: 'Page',
            tealium_event: 'event'
        }

        // Act
        setUtagData(utagDataToBePassed)

        // Assert
        expect(window.utag_data).toEqual(utagDataToBePassed)
    });

    it('should set the utag_data window object to an empty object', () => {
        // Arrange
        const utagDataToBePassed = null
        const expectedUtagValue = {}

        // Act
        setUtagData(utagDataToBePassed)

        // Assert
        expect(window.utag_data).toEqual(expectedUtagValue)
    });
});