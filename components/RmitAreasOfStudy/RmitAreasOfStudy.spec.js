import { shallowMount } from '@vue/test-utils';
import RmitAreasOfStudy from './RmitAreasOfStudy';

const DEFAULT_AREAS = [
    {
        label: 'Blockchain',
        url: '/business-finance/blockchain',
        description:
            '<p>When Forbes say blockchain is where the internet was 20 years ago, you sit up and take notice.</p>',
    },
    {
        label: 'Finance & Accounting',
        url: '/business-finance/finance-accounting',
        description:
            '<p>Develop a deep knowledge of integral business concepts and principles across key business units. </p>',
    },
];

describe('Component: <rmit-areas-of-study>', () => {
    it('should match snapshot with areas', () => {
        const wrapper = shallowMount(RmitAreasOfStudy, {
            propsData: {
                title: 'Ferrari',
                areas: DEFAULT_AREAS,
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });

    it('should match snapshot without areas', () => {
        const wrapper = shallowMount(RmitAreasOfStudy, {
            propsData: {
                title: 'Lamborghini',
                areas: [],
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });

    it('should footer slot component render correctly', () => {
        const wrapper = shallowMount(RmitAreasOfStudy, {
            slots: {
                footer: '<button class="racing-mode"></button>',
            },
        });

        expect(wrapper.find('button.racing-mode').exists()).toBe(true);
    });
});
