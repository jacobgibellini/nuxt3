import RmitoPageSection from '@/components/RmitoPageSection';
import RmitoTestimonial from './RmitoTestimonial.vue';
import RmitoTestimonials from './RmitoTestimonials.vue';
import { shallowMount } from '@vue/test-utils';

describe('Component: <rmito-testimonials ... />', () => {
    const MOCK_REVIEW_ITEMS = [
        {
            id: 'testimonial-item-111',
            image: '//assets.rmit.edu.au/thumbnail/public/Jimmy.png',
            name: 'J Mon',
            quote: '<p>When i first met Jenny</p>',
            title: 'Tester 1',
        },
        {
            id: 'testimonial-item-222',
            image: '//assets.rmit.edu.au/thumbnail/public/Jenny.png',
            name: 'J Tran',
            quote: '<p>When i first met Jimmy</p>',
            title: 'Tester 2',
        },
    ];
    let wrapper;

    beforeAll(() => {
        wrapper = shallowMount(RmitoTestimonials, {
            propsData: {
                sectionTitle: 'What our clients say',
                sectionDescriptionHtml: '<p>Testing section description HTML</p>',
                reviewItems: MOCK_REVIEW_ITEMS,
            },
        });
    });

    it('should "RmitoPageSection" component mounted with "sectionTitle"', () => {
        expect(wrapper.findComponent(RmitoPageSection).props('sectionTitle')).toBe(
            'What our clients say'
        );
    });

    it('should "RmitoPageSection" component mounted with "sectionTitle"', () => {
        expect(wrapper.findComponent(RmitoPageSection).props('sectionDescriptionHtml')).toBe(
            '<p>Testing section description HTML</p>'
        );
    });

    it('should all "reviewItems" items rendered', () => {
        expect(wrapper.findAllComponents(RmitoTestimonial)).toHaveLength(2);
    });

    it('should both "RmitoTestimonial" components mounted with "reviewItems" data correctly', () => {
        const rmitTestimonialComponents = wrapper.findAllComponents(RmitoTestimonial);

        expect(rmitTestimonialComponents.at(0).attributes()).toEqual(MOCK_REVIEW_ITEMS[0]);
        expect(rmitTestimonialComponents.at(1).attributes()).toEqual(MOCK_REVIEW_ITEMS[1]);
    });

    it('should render nothing if "reviewItems" is an empty array', () => {
        const wrapper = shallowMount(RmitoTestimonials, {
            propsData: {
                sectionTitle: 'What our clients say',
                sectionDescriptionHtml: '<p>Testing section description HTML</p>',
                reviewItems: [],
            },
        });

        expect(wrapper.html()).toBe('');
    });

    it('should render maximum of 2 testimonials only', () => {
        const wrapper = shallowMount(RmitoTestimonials, {
            propsData: {
                sectionTitle: 'What our clients say',
                sectionDescriptionHtml: '<p>Testing section description HTML</p>',
                reviewItems: [
                    ...MOCK_REVIEW_ITEMS,
                    {
                        id: 'here-comes-the-new-challenger',
                        image: '//assets.rmit.edu.au/thumbnail/public/dont-show-my-face.png',
                        name: 'Ryu',
                        quote: '<p>Hadouken!</p>',
                        title: 'Street Fighter',
                    },
                ],
            },
        });

        const rmitTestimonialComponents = wrapper.findAllComponents(RmitoTestimonial);

        expect(rmitTestimonialComponents).toHaveLength(2);
    });
});
