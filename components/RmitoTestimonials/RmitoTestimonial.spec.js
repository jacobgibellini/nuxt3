import RmitoTestimonial from './RmitoTestimonial';
import { shallowMount } from '@vue/test-utils';

describe('Component: <rmito-testimonial />', () => {
    it('should render correctly with data', () => {
        const wrapper = shallowMount(RmitoTestimonial, {
            propsData: {
                image: 'localhost:3000/image',
                name: 'Ezio Auditore',
                title: 'Italian master assassin',
                quote: 'We work in the Dark to serve the Light. We are assassins.',
            },
            stubs: ['rmito-flex'],
        });

        expect(wrapper.element).toMatchSnapshot();
    });

    it('should render correctly without data', () => {
        const wrapper = shallowMount(RmitoTestimonial, {
            stubs: ['rmito-flex'],
        });

        expect(wrapper.element).toMatchSnapshot();
    });

    it('should root element contain "id" attribute', () => {
        const wrapper = shallowMount(RmitoTestimonial, {
            propsData: {
                id: 'lala',
                image: 'miaw.jpg',
                name: 'Jonny Miaw',
                title: 'Tequila Shot Master',
                quote: 'I can drink bottles of tequila and will never get drunk, dare me! BLURGHHHHH',
            },
            stubs: ['rmito-flex'],
        });

        expect(wrapper.attributes('id')).toBe('lala');
    });
});
