import RmitoPageSection from './RmitoPageSection';
import RmitoHeadline from '~/components/RmitoHeadline';
import { mount, shallowMount } from '@vue/test-utils';

const findDescriptionElement = (wrapper) =>
    wrapper.element.querySelector('.rmit-page-section__description');

describe('Component: <rmito-page-section ...>...</rmito-page-section>', () => {
    it('should headline and description not rendered when props - "sectionDescriptionHtml" and "sectionTitle" not provided', () => {
        const wrapper = mount(RmitoPageSection);

        const descriptionElement = findDescriptionElement(wrapper);

        expect(descriptionElement).toBeNull();
        expect(wrapper.findComponent(RmitoHeadline).exists()).toBe(false);
    });

    it('should render description when "sectionDescriptionHtml" is provided', () => {
        const wrapper = shallowMount(RmitoPageSection, {
            propsData: {
                sectionDescriptionHtml:
                    'This is a <strong>section</strong> description that render with HTML String',
            },
        });

        const descriptionElement = findDescriptionElement(wrapper);

        expect(descriptionElement.textContent).toBe(
            'This is a section description that render with HTML String'
        );
    });

    it('should render description when "sectionDescriptionHtml" is provided', () => {
        const wrapper = shallowMount(RmitoPageSection, {
            propsData: {
                sectionDescriptionHtml:
                    'This is a <strong>section</strong> description that render with HTML String',
            },
        });

        const descriptionElement = findDescriptionElement(wrapper);

        expect(descriptionElement.textContent).toBe(
            'This is a section description that render with HTML String'
        );
    });

    it('should "sectionTitle" prop value render with a <h2>', () => {
        const wrapper = mount(RmitoPageSection, {
            propsData: {
                sectionTitle: 'Nothing is true; everything is permitted',
            },
        });

        const headlineComponent = wrapper.findComponent(RmitoHeadline);

        expect(headlineComponent.text()).toBe('Nothing is true; everything is permitted');
        expect(headlineComponent.element.tagName).toBe('H2');
    });

    it('should default slot render content as expected', () => {
        const wrapper = mount(RmitoPageSection, {
            slots: {
                default: 'Click <a href="#paradise">here</a> for more details',
            },
        });

        expect(wrapper.element).toMatchSnapshot();
    });
});
