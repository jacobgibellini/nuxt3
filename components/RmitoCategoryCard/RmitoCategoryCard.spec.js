import { shallowMount } from '@vue/test-utils';
import RmitoCategoryCard from './RmitoCategoryCard';
import ChevronBlueSvg from '~/static/img/svg/chevron-blue.svg';

describe('Component: <rmito-category-card>', () => {
    let wrapper;

    beforeEach(async () => {
        wrapper = shallowMount(RmitoCategoryCard, {
            propsData: {
                title: 'Tile title',
                description: 'This is a short description',
                href: 'http://localhost/page',
            },
        });

        await wrapper.vm.$nextTick();
    });

    it('should match default snapshop', () => {
        expect(wrapper.element).toMatchSnapshot();
    });

    it('should have an href', () => {
        // Arrange
        const expectedUrl = 'http://localhost/page';

        // Act
        const anchorElement = wrapper.find('a');
        const href = anchorElement.attributes().href;

        // Assert
        expect(href).toBe(expectedUrl);
    });

    it('should have a ribbon with a solid colour when no colour is provided', async () => {
        // Arrange
        const expectedComputedStyle = '#030054';

        // Act
        const computedStyle = wrapper.vm.backgroundStyle.background;

        // Assert
        expect(computedStyle).toEqual(expectedComputedStyle);
    });

    it('should have a ribbon with a gradient when colour is provided', async () => {
        // Arrange
        const expectedComputedStyle = 'linear-gradient(#030054, #4287f5)';
        wrapper = shallowMount(RmitoCategoryCard, {
            propsData: {
                title: 'Tile with gradient',
                description: 'This is a tile with a gradient',
                href: 'http://localhost/page2',
                color: '#4287f5',
            },
        });

        // Act
        await wrapper.vm.$nextTick();
        const computedStyle = wrapper.vm.backgroundStyle.background;

        // Assert
        expect(computedStyle).toEqual(expectedComputedStyle);
    });

    it('should root element render as <div> and chevron icon not rendered when no "href"', () => {
        // Arrange
        const wrapper = shallowMount(RmitoCategoryCard, {
            propsData: {
                title: 'Micro Credential Card',
            },
        });

        // Assert
        expect(wrapper.attributes('href')).toBe(undefined);
        expect(wrapper.element.tagName).toBe('DIV');
        expect(wrapper.findComponent(ChevronBlueSvg).exists()).toBe(false);
    });

    it('should root element render as <a> when "href" value is valid', () => {
        // Arrange
        const wrapper = shallowMount(RmitoCategoryCard, {
            propsData: {
                title: 'Short Course Card',
                href: '/test-course-path',
            },
        });

        // Assert
        expect(wrapper.attributes('href')).toBe('/test-course-path');
        expect(wrapper.element.tagName).toBe('A');
        expect(wrapper.findComponent(ChevronBlueSvg).exists()).toBe(true);
    });
});
