import { shallowMount } from '@vue/test-utils';
import RmitoImage from './RmitoImage';
import '@/jest/__mocks__/image.mock';

const mountComponent = (props) => {
    return shallowMount(RmitoImage, {
        propsData: {
            src: 'http://placehold.it/120x120&text=image1',
            alt: 'Alt text',
            ...props,
        },
    });
};

describe('Component: <rmito-image>', () => {
    it('should match default snapshot', () => {
        // Arrange
        const src = 'http://placehold.it/120x120&text=image2';
        const expectedUrl = 'url("http://placehold.it/120x120&text=image2")';

        // Act
        const wrapper = mountComponent({ src });

        // Assert
        expect(wrapper.vm.styles.backgroundImage).toBe(expectedUrl);
        expect(wrapper.element).toMatchSnapshot();
    });

    it('should set the background position', () => {
        // Arrange
        const position = '40% 60%';
        const expected = '40% 60%';

        // Act
        const wrapper = mountComponent({ position });

        // Assert
        expect(wrapper.vm.styles.backgroundPosition).toBe(expected);
    });

    it('should set the background size', () => {
        // Arrange
        const size = 'auto';
        const expected = 'auto';

        // Act
        const wrapper = mountComponent({ size });

        // Assert
        expect(wrapper.vm.styles.backgroundSize).toBe(expected);
    });

    it('should NOT set the max width and height', () => {
        // Arrange
        const expectedMaxHeight = undefined;
        const expectedMaxWidth = undefined;

        // Act
        const wrapper = mountComponent({});

        // Assert
        expect(wrapper.vm.styles.maxHeight).toBe(expectedMaxHeight);
        expect(wrapper.vm.styles.maxWidth).toBe(expectedMaxWidth);
    });

    it('should set the max height', () => {
        // Arrange
        const maxHeight = 100;
        const expected = '100px';

        // Act
        const wrapper = mountComponent({ maxHeight });

        // Assert
        expect(wrapper.vm.styles.maxHeight).toBe(expected);
    });

    it('should set the max width', () => {
        // Arrange
        const maxWidth = 2;
        const expected = '2px';

        // Act
        const wrapper = mountComponent({ maxWidth });

        // Assert
        expect(wrapper.vm.styles.maxWidth).toBe(expected);
    });

    it('should render an img element', async () => {
        // Arrange
        const imgTag = true;
        const expectedTagName = 'IMG';

        // Act
        const wrapper = mountComponent({ imgTag });
        await wrapper.vm.$nextTick();

        // Assert
        expect(wrapper.element.tagName).toBe(expectedTagName);
        expect(wrapper.html()).toMatchSnapshot();
    });
});
