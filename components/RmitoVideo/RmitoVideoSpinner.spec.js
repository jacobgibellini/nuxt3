import RmitoVideoSpinner from './RmitoVideoSpinner';
import { mount } from '@vue/test-utils';

describe('Component: <rmito-video-spinner />', () => {
    it('should render correctly', () => {
        const wrapper = mount(RmitoVideoSpinner);

        expect(wrapper.element).toMatchSnapshot();
    });
});
