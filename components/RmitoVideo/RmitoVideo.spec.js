import RmitoVideo from './RmitoVideo';
import VueYoutube from 'vue-youtube';
import VueLazyload from 'vue-lazyload';
import { VUE_LAZY_LOAD_CONFIGS } from '~/plugins/vue-plugins';
import { mount, createLocalVue } from '@vue/test-utils';

const localVue = createLocalVue();

localVue.use(VueLazyload, VUE_LAZY_LOAD_CONFIGS);
localVue.use(VueYoutube);

describe('Component: <rmito-video />', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(RmitoVideo, {
            localVue,
            propsData: {
                title: "How to predict Melbourne's weather",
                url: 'https://www.youtube.com/watch?v=FDZPdeND5Kw',
            },
            stubs: ['no-ssr', 'youtube'],
        });
    });

    it('should render correctly', () => {
        expect(wrapper.element).toMatchSnapshot();
    });

    it('should render the video component when clicked', async () => {
        // Arrange
        const playButtonElement = wrapper.find('.rmit-video__play-button');

        // Act
        playButtonElement.trigger('click');
        await wrapper.vm.$nextTick();

        // Assert
        expect(wrapper.vm.showYoutubeComponent).toBe(true);
        expect(wrapper.element).toMatchSnapshot();
    });

    it('should render image overlay when youtube video is not playing', () => {
        // Arrange
        const expectedResult = true;

        // Act
        const overlayElement = wrapper.find('.rmit-video__overlay');

        // Assert
        expect(overlayElement.exists()).toBe(expectedResult);
    });

    it('should not render image overlay when youtube video is playing', async () => {
        // Arrange
        const expectedResult = false;

        // Act
        wrapper.vm.videoPlaying = true;
        await wrapper.vm.$nextTick();
        const overlayElement = wrapper.find('.rmit-video__overlay');

        // Assert
        expect(overlayElement.exists()).toBe(expectedResult);
    });
});
