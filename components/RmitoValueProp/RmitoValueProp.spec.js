import RmitoValueProp from './RmitoValueProp';
import { mount } from '@vue/test-utils';

describe('Component: <rmito-value-prop />', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(RmitoValueProp, {
            propsData: {
                title: 'Title text',
                body: 'Body text',
            },
            slots: {
                default: '<div class="slot">This is slot content</div>',
            },
        });
    });

    it('should render correctly', () => {
        expect(wrapper.element).toMatchSnapshot();
    });

    it('should render slot', () => {
        // Act
        const slot = wrapper.find('.slot');

        // Assert
        expect(slot.exists()).toBe(true);
        expect(slot.element).toMatchSnapshot();
    });

    it('should render title text', () => {
        // Arrange
        const expectedText = 'Title text';

        // Act
        const titleElement = wrapper.find('.rmito-value-prop__title');

        // Assert
        expect(titleElement.text()).toBe(expectedText);
    });

    it('should render body text', () => {
        // Arrange
        const expectedText = 'Body text';

        // Act
        const bodyElement = wrapper.find('.rmito-value-prop__body');

        // Assert
        expect(bodyElement.text()).toBe(expectedText);
    });
});
