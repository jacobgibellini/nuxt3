import RmitoValueProp from './RmitoValueProp';
import SvgCertificate from '@/assets/img/svg/Certificate.svg';
import SvgIndustry from '@/assets/img/svg/Industry.svg';
import SvgRealWorld from '@/assets/img/svg/RealWorld.svg';
import SvgSupport from '@/assets/img/svg/Support.svg';
import { withDesign } from 'storybook-addon-designs';

const VALUE_PROP_ICON_OPTIONS = {
    Certificate: 'SvgCertificate',
    Industry: 'SvgIndustry',
    'Real World': 'SvgRealWorld',
    Support: 'SvgSupport',
};

export default {
    title: 'atoms/ValueProp',
    component: RmitoValueProp,
    decorators: [withDesign],
    argTypes: {
        icons: {
            description: 'List of icons used for Value Props',
            options: VALUE_PROP_ICON_OPTIONS,
            control: { type: 'select' },
        },
    },
};

const Template = (args, { argTypes }) => ({
    components: {
        RmitoValueProp,
        SvgCertificate,
        SvgIndustry,
        SvgRealWorld,
        SvgSupport,
    },
    props: Object.keys(argTypes),
    template: `
        <div style="max-width: 380px;">
            <rmito-value-prop v-bind="$props">
                <component :is="icons"></component>
            </rmito-value-prop>
        </div>
    `,
});

export const Default = Template.bind({});
Default.parameters = {
    design: {
        type: 'figma',
        url: 'https://www.figma.com/file/EExD5CjjoAszVP0vharvxr/GEL-UI-Pattern-Library?node-id=1095%3A87',
    },
};
Default.args = {
    title: 'Real world skills',
    body: 'Develop skills that have been validated by industry, while getting credentialed by a world-leading university.',
    // Below are for Storybook only:
    icons: VALUE_PROP_ICON_OPTIONS.Certificate,
};
