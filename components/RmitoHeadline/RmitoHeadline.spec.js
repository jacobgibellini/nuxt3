import RmitoHeadline from './RmitoHeadline';
import { shallowMount } from '@vue/test-utils';

describe('Component: <rmito-headline ...>...</rmito-headline>', () => {
    it('should default render as h1 with text', () => {
        const wrapper = shallowMount(RmitoHeadline, {
            slots: {
                default: 'Checking default heading',
            },
        });

        expect(wrapper.element.tagName).toBe('H1');
        expect(wrapper.text()).toBe('Checking default heading');
        expect(wrapper.classes('rmit-headline--1')).toBe(true);
    });

    it.each`
        level | tag          | expectedClass         | expectedTag
        ${1}  | ${undefined} | ${'rmit-headline--1'} | ${'H1'}
        ${2}  | ${null}      | ${'rmit-headline--2'} | ${'H2'}
        ${3}  | ${undefined} | ${'rmit-headline--3'} | ${'H3'}
        ${2}  | ${3}         | ${'rmit-headline--2'} | ${'H3'}
        ${3}  | ${5}         | ${'rmit-headline--3'} | ${'H5'}
    `(
        'should render as $expectedTag when "level" was $level and "tag" was $tag',
        ({ level, tag, expectedClass, expectedTag }) => {
            const wrapper = shallowMount(RmitoHeadline, {
                propsData: { level, tag },
            });

            expect(wrapper.element.tagName).toBe(expectedTag);
            expect(wrapper.classes(expectedClass)).toBe(true);
        }
    );
});
