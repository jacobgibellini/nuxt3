import { mount } from '@vue/test-utils';
import RmitoLogo from './RmitoLogo.vue';

describe('Component: RmitoLogo', () => {
    let wrapper;

    beforeEach(async () => {
        wrapper = await mount(RmitoLogo);
    });

    it('should render the dark variant by default', async () => {
        // Arrange
        const expected = 'rmit-online-logo--dark';

        // Act
        const el = wrapper.find('.rmit-online-logo');

        // Assert
        expect(el.classes()).toContain(expected);
    });

    it('should render the light variant', async () => {
        // Arrange
        const expected = 'rmit-online-logo--light';
        await wrapper.setProps({ variant: 'light' });

        // Act
        const el = wrapper.find('.rmit-online-logo');

        // Assert
        expect(el.classes()).toContain(expected);
    });
});
