import { mount } from '@vue/test-utils';
import RmitoValueProps from './RmitoValueProps';
import RmitoValueProp from '~/components/RmitoValueProp';

const getTitleAndBodyFromTile = (tileComponents, position) => {
    const selectedTileComponent = tileComponents.at(position);

    return {
        title: selectedTileComponent.find('.rmito-value-prop__title').text(),
        body: selectedTileComponent.find('.rmito-value-prop__body').text(),
    };
};

describe('Component: <rmito-value-props />', () => {
    let wrapper;
    let rmitValueTileComponents;

    beforeAll(() => {
        wrapper = mount(RmitoValueProps);
        rmitValueTileComponents = wrapper.findAllComponents(RmitoValueProp);
    });

    it('should render correctly', () => {
        expect(wrapper.element).toMatchSnapshot();
    });

    it('should have section title "Why choose RMIT Online"', () => {
        // Arrange
        const expected = 'Why choose RMIT Online';

        // Act
        const sectionTitle = wrapper.find('.rmito-value-props__title').text();

        // Assert
        expect(sectionTitle).toBe(expected);
    });

    it('should have section subtitle "Get a world-class education and transform your career."', () => {
        // Arrange
        const expected = 'Get a world-class education and transform your career.';

        const sectionSubtitle = wrapper.find('.rmito-value-props__subtitle').text();

        // Assert
        expect(sectionSubtitle).toBe(expected);
    });

    it('should have total 4 tiles', () => {
        expect(rmitValueTileComponents).toHaveLength(4);
    });

    it('should first tile render with static title and body text as expected', () => {
        // Arrange
        const expectedTitle = 'Real world skills';
        const expectedBody =
            'Develop skills that have been validated by industry, while getting credentialed by a world-leading university.';

        // Act
        const { title, body } = getTitleAndBodyFromTile(rmitValueTileComponents, 0);

        // Assert
        expect(title).toBe(expectedTitle);
        expect(body).toBe(expectedBody);
    });

    it('should render first tile with non-credential body variant', async () => {
        // Arrange
        const expectedTitle = 'Real world skills';
        const expectedBody =
            'Develop skills that have been validated by industry, while getting recognised by a world-leading university.';

        // Act
        await wrapper.setProps({ nonCredentialed: true });
        const { title, body } = getTitleAndBodyFromTile(rmitValueTileComponents, 0);

        // Assert
        expect(title).toBe(expectedTitle);
        expect(body).toBe(expectedBody);
    });

    it('should second tile render with static title and body text as expected', () => {
        // Arrange
        const expectedTitle = 'Industry connected';
        const expectedBody =
            "You'll gain knowledge and practical skills from renowned industry partners who are at the forefront of their field.";

        // Act
        const { title, body } = getTitleAndBodyFromTile(rmitValueTileComponents, 1);

        // Assert
        expect(title).toBe(expectedTitle);
        expect(body).toBe(expectedBody);
    });

    it('should third tile render with static title and body text as expected', () => {
        // Arrange
        const expectedTitle = 'Flexible delivery';
        const expectedBody =
            'Advance your career while you study. RMIT Online courses let you balance work, study and life commitments.';

        // Act
        const { title, body } = getTitleAndBodyFromTile(rmitValueTileComponents, 2);

        // Assert
        expect(title).toBe(expectedTitle);
        expect(body).toBe(expectedBody);
    });

    it('should fourth tile render with static title and body text as expected', () => {
        // Arrange
        const expectedTitle = 'Supported community';
        const expectedBody =
            'Be guided by a network of industry experts and peers, and supported by our dedicated success team.';

        // Act
        const { title, body } = getTitleAndBodyFromTile(rmitValueTileComponents, 3);

        // Assert
        expect(title).toBe(expectedTitle);
        expect(body).toBe(expectedBody);
    });

    describe('Component styling variants', () => {
        it('should render the secondary variant', async () => {
            // Arrange
            const expectedClasses = 'rmito-value-props--secondary';

            // Act
            await wrapper.setProps({ secondary: true });
            await wrapper.vm.$nextTick();

            // Assert
            expect(wrapper.classes()).toContain(expectedClasses);
        });

        it('should render the full width variant', async () => {
            // Arrange
            const expectedClasses = 'rmito-value-props--full';

            // Act
            await wrapper.setProps({ fullWidth: true });
            await wrapper.vm.$nextTick();

            // Assert
            expect(wrapper.classes()).toContain(expectedClasses);
        });
    });
});
