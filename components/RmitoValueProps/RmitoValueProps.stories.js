import RmitoValueProps from './RmitoValueProps';
import { withDesign } from 'storybook-addon-designs';

export default {
    title: 'molecules/ValueProps',
    component: RmitoValueProps,
    decorators: [withDesign],
};

const Template = (args, { argTypes }) => ({
    components: {
        RmitoValueProps,
    },
    props: Object.keys(argTypes),
    template: `<rmito-value-props v-bind="$props" />`,
});

export const Default = Template.bind({});
Default.parameters = {
    design: {
        type: 'figma',
        url: 'https://www.figma.com/file/lRwuTtOnB2xMuv8DjT0I5F/Course-detail-page?node-id=909%3A739',
    },
};

export const FullWidth = Template.bind({});
FullWidth.args = {
    fullWidth: true,
};
