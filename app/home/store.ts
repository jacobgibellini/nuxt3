import { mapToAreaOfStudy } from '@/utils/mappers/taxonomy-terms';
import { mapIndustryPartnersLogo } from '@/utils/mappers/industry-partners-logo';
import { isArrayAndHasChild } from '@/utils/array';
import { defineStore } from 'pinia'

const BASE_API_URL = 'https://api.marketstg.rmitonlinestg.com/graphql'

export const useHomeStore = defineStore('home', { 
    state: () => ({
        masthead: {
            alt: '',
            src: {
                desktop: '',
                tablet: '',
                mobile: '',
            }
        },
        testimonials: [],
        video: {},
        logos: [],
        coreStudyAreas: [],
    }),

    actions: {
        async fetchAll () {
            await Promise.all([
                this.fetchHomepageDetails(),
                this.fetchIndustryIcons(),
                this.fetchCoreStudyAreas(),
                this.fetchHomepageVideo(),
                this.fetchHomeStudentTestimonials()
            ])
        },

        async fetchHomepageDetails() {
            const response = await useFetch('https://api.marketstg.rmitonlinestg.com/graphql', {
                params: {
                    id: 'homeblockcontent:2',
                    variables: JSON.stringify({
                        type: 'hero_section_block',
                        limit: 1,
                    })
                }
            })

            const data: any = response.data.value
            const { results } = data.data.homeBlockContent

            if (isArrayAndHasChild(results)) {
                const details = results[0]
                const { desktop, tablet, mobile, alt } = details.fieldHeroImage || {};
        
                this.masthead = {
                    alt: alt || '',
                    src: {
                        desktop: desktop?.url || '',
                        tablet: tablet?.url || '',
                        mobile: mobile?.url || '',
                    },
                }
            }
        },
    
        async fetchHomeStudentTestimonials() {
            const response = await useFetch(BASE_API_URL, {
                params: {
                    id: 'homeblockcontent:2',
                    variables: JSON.stringify({
                        type: 'testimonials',
                        limit: 99,
                    }),
                }
            });

            const data: any = response?.data.value
            const { results = [] } = data?.data?.homeBlockContent || {}

            if (isArrayAndHasChild(results)) {
                this.testimonials = results.map((x: any, index: number) => ({
                    id: `home-page-testimonial-${index}`,
                    name: x.fieldStudentName,
                    image: x.fieldStudentImage?.derivative?.url,
                    quote: x.fieldStudentDescription?.value,
                    title: x.fieldStudentDesignation,
                }));
            }
        },
    
        async fetchCoreStudyAreas() {
            const response = await useFetch(BASE_API_URL, {
                params: {
                    id: 'taxonomyterms:2',
                    variables: JSON.stringify({
                        vid: 'industry',
                        parent: {
                            value: '0',
                        },
                    }),
                }
            });

            const data: any = response?.data?.value
            const { results = [] } = data?.data?.termList || {}
            this.coreStudyAreas = mapToAreaOfStudy(results)
        },
    
        async fetchHomepageVideo() {
            const response = await useFetch(BASE_API_URL, {
                params: {
                    frontEndUrl: '/',
                    id: 'homeblockcontent:2',
                    variables: JSON.stringify({
                        type: 'video_block',
                        limit: 1,
                    }),
                }
            });

            const data: any = response?.data?.value
            const { results = [] } = data?.data.homeBlockContent || {}

            if (isArrayAndHasChild(results)) {
                const video = results[0]
                console.log(results)
                this.video = {
                    url: video.fieldVideoUrl,
                    poster: video.fieldVideoImage?.derivative?.url,
                    title: video.fieldVideoText,
                }
            }
        },
    
        async fetchIndustryIcons() {
            const response = await useFetch(BASE_API_URL, {
                params: {
                    id: 'homeblockcontent:2',
                    variables: JSON.stringify({
                        type: 'industry_partners_logo',
                        limit: 99,
                    }),
                }
            });

            const data: any = response.data.value
            const { results = [] } = data.data?.homeBlockContent || {}

            if (isArrayAndHasChild(results)) {
                this.logos = mapIndustryPartnersLogo(results);
            }
        },
    }
});
