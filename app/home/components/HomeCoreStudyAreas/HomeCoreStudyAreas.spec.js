import { mount } from '@vue/test-utils';
import RmitHomeCoreStudyAreas from './HomeCoreStudyAreas.vue';
import RmitCourseCategorySectionHeader from '@/app/info-architecture/components/RmitCourseCategorySectionHeader';

describe('Component: <rmit-home-core-study-areas />', () => {
    let wrapper;

    beforeAll(() => {
        wrapper = mount(RmitHomeCoreStudyAreas, {
            mocks: {
                $store: {
                    getters: {
                        homeCoreStudyAreas: [
                            {
                                label: 'Category A',
                                description: '<p>Category A description</p>',
                                url: '/category_a_machine_name',
                            },
                            {
                                label: 'Category B',
                                description: '',
                                url: '/category_b_machine_name',
                            },
                            {
                                label: 'Category C',
                                description: '<p>Category C description</p>',
                                url: '/category_c_machine_name',
                            },
                        ],
                    },
                },
            },
        });
    });

    it('should have link to catalogue page', () => {
        const linkEl = wrapper.findComponent({ name: 'RmitCourseCategoryFooterLink' });

        expect(linkEl.text()).toBe('All courses & degrees');
        expect(linkEl.attributes('href')).toBe('/courses');
    });

    it('should have section title "Browse our core study areas"', () => {
        const sectionTitle = wrapper.findComponent(RmitCourseCategorySectionHeader).text();

        expect(sectionTitle).toBe('Browse our core study areas');
    });

    describe('When store was mocked with 3 mocked categories', () => {
        let rmitCategoryCardComponents;

        beforeAll(() => {
            rmitCategoryCardComponents = wrapper.findAllComponents({ name: 'RmitCategoryCard' });
        });

        it('should have total of 3 <rmit-category-card ... /> components', () => {
            expect(rmitCategoryCardComponents).toHaveLength(3);
        });

        it.each`
            index | expectedTitle   | expectedHref                  | expectedDescription
            ${0}  | ${'Category A'} | ${'/category_a_machine_name'} | ${'<p>Category A description</p>'}
            ${1}  | ${'Category B'} | ${'/category_b_machine_name'} | ${''}
            ${2}  | ${'Category C'} | ${'/category_c_machine_name'} | ${'<p>Category C description</p>'}
        `(
            'should <rmit-category-card ... /> component index $index props assigned from store as expected',
            ({ index, expectedTitle, expectedHref, expectedDescription }) => {
                expect(rmitCategoryCardComponents.at(index).props('title')).toBe(expectedTitle);
                expect(rmitCategoryCardComponents.at(index).attributes('href')).toBe(expectedHref);
                expect(rmitCategoryCardComponents.at(index).props('description')).toBe(
                    expectedDescription
                );
            }
        );
    });
});
