import HomeCoursesDesignedByExperts from './HomeCoursesDesignedByExperts';
import { shallowMount } from '@vue/test-utils';

describe('<home-courses-designed-by-experts />', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallowMount(HomeCoursesDesignedByExperts, {
            propsData: {
                title: 'Courses designed by experts',
                industryIcons: [
                    {
                        alt: 'accenture',
                        url: '//assets.fakecdn.com/accenture',
                    },
                    {
                        alt: 'IBM',
                        url: '//assets.fakecdn.com/ibm',
                    },
                    {
                        alt: 'NAB',
                        url: '//assets.fakecdn.com/nab',
                    },
                    {
                        alt: 'salesforce',
                        url: '//assets.fakecdn.com/salesforce',
                    },
                ],
            },
            stubs: ['rmito-section', 'rmit-image'],
        });
    });

    it('should render correctly', () => {
        expect(wrapper.element).toMatchSnapshot();
    });
});
