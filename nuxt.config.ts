import svgLoader from 'vite-svg-loader'

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    modules: [
        '@pinia/nuxt',
    ],

    css: [
        // '@/assets/sass/_main.sass',
        // '@/assets/sass/global/index.sass',
    ],

    vite: {
        plugins: [
            svgLoader()
        ],
        css: {
            preprocessorOptions: {
                sass: {
                    additionalData: `@import "@/assets/sass/_main.sass"`
                }
            }
        }
    }
})
